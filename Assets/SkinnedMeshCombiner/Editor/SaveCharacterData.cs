﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace LylekGames.Tools
{
    public class SaveCharacterData : Editor
    {
        public static Texture2D SaveTexture(Texture2D myTexture, string path, string extension = "", TextureFormat textureFormat = TextureFormat.ARGB32, bool isMipMap = true)
        {
            //CREATE A TEXTURE TO SAVE
            Texture2D newTexture = new Texture2D(myTexture.width, myTexture.height, textureFormat, isMipMap);
            //GET IMAGE
            newTexture.SetPixels(0, 0, myTexture.width, myTexture.height, myTexture.GetPixels());
            newTexture.Apply();
            //ENCODE TEXTURE TO PNG
            byte[] bytes = newTexture.EncodeToPNG();
            File.WriteAllBytes("Assets" + path + "Textures/" + extension, bytes);
            //FORCE IMPORT OUR PNGs (so unity notices them and we can use them immediately)
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset("Assets" + path + "Textures/" + extension, ImportAssetOptions.ForceUpdate);

            //ADJUST THE IMPORT SETTINGS OF OUR NORMAL MAP
            if (extension.Contains("_normals"))
            {
                Texture2D saveTexture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets" + path + "Textures/" + extension, typeof(Texture2D));
                string textureAssetPath = AssetDatabase.GetAssetPath(saveTexture);
                TextureImporter textureImport = (TextureImporter)TextureImporter.GetAtPath(textureAssetPath);
                textureImport.textureType = TextureImporterType.NormalMap;
                textureImport.SaveAndReimport();
            }

            return newTexture;
        }
        public static Material SaveMaterial(Material myMaterial, string path, string extension = "")
        {
            //CREATE AND SAVE NEW MATERIAL
            AssetDatabase.CreateAsset(myMaterial, "Assets" + path + "Materials/" + extension);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            //GET OUR MATERIAL
            myMaterial = (Material)AssetDatabase.LoadAssetAtPath("Assets" + path + "Materials/" + extension, typeof(Material));

            return myMaterial;
        }
        public static Mesh SaveMesh(Mesh myMesh, string path, string extension = "")
        {
            //SAVE MESH DATA
            AssetDatabase.CreateAsset(myMesh, "Assets" + path + "Meshes/" + extension);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            //GET OUR MESH
            myMesh = (Mesh)AssetDatabase.LoadAssetAtPath("Assets" + path + "Meshes/" + extension, typeof(Mesh));

            return myMesh;
        }
        public static GameObject SaveAsPrefab(GameObject myObject, string path, string extension = "")
        {
            //CREATE A PREFAB OF THIS GAMEOBJECT
            PrefabUtility.SaveAsPrefabAssetAndConnect(myObject, "Assets" + path + extension, InteractionMode.UserAction);

            //GET OUR GAMEOBJECT
            GameObject prefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets" + path + extension, typeof(GameObject));
            AssetDatabase.SaveAssets();

            return prefab;
        }
    }
}
