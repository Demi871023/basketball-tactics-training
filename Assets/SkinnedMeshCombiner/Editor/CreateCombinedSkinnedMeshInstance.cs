﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LylekGames.Tools
{
	public class CreateCombinedStaticMeshInstance
    {
		[MenuItem("Tools/Combine/Skinned Meshes")]
		private static void CreateNewPlayer() {
			//MY OBJECT
			Object selectedObject = Selection.activeObject;
			GameObject myObject = (GameObject)selectedObject;

            CombineSkinMeshesTextureAtlas myCombine;
            if (!myObject.GetComponent<CombineSkinMeshesTextureAtlas>())
                myCombine = myObject.AddComponent<CombineSkinMeshesTextureAtlas>();
            else
                myCombine = myObject.GetComponent<CombineSkinMeshesTextureAtlas>();
        }
	}
}