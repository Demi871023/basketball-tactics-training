﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;

[Serializable]
public class RunPacket
{
    public int startTime;
    public int duration;
    public string handler;
    public int roadStart;
    public int roadEnd;
    public int rate;
    public int ballHolder;

    #region Add after add dribble and screen
    public int pathType;
    public float screenAngle;
    public float dribbleAngle;
    public float dribbleLength;
    #endregion
}

[Serializable]
public class BallPassPair
{
    public bool isValid;
    public int passerId;
    public int catcherId;
}

[Serializable]
public class Tactic {
    public Constants.TacticCategory categoryId;
    public string tacticName;
    public int id;
    // <initialBallHolder>
    // index from 1 to 5
    public int initialBallHolder;
    
    public List<RunPacket> runBag;
    public List<Vector2>[] offenderPath;
    public List<Vector2>[] defenderPath;
    public List<Vector2> ballPath;
    public List<Vector2> initialPosition;
    // <ballpasspairs>
    // Not send from the tactic board,
    // Will be calculated at the start of the tactic execution.
    public BallPassPair[] ballpasspairs;
}

public class TacticManager : Singleton<TacticManager> {
    protected TacticManager(){}

	public List<Tactic> _tactics;
	public Tactic _currentChosenTactic;
    public int _currentSelectIndex;
    private bool _isTacticLoad;

    // Use this for initialization
    void Start () {
        _tactics = new List<Tactic>();
        LoadTactics();
        _currentChosenTactic = _tactics[0];
        _currentSelectIndex = 0;
	}

    // Garabage function
    private string HandleNameTransfer(string oriName)
    {
        string inGameName = "";
        if (oriName.Equals("P1_Handle"))
        {
            inGameName = "0";
        }
        else if (oriName.Equals("P2_Handle"))
        {
            inGameName = "1";
        }
        else if (oriName.Equals("P3_Handle"))
        {
            inGameName = "2";
        }
        else if (oriName.Equals("P4_Handle"))
        {
            inGameName = "3";
        }
        else if (oriName.Equals("P5_Handle"))
        {
            inGameName = "4";
        }
        else if (oriName.Equals("B_Handle"))
        {
            inGameName = "10";
        }
        else if (oriName.Equals("D1_Handle"))
        {
            inGameName = "5";
        }
        else if (oriName.Equals("D2_Handle"))
        {
            inGameName = "6";
        }
        else if (oriName.Equals("D3_Handle"))
        {
            inGameName = "7";
        }
        else if (oriName.Equals("D4_Handle"))
        {
            inGameName = "8";
        }
        else if (oriName.Equals("D5_Handle"))
        {
            inGameName = "9";
        }

        return inGameName;
    }

    // Add tactic to the list by parsing the JSON
    private void AddTactic(string jsonString, int id){
        JSONNode jsonData = JSON.Parse(jsonString);
        
        List<RunPacket> tmpRunBag = new List<RunPacket>();
        List<Vector2>[] tmpOffenderPath = new List<Vector2> [5];
        List<Vector2>[] tmpDefenderPath = new List<Vector2> [5];
        List<Vector2> tmpBallPath = new List<Vector2>();
        List<Vector2> tmpInitialPosition = new List<Vector2>();

        for (int i = 0; i < jsonData["Runline"].Count; i++) {
            RunPacket tmpPacket = new RunPacket();
            tmpPacket.startTime = jsonData["Runline"][i]["start_time"];
            tmpPacket.duration = jsonData["Runline"][i]["duration"];
            tmpPacket.handler = HandleNameTransfer(jsonData["Runline"][i]["handler"]);
            tmpPacket.roadStart = (jsonData["Runline"][i]["road_start"] - 1)/2;
            tmpPacket.roadEnd = jsonData["Runline"][i]["road_end"]/2;
            tmpPacket.rate = jsonData["Runline"][i]["rate"];
            tmpPacket.ballHolder = jsonData["Runline"][i]["ball_num"];
            
            tmpPacket.pathType = jsonData["Runline"][i]["path_type"];
            tmpPacket.screenAngle = jsonData["Runline"][i]["screen_angle"];
            tmpPacket.dribbleAngle = jsonData["Runline"][i]["dribble_angle"];
            tmpPacket.dribbleLength = jsonData["Runline"][i]["dribble_length"];

            tmpRunBag.Add(tmpPacket);
        }

        // 2018.09.10 Read the offensivePlayer path
        List<Vector2> tmpPlayer1Path = new List<Vector2>();
        List<Vector2> tmpPlayer2Path = new List<Vector2>();
        List<Vector2> tmpPlayer3Path = new List<Vector2>();
        List<Vector2> tmpPlayer4Path = new List<Vector2>();
        List<Vector2> tmpPlayer5Path = new List<Vector2>();

        for (int i = 0; i < jsonData["Runline"].Count; i++) {
            string handler = jsonData["Runline"][i]["handler"];
            int roadStart = jsonData["Runline"][i]["road_start"];
            int roadEnd = jsonData["Runline"][i]["road_end"];

            if(handler == "P1_Handle"){
                for (int j = roadStart; j < roadEnd; j=j+2) {
                    tmpPlayer1Path.Add(TransformCoordinate( new Vector2(jsonData["P1"][j], jsonData["P1"][j+1])) );
                }
            }
            else if(handler == "P2_Handle"){
                for (int j = roadStart; j < roadEnd; j=j+2) {
                    tmpPlayer2Path.Add(TransformCoordinate( new Vector2(jsonData["P2"][j], jsonData["P2"][j+1])) );
                }
            }
            else if(handler == "P3_Handle"){
                for (int j = roadStart; j < roadEnd; j=j+2) {
                    tmpPlayer3Path.Add(TransformCoordinate( new Vector2(jsonData["P3"][j], jsonData["P3"][j+1])) );
                }
            }
            else if(handler == "P4_Handle"){
                for (int j = roadStart; j < roadEnd; j=j+2) {
                    tmpPlayer4Path.Add(TransformCoordinate( new Vector2(jsonData["P4"][j], jsonData["P4"][j+1])) );
                } 
            }
            else if(handler == "P5_Handle"){
                for (int j = roadStart; j < roadEnd; j=j+2) {
                    tmpPlayer5Path.Add(TransformCoordinate( new Vector2(jsonData["P5"][j], jsonData["P5"][j+1])) );
                } 
            }
        }
        tmpOffenderPath[0] = tmpPlayer1Path;
        tmpOffenderPath[1] = tmpPlayer2Path;
        tmpOffenderPath[2] = tmpPlayer3Path;
        tmpOffenderPath[3] = tmpPlayer4Path;
        tmpOffenderPath[4] = tmpPlayer5Path;

        for (int i = 0; i < jsonData["B"].Count; i++) {
            tmpBallPath.Add(TransformCoordinate( new Vector2(jsonData["B"][i], jsonData["B"][i+1])) );
        }
        
        //Read the initial position
        for (int i = 0; i < jsonData["Initial_Position"].Count; i=i+2)
        {
            tmpInitialPosition.Add(TransformCoordinate( new Vector2(jsonData["Initial_Position"][i], jsonData["Initial_Position"][i+1])) );      
        }

        BallPassPair[] tmpBallpasspairs = new BallPassPair[20];
        for (int i = 0; i < 20; i++)
        {
            tmpBallpasspairs[i] = new BallPassPair()
            {
                isValid = false,
                passerId = -1,
                catcherId = -1
            };
        }

        int prevHolderId = jsonData["Initial_ball_holder"];
        for (int i = 0; i < tmpRunBag.Count; i++)
        {
            // If current handler is ball
            if (tmpRunBag[i].handler == "10")
            {
                int timing = tmpRunBag[i].startTime;
                tmpBallpasspairs[timing] = new BallPassPair()
                {
                    isValid = true,
                    passerId = prevHolderId,
                    catcherId = tmpRunBag[i].ballHolder
                };
                prevHolderId = tmpRunBag[i].ballHolder;

            }
        }

        Tactic newTactic = new Tactic()
        {
            id = id,
            runBag = tmpRunBag,
            offenderPath = tmpOffenderPath,
            defenderPath = tmpDefenderPath,
            ballPath = tmpBallPath,
            initialPosition = tmpInitialPosition,
            initialBallHolder = jsonData["Initial_ball_holder"],
            ballpasspairs = tmpBallpasspairs,
            tacticName = jsonData["Tactic_name"],
            categoryId = (Constants.TacticCategory)jsonData["Category_id"].AsInt,
        };
        _tactics.Add(newTactic);
    }

    [ContextMenu("Load Tactics")]
    private void LoadTactics(){
        TextAsset[] targetFile = Resources.LoadAll<TextAsset>("TacticsE");
        
        for(int i=0;i<targetFile.Length;i++)
            AddTactic(targetFile[i].text, i);
        Debug.LogWarning("Load tactic library:" + _tactics.Count);
        _isTacticLoad = true;
    }

    private Vector2 TransformCoordinate(Vector2 origin){
        float x = (origin.x - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X;
        float y = (origin.y - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y;
        return new Vector2(x, y);
    }
}
