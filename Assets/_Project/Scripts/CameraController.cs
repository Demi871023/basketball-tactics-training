﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using wvr;


// Constrain the camera to specific position
public class CameraController : MonoBehaviour {
	

	public GameObject target;
	public float root_height;
	// Update is called once per frame
	void Start(){
		root_height = 7.0f;
	}

	void Update () {
		float interp_velocity;
		
		Vector3 offset = Vector3.zero;
		Vector3 target_position = target.transform.position;

		Vector3 position_no_y = this.transform.position;
		position_no_y.y = target.transform.position.y;

		Vector3 target_direction = (target.transform.position - position_no_y);

		interp_velocity = target_direction.magnitude * 3f;

		target_position = this.transform.position + (target_direction.normalized * interp_velocity * 0.03f); //Time.deltaTime

		Vector3 new_position = Vector3.Lerp( this.transform.transform.position, target_position + offset, 0.25f);
		new_position.y = root_height;//7.0f;
		this.transform.position = new_position;//Vector3.Lerp( this.transform.transform.position, targetPos + offset, 0.25f);
	}
}
