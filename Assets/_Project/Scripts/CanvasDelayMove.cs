﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasDelayMove : MonoBehaviour {
	public float distance;
	private const int DELAY_FRAME = 15; 
    private enum CameraState {
        INITIAL,
        FIRST,
        DELAY
    }
	private CameraState current_state;
	private int frame_counter;
    private Vector3[] last_position;

	void Start () {
		frame_counter = 0;
		last_position = new Vector3[DELAY_FRAME];
		current_state = CameraState.INITIAL;
	}
	
	// Update is called once per frame
	void Update () {
		Camera attached_camera = CameraManager.Instance.wave_camera;
		Vector3 camera_forward, new_position;
		
        switch (current_state) {
            case CameraState.INITIAL:
                camera_forward = attached_camera.transform.forward;
                new_position = attached_camera.transform.position + camera_forward * distance;
                this.transform.position = new_position;
                this.transform.LookAt(attached_camera.transform.position);

                current_state = CameraState.FIRST;
                break;
            case CameraState.FIRST:
                camera_forward = attached_camera.transform.forward;
                new_position = attached_camera.transform.position + camera_forward * distance;
                last_position[frame_counter] = new_position;
                frame_counter++;
                if (frame_counter == DELAY_FRAME)
                {
                    frame_counter = 0;
                    current_state = CameraState.DELAY;
                }
                break;
            case CameraState.DELAY:
                this.transform.position = last_position[frame_counter];
                this.transform.LookAt(attached_camera.transform.position);

                camera_forward = attached_camera.transform.forward;
                new_position = attached_camera.transform.position + camera_forward * distance;

                //Debug.Log("C:" + newPosition);
                //Debug.Log("5:" + lastFivePosition[frameCounter]);

                last_position[frame_counter] = new_position;
                frame_counter++;
                if (frame_counter == DELAY_FRAME)
                {
                    frame_counter = 0;
                    current_state = CameraState.DELAY;
                }
                break;
        }
	}
}
