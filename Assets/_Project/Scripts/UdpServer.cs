using UnityEngine;  
using System.Collections;  
//引入库  
using System.Net;  
using System.Net.Sockets;  
using System.Text;  
using System.Threading;
using System;
using System.Net.NetworkInformation;

public static class IPAddressExtensions
{
        public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
        {
            byte[] ip_adress_bytes = address.GetAddressBytes();
            byte[] subnet_mask_bytes = subnetMask.GetAddressBytes();

            if (ip_adress_bytes.Length != subnet_mask_bytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcast_address = new byte[ip_adress_bytes.Length];
            for (int i = 0; i < broadcast_address.Length; i++)
            {
                broadcast_address[i] = (byte)(ip_adress_bytes[i] | (subnet_mask_bytes[i] ^ 255));
            }
            return new IPAddress(broadcast_address);
        }
}  


public class UdpServer:MonoBehaviour  
    {  
        //以下默认都是私有的成员  
        Socket socket; //目标socket  
        EndPoint client_end; //客户端  
        IPEndPoint ip_end; //侦听端口  
        string receive_string; //接收的字符串  
        string send_string; //发送的字符串  
        byte[] receive_data=new byte[1024]; //接收的数据，必须为字节  
        byte[] send_data=new byte[1024]; //发送的数据，必须为字节  
        int receive_string_length; //接收的数据长度  
        Thread connect_thred; //连接线程  
	
		Socket server;
		IPEndPoint remote_ip;
		UdpClient uc;
        Socket s;
         
        //初始化  
        void InitSocket()  
        {  
            foreach(NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
            if(ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
            {
                //Debug.LogWarning(ni.Name);
                foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        //Debug.LogWarning(ip.Address.ToString());
                    }
                }
            }  
            }
            // TODO:How to get the subnet mask?
            
            IPAddress current_ip = IPAddress.Parse(GetLocalIPAddress());
            IPAddress subnet_mask = IPAddress.Parse("255.255.255.0");
            IPAddress broadcast_add = IPAddressExtensions.GetBroadcastAddress(current_ip, subnet_mask);
            remote_ip = new IPEndPoint(broadcast_add, 3983); //可自行定義廣播區域跟Port
            //remote_ip = new IPEndPoint(IPAddress.Broadcast, 3983);
            //Debug.LogWarning("broadcast: " + IPAddress.Broadcast);
            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            //2019.02.15 Essential for broadcasting in android <- Don't know why
            s.EnableBroadcast = true;
			
			StartCoroutine(SendData());
        }  

        

		IEnumerator SendData(){
			while(true){
				byte[] push_data = new byte[1024]; //定義要送出的封包大小
				string host =  Dns.GetHostName();
                push_data = Encoding.UTF8.GetBytes(host);//GetLocalIPAddress()); //把要送出的資料轉成byte型態
				//Debug.LogError(GetLocalIPAddress());
				s.SendTo(push_data, remote_ip); //送出的資料跟目的
				yield return new WaitForSeconds(1);
			}
		}
      
        void SocketSend(string sendStr)  
        {  
            //清空发送缓存  
            send_data=new byte[1024];  
            //数据类型转换  
            send_data=Encoding.ASCII.GetBytes(sendStr);  
            //发送给指定客户端  
            socket.SendTo(send_data,send_data.Length,SocketFlags.None,client_end);  
        }  

        public static string GetLocalIPAddress()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == AddressFamily.InterNetwork)
				{   
                    Debug.LogWarning(ip);
					return ip.ToString();
				}
			}
			throw new Exception("No network adapters with an IPv4 address in the system!");
		}
      
        //服务器接收  
        void SocketReceive()  
        {  
            //进入接收循环  
            while(true)  
            {  
				send_string="Send From Server";  
                SocketSend(send_string);  

                //对data清零  
                receive_data=new byte[1024];  
                //获取客户端，获取客户端数据，用引用给客户端赋值  
                receive_string_length=socket.ReceiveFrom(receive_data,ref client_end);  
                print("message from: "+client_end.ToString()); //打印客户端信息  
                //输出接收到的数据  
                receive_string=Encoding.ASCII.GetString(receive_data,0,receive_string_length);  
                print(receive_string);  
                //将接收到的数据经过处理再发送出去  
                send_string="From Server: "+receive_string;  
                SocketSend(send_string);  
            }  
        }  
      
        //连接关闭  
        void SocketQuit()  
        {  
            //关闭线程  
            if(connect_thred!=null)  
            {  
                connect_thred.Interrupt();  
                connect_thred.Abort();  
            }  
            //最后关闭socket  
            if(socket!=null)  
                socket.Close();  
            print("disconnect");  
        }  
      
        // Use this for initialization  
        void Start()  
        {  
            
            InitSocket(); //在这里初始化server

        }  
      
        void OnApplicationQuit()  
        {  
            SocketQuit();  
        }  

    }