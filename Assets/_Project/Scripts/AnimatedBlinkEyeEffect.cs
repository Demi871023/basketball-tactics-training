﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;

public class AnimatedBlinkEyeEffect : MonoBehaviour {
    CanvasGroup maskImage;
    float alpha = 1.0f;
    float blink_eye_step = 0.01f;


    // Use this for initialization
    void Awake () {
        maskImage = this.GetComponent<CanvasGroup>();
    }

	
	// Update is called once per frame
	void Update () {
        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            InvokeEyeBlink();
        }
        */
    }

    public void InvokeEyeBlink() {
        maskImage.alpha = 1.0f;
        alpha = 1.0f;
        InvokeRepeating("BlinkEye", 0.0f, 0.05f);
    }

    void BlinkEye() {
        maskImage.alpha = alpha;
        alpha = alpha - blink_eye_step;
        if (alpha < 0.05f)
            CancelInvoke("BlinkEye");
    }
}
