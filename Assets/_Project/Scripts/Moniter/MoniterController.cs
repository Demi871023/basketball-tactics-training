﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using wvr;
using UnityEngine.UI;
using System;

public class MoniterController : MonoBehaviour {
	
	
	public Text tx_tactic;
	public Text tx_tactic_name;

	private float previous_y_axis;
	private float initial_height;

	
	void Start(){
		previous_y_axis = 0.0f;
		initial_height = ModeManagerLite.Instance._waveVR.transform.position.y;
	}

	void Update () {

		if (Input.GetKeyDown(KeyCode.LeftArrow)){
			TacticManager.Instance._currentSelectIndex--;
			if(TacticManager.Instance._currentSelectIndex < 0) TacticManager.Instance._currentSelectIndex = TacticManager.Instance._tactics.Count-1;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterMoniter.Instance.OnSelectedTactic();
		}

		if (Input.GetKeyDown(KeyCode.RightArrow)){
			TacticManager.Instance._currentSelectIndex++;
			if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterMoniter.Instance.OnSelectedTactic();
		}

		if(Input.GetKeyDown(KeyCode.L)){
			TacticExecuterMoniter.Instance.OnSelectedTactic();
		}

		if(Input.GetKeyDown(KeyCode.M)){
			OnTriggerMode();
		}
		if(Input.GetKeyDown(KeyCode.P)){
			OnTriggerPerspective();
		}
		if(Input.GetKeyDown(KeyCode.C)){
			TacticExecuterMoniter.Instance.OnTriggerContinue(true);
		}
		if(Input.GetKeyDown(KeyCode.S)){
			TacticExecuterMoniter.Instance.InvokeExecution();
		}
	}

	public void OnSelectNextTactic(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			TacticManager.Instance._currentSelectIndex++;
			if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterMoniter.Instance.OnSelectedTactic();
			tx_tactic.text = TacticManager.Instance._currentSelectIndex.ToString();
			tx_tactic_name.text = TacticManager.Instance._currentChosenTactic.tacticName;
		}
	}

	public void OnTriggerPerspective(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_perspecitve = (int)ModeManagerLite.Instance._perspective + 1;
			if(new_perspecitve > (int)Constants.Perspective.FIRST_PP_5) new_perspecitve = 0;
			ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)new_perspecitve);
		}
	}

	public void OnTriggerMode(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_mode = (int)ModeManagerLite.Instance._mode + 1;
			if(new_mode > (int)Constants.Mode.LEARNING) new_mode = 0;
			ModeManagerLite.Instance._mode = (Constants.Mode)new_mode;

			//Tactic3DExecuter.Instance.learning_pass_hint.SetActive(!Tactic3DExecuter.Instance.learning_pass_hint.gameObject.activeInHierarchy);
		}
	}

}
