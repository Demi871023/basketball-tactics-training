﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixRotation : MonoBehaviour {
	private Camera camera;
	// Update is called once per frame
	void Start(){
		camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}

	void Update () {
		
		this.transform.LookAt(camera.transform.position);
		this.transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));
	}
}
