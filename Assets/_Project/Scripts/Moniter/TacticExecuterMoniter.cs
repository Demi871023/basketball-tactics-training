﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Profiling;
using SimpleJSON;
using UnityEngine.UI;

public class TacticExecuterMoniter : Singleton<TacticExecuterMoniter> {

/********************************/
    //      3D tactic execution     //
    /********************************/

	public int current_ball_holder;
	public GameObject runpath_renderer_parent;

	private IEnumerator coroutine;
	private Constants.ExecutorState current_state;

	private int max_time;
	private int time_counter;
	private bool is_pass_ball = false;

	private const float update_rate = 1F;
	
	public InvokedAgent[] invoked_agent;
    public class InvokedAgent
    {
        public bool is_invoked;
        public bool is_dribbling;
        public bool is_screen;
        public float screen_angle;
        public int start_index;
        public int end_index;
		public int path_type;
    };

	private bool player_direction_is_right = false;
	private bool user_want_to_continue = false;
	private bool activate_pass_ball = false;
	private bool need_to_pass_ball = false;
	
	private List<LineRenderer> runpath_renderers;
	private List<GameObject> screen_bar_prefabs;
	private Color[] runpath_color_start;
	private Color[] runpath_color_end;

	public class Player
	{
		public GameObject player;
		public Animator animator;
		public NavMeshAgent navmesh_agent;
		public NavMeshWayPoint navmesh_waypoint;
		public PlayerMovement player_movement;
	};

	public Player[] players;

	// Use this for initialization
  	void Start () {
		coroutine = ExecuteTactic();

		runpath_renderers = new List<LineRenderer>();
		screen_bar_prefabs = new List<GameObject>();
		#region Initialize run path color
		runpath_color_end = new Color[5];
		
		runpath_color_end[0] = new Color32(17, 75, 95, 255);
		runpath_color_end[1] = new Color32(21, 127, 31, 255);
		runpath_color_end[2] = new Color32(228, 253, 225, 255);
		runpath_color_end[3] = new Color32(69, 105, 144, 255);
		runpath_color_end[4] = new Color32(244, 91, 105, 255);

		runpath_color_start = new Color[5];
		runpath_color_start[0] = new Color32(255, 255, 255, 255);
		runpath_color_start[1] = new Color32(255, 255, 255, 255);
		runpath_color_start[2] = new Color32(255, 255, 255, 255);
		runpath_color_start[3] = new Color32(255, 255, 255, 255);
		runpath_color_start[4] = new Color32(255, 255, 255, 255);
		#endregion
		#region Initialize players
		players = new Player[5];
		for(int i=0 ; i<5 ; i++){
			players[i] = new Player();
			players[i].player = GameObject.FindGameObjectWithTag("Player" + (i+1));
			players[i].animator = players[i].player.GetComponent<Animator>();
			players[i].navmesh_agent = players[i].player.GetComponent<NavMeshAgent>();
			players[i].navmesh_waypoint = players[i].player.GetComponent<NavMeshWayPoint>();
			players[i].player_movement = players[i].player.GetComponent<PlayerMovement>();
		}
		#endregion
	
	}
	
	public void OnSelectedTactic(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;

			//Set the ball holder to hold ball humaniod rig position
			GameObject ball = GameObject.FindGameObjectWithTag("Basketball");
			ball.GetComponent<Rigidbody>().useGravity = false;
			ball.GetComponent<Rigidbody>().isKinematic = true;
			ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;

			//Reset the animation state of all the players and defenders
			for(int i=0;i<5;i++)
				players[i].animator.SetTrigger("tacticStart");
			

			int ball_holder_id = current_tactic.initialBallHolder;
			
			Animator ball_holder_animator = players[ball_holder_id-1].animator;
			ball_holder_animator.SetTrigger("holdBall");

			GameObject ball_holder;
			Transform ball_transform = ball.transform;
			ball.transform.SetParent(ball_holder_animator.GetBoneTransform(HumanBodyBones.RightHand));
			// The paremeter is properly setting in the scene.
			ball_transform.localPosition = new Vector3(-0.0142f, 0.0467f, 0.0066f);
			ball_transform.localRotation = Quaternion.identity;
			ball_transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
			
			//Make the ballHolder look at the hoop direction
			ball_holder = players[ball_holder_id-1].player;

			Vector3 hoop_point = ModeManagerLite.Instance._hoopPosition;
			hoop_point.y = ball_holder.transform.position.y;
			ball_holder.transform.LookAt(hoop_point);
			
			//Set all the player in target postion
			for (int i = 0; i < 5; i++)
			{
				GameObject player = players[i].player;
				players[i].navmesh_agent.enabled = false;
				players[i].player.transform.position = new Vector3(current_tactic.initialPosition[i].x, 0, current_tactic.initialPosition[i].y);
				players[i].navmesh_agent.enabled = true;
				
				
				float distance_to_hoop = (player.transform.position - ModeManagerLite.Instance._hoopPosition).magnitude;
				
				if (distance_to_hoop > 7.0f){
					player.transform.LookAt(hoop_point);
				}
				else
					player.transform.LookAt(ball_holder.transform.position);
				
				// Pre relocate the Camera's position
				if((i+1) == (int)ModeManagerLite.Instance._perspective){
					GameObject input_manager = GameObject.FindGameObjectWithTag("InputManager");
					input_manager.GetComponent<CameraController>().target = players[i].player;
					input_manager.transform.position = players[i].player.transform.position;
					input_manager.transform.rotation = players[i].player.transform.rotation;
				}

			}

			ClearRunPathList();
			ball.GetComponent<BasketballMoniter>().ClearBallPath();
		}
	}

	public void InvokeExecution(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;
			
			//Initialize the maxTime of current tactic
			max_time = 0;
			time_counter = 0;
			for (int i = 0; i < current_tactic.runBag.Count; i++) {
				if (current_tactic.runBag[i].startTime > max_time) {
					max_time = current_tactic.runBag[i].startTime;
				}
			}
			current_state = Constants.ExecutorState.START;
			current_ball_holder = current_tactic.initialBallHolder;

			OnSelectedTactic();
			Debug.Log(TacticManager.Instance._currentSelectIndex+":"+TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex].tacticName);
			invoked_agent = new InvokedAgent[11];
			user_want_to_continue = false;
			

			coroutine = ExecuteTactic();
			this.StartCoroutine(coroutine);
		}
	}

	private IEnumerator ExecuteTactic(){
		while(true){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;
			List<RunPacket> current_runbag = current_tactic.runBag;
			bool current_mode_is_learning = (ModeManagerLite.Instance._mode == Constants.Mode.LEARNING) ? true : false;
			bool current_perspective_is_3PP = (ModeManagerLite.Instance._perspective == Constants.Perspective.THIRD_PP) ? true : false;
			
			GameObject ball = GameObject.FindGameObjectWithTag("Basketball");
			
			switch(current_state){
				case Constants.ExecutorState.START:
					Debug.LogError("Tactic Start to play.");
					ModeManagerLite.Instance._tacticIsExecuting = true;
					for (int i = 0; i < 11; i++){
						invoked_agent[i] = new InvokedAgent();
						invoked_agent[i].is_invoked = false;
					}

					if(ModeManagerLite.Instance._mode == Constants.Mode.LEARNING){
						current_state = Constants.ExecutorState.LEARNING_START;
						ModeManagerLite.Instance._canvasContinue.SetActive(true);
					}
					else{
						current_state = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
					}

					//TODO:Remember to re-rotate the VR camera
					break;
				
				case Constants.ExecutorState.LEARNING_START:
					
					if(user_want_to_continue){
						ModeManagerLite.Instance._canvasContinue.SetActive(false);
						current_state = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
					}
					break;
				case Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG:
					//Debug.Log("State ==> CheckCurrTimeRunBag");
					activate_pass_ball = false;
					player_direction_is_right = false;
					user_want_to_continue = false;
					need_to_pass_ball = false;

					for(int i=0 ; i<current_runbag.Count ; i++){
						if(current_runbag[i].startTime == time_counter){

							#region Parse the handler to handleName
							string handle = current_runbag[i].handler;
							string handle_name;
							if(handle == "10"){
								handle_name = "Basketball";
							}
							else if(handle == "0" || handle == "1" || handle == "2" || handle == "3" || handle == "4"){
								handle_name = "Player" + (System.Convert.ToInt32(handle) + 1);
							}
							else{
								handle_name = "Defender" + (System.Convert.ToInt32(handle) - 4);
							}
							#endregion

							for(int j=0;j<5;j++){
								players[j].animator.SetBool("isScreen", false);
							}

							#region According to the handle, Setting the invoke agent and target animations
							if(handle == "10"){
								// Ball passing is being invoked.
								// Setting the animation parameter of "passBall" 

								
                                int passer_id = current_tactic.ballpasspairs[time_counter].passerId;
								int catcher_id = current_tactic.ballpasspairs[time_counter].catcherId;
								GameObject passer = players[passer_id-1].player;
								//Check whether the dribbling layer is invoked.
								/*
								if(players[passer_id-1].animator.GetLayerWeight(1) > 0.9f){
									players[passer_id-1].animator.SetTrigger("holdBall");
									players[passer_id-1].animator.SetLayerWeight(1, 0.0f);
									ball.GetComponent<BasketballMoniter>().is_dribbling_end = true;
								}

								//players[passer_id-1].animator.ResetTrigger("holdBall");
								players[passer_id-1].animator.SetTrigger("passBall");
								*/

								// Setting the attachment of the ball and the trajectory 
								GameObject catcher = GameObject.FindGameObjectWithTag("Player" + current_tactic.ballpasspairs[time_counter].catcherId);
								Vector3 passer_hand_position = passer.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform.position;
								Vector3 catcher_hand_position = catcher.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform.position;
								ball.GetComponent<BasketballMoniter>().GeneratePassBallTrajectory(passer_hand_position, catcher_hand_position);
								
								//if( ModeManagerLite.Instance.perspective == (Constants.Perspective)passer_id)
								//	need_to_pass_ball = true;

								if (ModeManagerLite.Instance._perspective != (Constants.Perspective)passer_id)
                                {
                                    // If current passer is not the user's current perspective
                                    //passer.transform.LookAt(catcher.transform.position);		
									
									// 2018.12.20 Smooth look at the catcher						
									//passer.transform.rotation = Quaternion.Slerp(passer.transform.rotation, Quaternion.LookRotation(catcher.transform.position - passer.transform.position, passer.transform.up), Time.deltaTime * rotateRate);

									passer.GetComponent<PlayerMovement>().SmoothLookAt(catcher);
								}

								if(ModeManagerLite.Instance._perspective != (Constants.Perspective)catcher_id)
									catcher.GetComponent<PlayerMovement>().SmoothLookAt(passer);

								invoked_agent[10].is_invoked = true;
								current_ball_holder = current_tactic.ballpasspairs[time_counter].catcherId;
								ball.GetComponent<BasketballMoniter>().time_counter = time_counter;
							}
							else{
								GameObject player = GameObject.FindGameObjectWithTag(handle_name);
								NavMeshAgent player_agent = player.GetComponentInChildren<NavMeshAgent>();
								if (player != null)
                                {
									int start_index = current_runbag[i].roadStart;
                                    int end_index = current_runbag[i].roadEnd - 1;
									int invoke_id = System.Convert.ToInt32(handle);

									invoked_agent[invoke_id].is_invoked = true;
                                    invoked_agent[invoke_id].start_index = current_runbag[i].roadStart;
                                    invoked_agent[invoke_id].end_index = current_runbag[i].roadEnd - 1;
                                    invoked_agent[invoke_id].is_dribbling = (current_runbag[i].pathType == 2) ? true:false;
									invoked_agent[invoke_id].is_screen = (current_runbag[i].pathType == 1)? true:false;
									invoked_agent[invoke_id].path_type = current_runbag[i].pathType;
									
									if(invoke_id < 5){
										List<Vector2> path = new List<Vector2>();
										for(int t = start_index ; t<end_index ; t++){
                                            bool null_checker = current_tactic.offenderPath[invoke_id][t] == null ? true:false;
                                            if(!null_checker){
                                                path.Add(new Vector2(current_tactic.offenderPath[invoke_id][t].x, current_tactic.offenderPath[invoke_id][t].y));
                                            }
                                        }
										player_agent.GetComponent<NavMeshWayPoint>().SetWayPoints(path);
                                        /*
										player_agent.GetComponent<NavMeshWayPoint>().StartToMove();
										
										if(current_runbag[i].path_type == 2){
                                            Animator player_anim = player.GetComponent<Animator>();
                                            player_anim.SetLayerWeight(1, 1.0f);
                                            player_anim.ResetTrigger("holdBall");
											player_anim.SetTrigger("tacticStart");
											ball.GetComponent<BasketballMoniter>().InvokeDribbling();
											ball.GetComponent<BasketballMoniter>().is_dribbling_end = false;
                                        }
										*/
										// 2018.12.30 Render run paths
										LineRenderer line_renderer =  ((GameObject)Instantiate(Resources.Load("Prefabs/PlayerRunPathRenderer"))).GetComponent<LineRenderer>();
										line_renderer.transform.parent = runpath_renderer_parent.transform;
										//lineRenderer.SetColors(runPathColorEnd[invokeId], runPathColorEnd[invokeId]);
										Gradient gradient = new Gradient();
										gradient.SetKeys(
											new GradientColorKey[] { new GradientColorKey(runpath_color_end[invoke_id], 0.0f), new GradientColorKey(runpath_color_end[invoke_id], 1.0f) },
											new GradientAlphaKey[] { new GradientAlphaKey(0.2f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
										);
										line_renderer.colorGradient = gradient;
										line_renderer.positionCount = path.Count;
										for(int j=0 ; j<path.Count ; j++){
											line_renderer.SetPosition(j, new Vector3(path[j].x, 0.1f, path[j].y));
										}
										runpath_renderers.Add(line_renderer);

										if(invoked_agent[invoke_id].is_screen){
											GameObject screen_bar =  ((GameObject)Instantiate(Resources.Load("Prefabs/ScreenBar")));
											screen_bar.GetComponentInChildren<Image>().color = runpath_color_end[invoke_id];
											Vector3 screen_bar_position = new Vector3(path[path.Count-1].x, 0.1f, path[path.Count-1].y);
											screen_bar.transform.position = screen_bar_position;
											Vector3 target_direction = new Vector3(path[path.Count-1].x, 0.1f, path[path.Count-1].y) - new Vector3(path[path.Count-2].x, 0.1f, path[path.Count-2].y);
											//screen_bar.transform.rotation = Quaternion.Euler(Vector3.RotateTowards(Vector3.zero, target_direction, 360.0f, 5.0f));
											screen_bar.transform.rotation = Quaternion.LookRotation(target_direction, new Vector3(0.0f, 1.0f, 0.0f));
											screen_bar_prefabs.Add(screen_bar);
										}
										 
									}else{

									}
								}
							}
							#endregion
						}
					}
					current_state = Constants.ExecutorState.INVOKE_CURRENT_TIME_RUN_BAG;
					
					if(current_mode_is_learning)	
						ModeManagerLite.Instance._canvasContinue.SetActive(true);
					
					break;
				case Constants.ExecutorState.INVOKE_CURRENT_TIME_RUN_BAG:
					if(current_mode_is_learning){
						if(user_want_to_continue){
							ModeManagerLite.Instance._canvasContinue.SetActive(false);
							user_want_to_continue = false;

							if(invoked_agent[10].is_invoked){
								int passer_id = current_tactic.ballpasspairs[time_counter].passerId;
								int catcher_id = current_tactic.ballpasspairs[time_counter].catcherId;
								GameObject passer = players[passer_id-1].player;
								//Check whether the dribbling layer is invoked.
								
								if(players[passer_id-1].animator.GetLayerWeight(1) > 0.9f){
									players[passer_id-1].animator.SetTrigger("holdBall");
									players[passer_id-1].animator.SetLayerWeight(1, 0.0f);
									ball.GetComponent<BasketballMoniter>().is_dribbling_end = true;
								}

								//players[passer_id-1].animator.ResetTrigger("holdBall");
								players[passer_id-1].animator.SetTrigger("passBall");
							}

							for(int i=0;i<5;i++){
								if(invoked_agent[i].is_invoked){
									players[i].navmesh_waypoint.StartToMove();
								
									if(invoked_agent[i].path_type == 2){
										Animator player_anim = players[i].animator;
										player_anim.SetLayerWeight(1, 1.0f);
										player_anim.ResetTrigger("holdBall");
										player_anim.SetTrigger("tacticStart");
										ball.GetComponent<BasketballMoniter>().InvokeDribbling();
										ball.GetComponent<BasketballMoniter>().is_dribbling_end = false;
									}
								}	
							}
							
							current_state = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;
						}
					}else{
						if(invoked_agent[10].is_invoked){
							int passer_id = current_tactic.ballpasspairs[time_counter].passerId;
							int catcher_id = current_tactic.ballpasspairs[time_counter].catcherId;
							GameObject passer = players[passer_id-1].player;
							//Check whether the dribbling layer is invoked.
							
							if(players[passer_id-1].animator.GetLayerWeight(1) > 0.9f){
								players[passer_id-1].animator.SetTrigger("holdBall");
								players[passer_id-1].animator.SetLayerWeight(1, 0.0f);
								ball.GetComponent<BasketballMoniter>().is_dribbling_end = true;
							}

							//players[passer_id-1].animator.ResetTrigger("holdBall");
							players[passer_id-1].animator.SetTrigger("passBall");
						}

						for(int i=0;i<5;i++){
							if(invoked_agent[i].is_invoked){
								players[i].navmesh_waypoint.StartToMove();
							
								if(invoked_agent[i].path_type == 2){
									Animator player_anim = players[i].animator;
									player_anim.SetLayerWeight(1, 1.0f);
									player_anim.ResetTrigger("holdBall");
									player_anim.SetTrigger("tacticStart");
									ball.GetComponent<BasketballMoniter>().InvokeDribbling();
									ball.GetComponent<BasketballMoniter>().is_dribbling_end = false;
								}
							}	
						}
						
						current_state = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;				
					}
					
					break;
				case Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG:
					// Rotate the defender to target rotation
					#region Check whether each agent reach their destination
					for(int i=0 ; i<5 ; i++){
						if(invoked_agent[i].is_invoked){
							string handle = "Player" + (i + 1);

                            GameObject player = GameObject.FindGameObjectWithTag(handle);
                            NavMeshAgent player_agent = player.GetComponentInChildren<NavMeshAgent>();
                            Animator player_animator = player.GetComponentInChildren<Animator>();
							
							if(!player_agent.pathPending){
								if (player_agent.GetComponent<NavMeshWayPoint>()._reachEndPoint){
								//if ((player_agent.remainingDistance <= player_agent.stoppingDistance) || player_agent.GetComponent<NavMeshWayPoint>().reachEndPoint){
									invoked_agent[i].is_invoked = false;
									if(invoked_agent[i].is_screen){
										GameObject.FindGameObjectWithTag("Player"+(i+1)).GetComponent<Animator>().SetBool("isScreen", true);
									}
								}
							}
						}
					}
					#endregion

					bool is_agent_not_finish = false;
					// Check whether every invoked agent finished their task in this time step
                    if(current_mode_is_learning){
						for (int i = 0; i < 10; i++)
							is_agent_not_finish = is_agent_not_finish || invoked_agent[i].is_invoked;
					}
					else{
						for (int i = 0; i < 10; i++)
							is_agent_not_finish = is_agent_not_finish || invoked_agent[i].is_invoked;
					}
						
					if(is_agent_not_finish)
						current_state = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;
					else
						current_state = Constants.ExecutorState.CHECK_FINISH;
					
					break;
				case Constants.ExecutorState.CHECK_FINISH:

					//                           |---> Third Person Perspective 
					//       |--> Normal mode ---|                                    
					//       |                   |                                 |---> Donot need to pass ball
                    // When -|                   |---> First Person Perspective ---|                          
                    //       |                                                     |---> 1.Need to pass ball  2.Pass ball action active   
					//       |                                                       
					//       |                     |---> Third Person Perspective                                     
					//       |--> Learning mode ---|                                                          
					//                             |                                 |---> 1. Donot need to pass ball 2.Player face to correct direction 3.User wants to continue
                    //       					   |---> First Person Perspective ---|              						  
					//                                                               |---> 1. Need to pass ball 2.Player face to correct direction 3. Pass ball action active 4. User wants to continue
					//
                    // If all the conditions commit, the next step can execute.
					

					#region Invoke pass ball event
					//2019.01.26 Check when to invoke pass ball event
					//
					//  Normal   3PP X ---> PassBallSMBehaviour
					//  Normal   1PP X ---> PassBallSMBehaviour
					//  Normal   1PP O ---> 
					//  Learning 3PP X ---> PassBallSMBehaviour
					//  Learning 1PP X ---> PassBallSMBehaviour
					//  Learning 1PP O --->

					if(!is_pass_ball){
						if(!current_mode_is_learning && !current_perspective_is_3PP && need_to_pass_ball && activate_pass_ball){
							GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballMoniter>().InvokePassBall();
							is_pass_ball = true;
						}
						else if(current_mode_is_learning && !current_perspective_is_3PP && need_to_pass_ball && activate_pass_ball){
							GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballMoniter>().InvokePassBall();
							is_pass_ball = true;
						}
					}
					#endregion


					//2019.01.25 New conditions: need_to_pass_ball, activate_pass_ball
					if( ( !current_mode_is_learning && ( current_perspective_is_3PP || 
					                                   ( !current_perspective_is_3PP && ( !need_to_pass_ball || 
													                                    ( need_to_pass_ball && activate_pass_ball ) )) ) ) || 
					    (  current_mode_is_learning && ( (current_perspective_is_3PP && user_want_to_continue )|| 
						                               ( !current_perspective_is_3PP && ( (!need_to_pass_ball && player_direction_is_right && user_want_to_continue) || 
													                                      (need_to_pass_ball && player_direction_is_right && activate_pass_ball && user_want_to_continue))))))
					{
						current_state = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
						time_counter++;
						is_pass_ball = false;
						ModeManagerLite.Instance._canvasContinue.SetActive(false);
					}

					/*
					if( (!current_mode_is_learning) || (current_mode_is_learning && user_want_to_continue &&
							  (current_perspective_is_3PP || (!current_perspective_is_3PP && player_direction_is_right)))){
						current_state = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
						time_counter++;
					}
					*/

					if(time_counter > max_time){
						current_state = Constants.ExecutorState.LAST_THROW;
						is_pass_ball = false;
					}
					break;
				case Constants.ExecutorState.LAST_THROW:
					
					players[current_ball_holder-1].player_movement.SmoothLookAt(GameObject.FindGameObjectWithTag("hoop"));
					if(players[current_ball_holder-1].animator.GetLayerWeight(1) > 0.9f){
						players[current_ball_holder-1].animator.SetTrigger("holdBall");
						players[current_ball_holder-1].animator.SetLayerWeight(1, 0.0f);
						ball.GetComponent<BasketballMoniter>().is_dribbling_end = true;
					}
					Vector3 passer_position = players[current_ball_holder-1].player.transform.localPosition;
					passer_position.y = 2.0f;
					// 2019.02.01 Slightly forward to generate more accurate ball trajectory begin position (closer to hand position)
					//passer_position += players[current_ball_holder-1].player.transform.forward * 0.5f;

					ball.GetComponent<BasketballMoniter>().GenerateThrowBallTrajectory(passer_position, ModeManagerLite.Instance._hoopPosition);
					players[current_ball_holder-1].animator.SetTrigger("throwBall");
					current_state = Constants.ExecutorState.END;
					break;
				case Constants.ExecutorState.END:
					
					//TODO:Re-enable state gracefully
					ModeManagerLite.Instance._canvasContinue.SetActive(false);
					ModeManagerLite.Instance._tacticIsExecuting = false;
					
					StopCoroutine(coroutine);
					coroutine = null;
					break;	
			}

			yield return new WaitForSeconds(update_rate);
		}
	}

	public void ClearRunPathList(){
		for(int i=0;i<runpath_renderers.Count;i++){
			Destroy(runpath_renderers[i]);
		}
		runpath_renderers.Clear();
		for(int i=0;i<screen_bar_prefabs.Count;i++){
			Destroy(screen_bar_prefabs[i]);
		}
		screen_bar_prefabs.Clear();
	}

	public bool CheckReadyForPassBall(){
		bool is_ready = false;
		bool current_mode_is_learning = (ModeManagerLite.Instance._mode == Constants.Mode.LEARNING) ? true : false;
		bool current_perspective_is_3PP = (ModeManagerLite.Instance._perspective == Constants.Perspective.THIRD_PP) ? true : false;
		
		if(!current_mode_is_learning && current_perspective_is_3PP){
			is_ready = true;
		}
		else if(!current_mode_is_learning &&  !current_perspective_is_3PP && !need_to_pass_ball){
			is_ready = true;
		}
		else if(current_mode_is_learning && current_perspective_is_3PP){
			is_ready = true;
		}
		else if(current_mode_is_learning &&  !current_perspective_is_3PP && !need_to_pass_ball ){
			is_ready = true;
		}
		return is_ready;
	}

	public void OnTriggerContinue(bool is_trigger){
		if(ModeManagerLite.Instance._canvasContinue.activeInHierarchy){
			user_want_to_continue = true;
		}
	}

	public void ActivatePassBall(bool is_activate){
		if(current_state == Constants.ExecutorState.CHECK_FINISH){	
			if(!activate_pass_ball){
				activate_pass_ball = is_activate;
			}
		}
	}
	
	
}
