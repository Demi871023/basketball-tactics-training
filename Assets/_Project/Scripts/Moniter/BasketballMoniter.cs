﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BasketballMoniter : MonoBehaviour {
	
	public int time_counter;
	public bool is_dribbling_end;
	private const float dribbling_ball_update_rate = 0.0005f;
	private int ball_counter;
	private float previous_hand_height = 0.0f;
	private AudioSource basket_audio_source;

	#region Field : throw ball parameters
	public float h_throw_strength = 3.0f;
	public float v_throw_strength = 10.0f;
	[Tooltip("視覺化投球軌跡")]
	public LineRenderer sight_line;
	private const int throw_trajectory_count = 20;
	private int modified_throw_count;
	private const float throw_ball_update_rate = 0.075f;
	private Vector3[] throw_ball_trajectory;
	private Vector3 last_velocity;
	private Collider hit_object;
	public Collider hitObject { get { return hit_object; } }
	#endregion

	#region Field : pass ball parameters 
	private const int pass_trajectory_count = 69;
	private const float pass_ball_update_rate = 0.007f;
	private Vector3[] pass_ball_trajectory;
	private List<LineRenderer> ball_path_renderers;
	private List<GameObject> ball_path_ghost;
	private GameObject baskeball_renderer;
	#endregion
	
	// Update is called once per frame
	void Start () {
		ball_path_renderers = new List<LineRenderer>();
		ball_path_ghost = new List<GameObject>();
		basket_audio_source = GetComponent<AudioSource>();
		
		Transform[] children = this.GetComponentsInChildren<Transform>(true);
        foreach(Transform child in children) {
			if(child.name == "Basketball_with_skel 1"){
				baskeball_renderer = child.gameObject;
			}
		}
	}

	public void InvokePassBall(){
		ball_counter = 0;
		InvokeRepeating("UpdatePassBallPosition", 0.0f, pass_ball_update_rate);
	}

	private void UpdatePassBallPosition(){
		if(TacticExecuterMoniter.Instance.invoked_agent[10].is_invoked && ball_counter < pass_trajectory_count){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;

			int catcher_id = current_tactic.ballpasspairs[time_counter].catcherId;
			GameObject catcher = GameObject.FindGameObjectWithTag("Player" + current_tactic.ballpasspairs[time_counter].catcherId);
			GameObject passer = GameObject.FindGameObjectWithTag("Player" + current_tactic.ballpasspairs[time_counter].passerId);

			// If current catcher is not the user's current perspective
			if (ModeManagerLite.Instance._perspective != (Constants.Perspective)catcher_id){
				//catcher.GetComponent<PlayerMovement>().SmoothLookAt(passer);
			}
			/*
			if (ballCounter % 10 == 0)
			{
				GameObject ballPassHint = (GameObject)Instantiate(Resources.Load("Prefabs/BallPassHint"));
				ModeManagerLite.Instance.ballpasshints.Add(ballPassHint);
				ballPassHint.transform.position = new Vector3(ballTrajectory[ballCounter].x, 0.1f, ballTrajectory[ballCounter].z);
				ballPassHint.transform.localRotation = Quaternion.LookRotation((catcher.transform.position - passer.transform.position) * (-1));
			}
			 */
			this.transform.parent = null;
			this.transform.position = pass_ball_trajectory[ball_counter];
			this.transform.Rotate(new Vector3(0.0f, 0.0f, -20.0f), Space.Self);

			ball_counter++;
		}

		if(TacticExecuterMoniter.Instance.invoked_agent[10].is_invoked && ball_counter == pass_trajectory_count){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;
			
			GameObject catcher = TacticExecuterMoniter.Instance.players[current_tactic.ballpasspairs[time_counter].catcherId-1].player;
			TacticExecuterMoniter.Instance.players[current_tactic.ballpasspairs[time_counter].catcherId-1].animator.SetTrigger("holdBall");

			Transform target_transform = TacticExecuterMoniter.Instance.players[current_tactic.ballpasspairs[time_counter].catcherId-1].animator.GetBoneTransform(HumanBodyBones.RightHand);
			this.transform.SetParent(target_transform);
			this.transform.localPosition = new Vector3(-0.038f, 0.05f, 0.027f);
			this.transform.localRotation = Quaternion.identity;
			this.transform.localScale = new Vector3(0.42f, 0.42f, 0.42f);

			TacticExecuterMoniter.Instance.invoked_agent[10].is_invoked = false;
			CancelInvoke("UpdatePassBallPosition");
		}
	}

	public void GeneratePassBallTrajectory(Vector3 start_position, Vector3 target_position) {
        pass_ball_trajectory = new Vector3[pass_trajectory_count];
        pass_ball_trajectory[0] = start_position;
        Vector3 velocity = (target_position - start_position)/pass_trajectory_count;

        for (int i = 1; i < pass_trajectory_count; ++i)
        {
            pass_ball_trajectory[i] = pass_ball_trajectory[i - 1] + velocity;
        }

		LineRenderer line_renderer =  ((GameObject)Instantiate(Resources.Load("Prefabs/PlayerRunPathRenderer"))).GetComponent<LineRenderer>();
		//lineRenderer.transform.parent = runPathRendererParent.transform;
		Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(new Color32(165,0,0,255), 0.0f), new GradientColorKey(new Color32(165,0,0,255), 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.2f), new GradientAlphaKey(1.0f, 1.0f) }
		);
		line_renderer.colorGradient = gradient;
		line_renderer.positionCount = pass_trajectory_count;
		for(int i=0 ; i<pass_trajectory_count ; i++){
			line_renderer.SetPosition(i, new Vector3(pass_ball_trajectory[i].x, 0.1f, pass_ball_trajectory[i].z));

			if(i%10 == 0){
				//GameObject ghost_ball = (GameObject)Instantiate(Resources.Load("Prefabs/GhostBasketball"));
				//ghost_ball.transform.position = pass_ball_trajectory[i];
				//ball_path_ghost.Add(ghost_ball);
			}
		}
		ball_path_renderers.Add(line_renderer);
		 
    }


	public void InvokeThrowBall(){
		ball_counter = 0;
		this.transform.SetParent(null);
		InvokeRepeating("UpdateThrowBallPosition", 0.0f, throw_ball_update_rate);
	}

	private void UpdateThrowBallPosition(){
		if(ball_counter < modified_throw_count){
			
			this.transform.position = throw_ball_trajectory[ball_counter];
			this.transform.Rotate(new Vector3(0.0f, 0.0f, -20.0f), Space.Self);
			ball_counter++;
		}
		//Debug.Log(modified_throw_count);
		if(ball_counter == modified_throw_count){
			this.GetComponent<Rigidbody>().velocity = last_velocity;
			this.GetComponent<Rigidbody>().useGravity = true;
			this.GetComponent<Rigidbody>().isKinematic = false;
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			CancelInvoke("UpdateThrowBallPosition");
		}
		
	}

	public void InvokeDribbling(){
		InvokeRepeating("UpdateDribblingBallPosition", 0.0f, dribbling_ball_update_rate);
	}

	private void UpdateDribblingBallPosition(){
		for(int i=0;i<5;i++){
            
            if(TacticExecuterMoniter.Instance.invoked_agent[i].is_dribbling){

                //Player (i+1) is dribbling the ball
                GameObject player = TacticExecuterMoniter.Instance.players[i].player;
                GameObject ball = this.gameObject;
				this.transform.parent = null;
				this.GetComponent<Rigidbody>().isKinematic = false;

                Vector3 player_hand_position = player.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform.position;
                if(previous_hand_height != 0.0f){// Skip the first access
                    // Current Hand is higher than the prev ---> the hand is moving up
                    float delta_height = player_hand_position.y - previous_hand_height;
					//Debug.Log(delta_height);
					//Debug.Log(player_hand_position.y);
					
					
					if( delta_height > 0.0f){
						//Hand is going up
                        float moving_distance = player_hand_position.y - 1.71f;
                        float moving_percent = moving_distance/0.4f;
						// the maximum of player hand moving distance 0.4f
						// the lowest position hand can be 1.71f
                       
                        Vector3 new_ball_position = new Vector3(player_hand_position.x, Mathf.Clamp(0.1f + Mathf.Abs(delta_height)*0.1f, 0.1f, 1.71f), player_hand_position.z);
                        ball.transform.position =  new_ball_position;
                    }
                    else{
						//Hand is going down
                        float moving_distance = 2.11f - player_hand_position.y;
						// the highest position hand can be 2.11f
                        float moving_percent = moving_distance/0.4f;

                        Vector3 new_ball_pos = new Vector3(player_hand_position.x, Mathf.Clamp(1.5f - Mathf.Abs(delta_height)*0.1f, 0.1f, 1.71f), player_hand_position.z);
                        ball.transform.position =  new_ball_pos;
                    }
					
                }
                else{
                    ball.transform.position = player_hand_position;
                }
				

                //Debug.LogWarning(ball.transform.position.y);
                previous_hand_height = player_hand_position.y;
                //ball.transform.position = playerHandPos - new Vector3(0.0f, 0.5f, 0.0f); 
				if(is_dribbling_end){
					this.transform.parent = TacticExecuterMoniter.Instance.players[i].player.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform;
					//this.transform.parent = player.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform;
					this.transform.localPosition = new Vector3(-0.038f, 0.05f, 0.027f);
					this.GetComponent<Rigidbody>().isKinematic = true;
					previous_hand_height = 0.0f;
					CancelInvoke("UpdateDribblingBallPosition");
				}
			}
        }
	}

	public void GenerateThrowBallTrajectory(Vector3 start_position, Vector3 target_position){
		throw_ball_trajectory = new Vector3[throw_trajectory_count];
		Vector3 distance = target_position - start_position;
		distance.y = 0.0f;
		throw_ball_trajectory[0] = start_position + distance.normalized* 0.4f;

		Vector3 v_segment_velocity = (TacticExecuterMoniter.Instance.players[TacticExecuterMoniter.Instance.current_ball_holder-1].player.transform.up * v_throw_strength);
		float segment_time = 0.1f;//(segment_velocity.sqrMagnitude != 0) ? segment_scale / segment_velocity.magnitude : 0;
		
		Vector3 h_segment_velocity = (distance/segment_time).normalized;
		bool has_found_trajectory = false;
		int temp = 1;

		float msg = 10.0f;
		for(float s = 10.0f; s>=0.0f ; s=s-0.2f){
			Vector3  segment_velocity = v_segment_velocity + h_segment_velocity * s;
			//Debug.Log("Magnitude:" + (segment_velocity * segment_time).magnitude);
			for (int i = 1; i < throw_trajectory_count; i++){
				segment_velocity = segment_velocity + Physics.gravity * segment_time;

				float distance_to_basket = Vector3.Distance(throw_ball_trajectory[i - 1] + segment_velocity * segment_time, ModeManagerLite.Instance._hoopPosition);
				if(distance_to_basket < 0.5f){
					has_found_trajectory = true;
					break;
				}
				else{
					throw_ball_trajectory[i] = throw_ball_trajectory[i-1] + segment_velocity * segment_time;
					last_velocity = segment_velocity.normalized*2.0f;
				}
				temp = i;
			}
		
			msg =  s;
			//Debug.Log(temp+"  "+s + "," + has_found_trajectory);
			if(has_found_trajectory){
				Debug.Log("Throw ball s : " + s);
				break;
			}
		 
		}
		Debug.Log("Final Throw ball s : " + msg);
		
		/*
		Vector3  segment_velocity = v_segment_velocity + h_segment_velocity * h_throw_strength;
			
		for (int i = 1; i < throw_trajectory_count; i++){
			segment_velocity = segment_velocity + Physics.gravity * segment_time;
			float segment_scale = segment_velocity.magnitude;
			//Debug.Log("Scale:"+segment_scale+",velocity:"+segment_velocity.normalized);
			RaycastHit hit;
			hit_object = null;

			//(segment_velocity * segment_time).magnitude)
			float distance_to_basket = Vector3.Distance(throw_ball_trajectory[i - 1] + segment_velocity * segment_time, ModeManagerLite.Instance.hoop_position);
			if(distance_to_basket < 0.2f){
				has_found_trajectory = true;
				break;
			}
			else{
				throw_ball_trajectory[i] = throw_ball_trajectory[i-1] + segment_velocity * segment_time;
				last_velocity = segment_velocity.normalized*2.0f;
			}
			
			temp = i;
		}
		 */

		modified_throw_count = temp;

		
		#region Visualization of throw ball trajectory
		
		Color start_color = Color.red;
		Color end_color = start_color;
		start_color.a = 0;
		end_color.a = 1;
		sight_line.startColor = start_color;
		sight_line.endColor = end_color;

        int modified_count = temp;//throw_trajectory_count;
        for(int i=0 ; i < throw_trajectory_count ; i++){
            if(throw_ball_trajectory[i].magnitude < 0.1f){
                modified_count = i;
                break;
            }
        }
		modified_throw_count = temp;
		sight_line.positionCount = temp;
		for (int i = 0; i < temp; i++){
			sight_line.SetPosition(i, throw_ball_trajectory[i]);
        }
		

		#endregion
		
	}

	public void ClearBallPath(){
		for(int i=0;i<ball_path_renderers.Count;i++){
			Destroy(ball_path_renderers[i]);
		}
		ball_path_renderers.Clear();

		for(int i=0;i<ball_path_ghost.Count;i++){
			Destroy(ball_path_ghost[i]);
		}
		ball_path_ghost.Clear();
	}

	/** 
	// 2018.01.04
	// InputManager偵測到使用者做出傳球動作  --> 傳球動作無效(3PP/1PP但現在不用傳球)
	//                                    |  
	//                                     --> 傳球動作有效 ---> 使用者看對方向 ---> 讓戰術可以繼續執行
	//                                                     |
	//													   ---> 使用者看錯方向
	*/

	public void ChangeBallAttachment(GameObject attach_place, Vector3 offset, Vector3 rotation){
		this.transform.parent = null;
		this.transform.parent = attach_place.transform;
		this.transform.localPosition = offset;
		//this.transform.localRotation = Quaternion.Euler(rotation);
	}

	public void AttachBallToHolder(){
		if(TacticExecuterMoniter.Instance.current_ball_holder != 0){
			Animator ball_holder_animator = GameObject.FindGameObjectWithTag("Player" + (TacticExecuterMoniter.Instance.current_ball_holder)).GetComponentInChildren<Animator>();
			Transform ball_transform = this.transform;
			this.transform.SetParent(ball_holder_animator.GetBoneTransform(HumanBodyBones.RightHand));
			// The paremeter is properly setting in the scene.
			ball_transform.localPosition = new Vector3(-0.038f, 0.05f, 0.027f);
			ball_transform.localRotation = Quaternion.identity;
			ball_transform.localScale = new Vector3(0.42f, 0.42f, 0.42f);
		}
	}

	public void SetTransparent(float value){
		Color32 col = baskeball_renderer.GetComponent<SkinnedMeshRenderer>().material.GetColor("_Color");
 		col.a = (byte)value;
		baskeball_renderer.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Color", col);
	}

	public void OnCollisionEnter(Collision collision){
		if(collision.collider.name == "Floor"){
			basket_audio_source.Play();
		}
	}
}
