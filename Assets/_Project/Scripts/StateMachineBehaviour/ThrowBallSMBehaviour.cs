﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using wvr;

public sealed class ThrowBallSMBehaviour : StateMachineBehaviour {
	public AudioClip _throwBallClip;
	private bool _hasThrowBall = false;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		_hasThrowBall = false;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if(stateInfo.normalizedTime > 0.3f){
			if(!_hasThrowBall){
				if(GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>() != null){
					GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>().InvokeThrowBall();
				}
				else{
					GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballMoniter>().InvokeThrowBall();
				}
				//GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>().InvokeThrowBall();
				animator.gameObject.GetComponent<AudioSource>().clip = _throwBallClip;
				animator.gameObject.GetComponent<AudioSource>().Play();
				_hasThrowBall = true;
			}
		}	
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
