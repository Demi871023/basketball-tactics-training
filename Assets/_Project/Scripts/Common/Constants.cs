﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants{
	public static float INIITIAL_HEIGHT = 0.5f;
	public static float THREE_LEFT_TOP_X = -9.92f;
    public static float THREE_LEFT_TOP_Y = 19.95f;
    public static float THREE_RIGHT_TOP_X = 9.94f;
    public static float THREE_RIGHT_TOP_Y = 19.95f;
    public static float THREE_CENTER_X = 0.0f;
    public static float THREE_CENTER_Y = 0.0f;

	public enum SceneName { 
		START, 
		EDITOR,
		GODVIEW,
		TUTORIAL,
		VR, 
	};
	public enum TacticCategory
	{
		THREE_PT, 
		PICK_AND_FADE_3PT, 
		PICK_AND_FADE_MID, 
		MID_RANGE, 
		CUTTER, 
		HANDOFF, 
		GUARD_POST_UP, 
		POST_UP_LOW, 
		POST_UP_HIGH,
		ISOLIATION, 
		PICK_AND_ROW, 
		PICK_AND_ROW_OPTION, 
		THROW_IN,  
		OTHERS, 
		EXPERIMENT
	};
	public enum PlayerPosition{ 
		POINT_GUARD, 
		SHOOTING_GUARD, 
		SMALL_FORWARD, 
		POWER_FORWARD, 
		CENTER 
	};
	public enum ExecutorState
    {
        START,
		LEARNING_START,
        CHECK_CURRENT_TIME_RUN_BAG,
		INVOKE_CURRENT_TIME_RUN_BAG,
        HMM_DEFENDER_UPDATE,
        EXECUTE_CURRENT_TIME_RUN_BAG,
		CHECK_FINISH,
        LAST_THROW,
        END
    };
	public enum Perspective { 
		THIRD_PP, 
		FIRST_PP_1, 
		FIRST_PP_2, 
		FIRST_PP_3, 
		FIRST_PP_4, 
		FIRST_PP_5 
	};
    public enum Mode { 
		NORMAL, 
		LEARNING 
	};
    public enum Defender { 
		NO_DEFENDER, 
		MODE1_DEFENDER
	};
	public enum GodViewPosition{
		LEFT,
		LEFT_45,
		CENTER,
		RIGHT_45,
		RIGHT
	};
	public enum Direction{
		LEFT,
		RIGHT
	}
	public enum Controller{
		DEFAULT,
		VIVE_CONTROLLER,
		LEAP_MOTION,
		NOITOM
	}
	public enum PlayerRace{
		DEFAULT,
		UMA
	}
}
