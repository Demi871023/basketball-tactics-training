﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MyTools{
    public List<Vector2> GenerateSpringPoints(Vector2 startPoint, Vector2 endPoint, double w, int n){
        // ref: https://stackoverflow.com/questions/35777581/drawing-a-zigzag-spring-in-java
        List<Vector2> points = new List<Vector2>();
        
        // vector increment
        double inv = 0.25/(double)n;
        Vector2 d = (endPoint - startPoint) * Convert.ToSingle(inv);

        // prependicular direction
        double inv2 = (Math.Abs(Mathf.Sqrt(d.x*d.x + d.y*d.y)) < float.Epsilon)? 0.0 : w/Mathf.Sqrt(d.x*d.x + d.y*d.y);
        Vector2 p = new Vector2(d.y * Convert.ToSingle(inv2), - d.x * Convert.ToSingle(inv2));

        Vector2 point = startPoint;
        for(int i=0 ; i<n*3 ; i++){
            points.Add(point);
            points.Add(point+d+p);
            points.Add(new Vector2(point.x + 3.0f *d.x - p.x, point.y + 3.0f * d.y - p.y));            
            point += 4.0f * d;
        }
        return points;
    }
}
