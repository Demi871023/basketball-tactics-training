﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyCamera : MonoBehaviour {
 
    /*
    Writen by Windexglow 11-13-10.  Use it, edit it, steal it I don't care.  
    Converted to C# 27-02-13 - no credit wanted.
    Simple flycam I made, since I couldn't find any others made public.  
    Made simple to use (drag and drop, done) for regular keyboard layout  
    wasd : basic movement
    shift : Makes camera accelerate
    space : Moves camera on X and Z axis only.  So camera doesn't gain any height*/
     
     
    float MAIN_SPEED = 100.0f; //regular speed
    float SHIFT_ADD = 250.0f; //multiplied by how long shift is held.  Basically running
    float MAX_SHIFT = 1000.0f; //Maximum speed when holdin gshift
    float CAM_SENS = 0.25f; //How sensitive it with mouse
    private Vector3 _lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
    private float _totalRun= 1.0f;
     
    void Update () {
        _lastMouse = Input.mousePosition - _lastMouse ;
        _lastMouse = new Vector3(-_lastMouse.y * CAM_SENS, _lastMouse.x * CAM_SENS, 0 );
        _lastMouse = new Vector3(transform.eulerAngles.x + _lastMouse.x , transform.eulerAngles.y + _lastMouse.y, 0);
        transform.eulerAngles = _lastMouse;
        _lastMouse =  Input.mousePosition;
        //Mouse  camera angle done.  
       
        //Keyboard commands
        float f = 0.0f;
        
        Vector3 p = GetBaseInput();
        if (Input.GetKey (KeyCode.LeftShift)){
            _totalRun += Time.deltaTime;
            p  = p * _totalRun * SHIFT_ADD;
            p.x = Mathf.Clamp(p.x, -MAX_SHIFT, MAX_SHIFT);
            p.y = Mathf.Clamp(p.y, -MAX_SHIFT, MAX_SHIFT);
            p.z = Mathf.Clamp(p.z, -MAX_SHIFT, MAX_SHIFT);
        }
        else{
            _totalRun = Mathf.Clamp(_totalRun * 0.5f, 1f, 1000f);
            p = p * MAIN_SPEED;
        }
       
        p = p * Time.deltaTime;
       Vector3 newPosition = this.transform.position;
        if (Input.GetKey(KeyCode.Space)){ //If player wants to move on X and Z axis only
            this.transform.Translate(p);
            newPosition.x = this.transform.position.x;
            newPosition.z = this.transform.position.z;
            this.transform.position = newPosition;
        }
        else{
            this.transform.Translate(p);
        }
       
    }
     
    private Vector3 GetBaseInput() { //returns the basic values, if it's 0 than it's not active.
        Vector3 playerVelocity = new Vector3();
        if (Input.GetKey (KeyCode.W)){
            playerVelocity += new Vector3(0, 0 , 1);
        }
        if (Input.GetKey (KeyCode.S)){
            playerVelocity += new Vector3(0, 0, -1);
        }
        if (Input.GetKey (KeyCode.A)){
            playerVelocity += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey (KeyCode.D)){
            playerVelocity += new Vector3(1, 0, 0);
        }
        return playerVelocity;
    }
}
