﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class SSAA : MonoBehaviour {
	public int antiAliasing = 2;
	// Use this for initialization
	void Start () {
		XRSettings.eyeTextureResolutionScale = 1.4f; // tune this value between 1.4-2.0
		QualitySettings.antiAliasing = antiAliasing;
	}
}
