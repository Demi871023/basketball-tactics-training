﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ShootSoundTrigger : MonoBehaviour {
	private AudioSource shoot_audio_source;

	// Use this for initialization
	void Start () {
		shoot_audio_source = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider collider)	{
		if(collider.CompareTag("Basketball")){
			shoot_audio_source.Play();
		}
	}
}
