using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class New_MenuUI : MonoBehaviour
{
    [SerializeField] private Text _textTactic;
	[SerializeField] private TextMeshProUGUI _textTacticName;

    public Toggle View_3PP_Toggle, View_1PP_Toggle;
    public Dropdown View_1PP_PlayerDDL;

    public Toggle Defender_Disable_Toggle, Defender_Enable_Toggle;
    // Start is called before the first frame update

    public GameObject IntroCanvas, SettingCanvas, ControlStateCanvas;
    public GameObject ViewHint, DefenderHint, TacticHint;

    public Button StartButton, PauseButton;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickStart()
    {
        IntroCanvas.SetActive(false);
        SettingCanvas.SetActive(true);
    }

    public void OnClickViewHint()
    {
        ViewHint.SetActive(!ViewHint.activeSelf);
    }

    public void OnClickDefenderHint()
    {
        DefenderHint.SetActive(!DefenderHint.activeSelf);
    }

    public void OnClickTacticHint()
    {
        TacticHint.SetActive(!TacticHint.activeSelf);
    }


    public void OnTriggerPerspective()
    {
        // value:0 → 3PP, value:1 → 1PP (預設 player1)
		
        if(!ModeManagerLite.Instance._tacticIsExecuting){
            int new_perspecitve = 0;

            if(View_3PP_Toggle.isOn)
            {
                new_perspecitve = 0;
                View_1PP_PlayerDDL.interactable = false;
            }
            else if(View_1PP_Toggle.isOn)
            {
                new_perspecitve = 1;
                View_1PP_PlayerDDL.interactable = true;
            }

            ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)new_perspecitve);
        }
	}


    public void OnTriggerPlayerPerspective(int value)
    {
        // value:1 → player1, value:2 → player2
		
        if(!ModeManagerLite.Instance._tacticIsExecuting){
            int new_perspecitve = value + 1;
            ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)new_perspecitve);
        }
	}

    public void OnTriggerDefender(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
            int new_defender = 0;

            if(Defender_Disable_Toggle.isOn)
                new_defender = 0;
            else if(Defender_Enable_Toggle.isOn)
                new_defender = 1;

			ModeManagerLite.Instance.ChangeDefenderMode((Constants.Defender)new_defender);
		}
	}


    


    public void OnSelectNextTactic(int value)
    {
        Debug.Log(value);

        if(!ModeManagerLite.Instance._tacticIsExecuting){
			// TacticManager.Instance._currentSelectIndex++;
			// if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;

            TacticManager.Instance._currentSelectIndex = value;

			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterLite.Instance.OnSelectedTactic();
			// _textTactic.text = TacticManager.Instance._currentSelectIndex.ToString();
			// _textTacticName.text = TacticManager.Instance._currentChosenTactic.tacticName;

			// ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			// _imgPerspective.sprite = _spritePerspective[0];
			ModeManagerLite.Instance._canvasEyeblink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();

		}
    }



    public void OnTriggerTrainingPause()
    {
        StartButton.interactable = true;
        PauseButton.interactable = false;
        Time.timeScale = 0;
    }


    public void OnTriggerTrainingGoing()
    {
        StartButton.interactable = false;
        PauseButton.interactable = true;
        Time.timeScale = 1;
    }


}
