﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Refactor{
    public class ThrowBallSMBehaviour : StateMachineBehaviour
    {
        private bool _hasThrowBall;
        private AudioSource _playerAudioSource;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetLayerWeight(layerIndex, 1.0f);
            _playerAudioSource = animator.gameObject.GetComponent<AudioSource>();
            _playerAudioSource.clip = Resources.Load("Audios/BallPasserorShoot") as AudioClip;
            _hasThrowBall = false;
            
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if(stateInfo.normalizedTime > 0.3f){
			if(!_hasThrowBall){
				if(GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>() != null){
					GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>().InvokeThrowBall();
				}
				animator.gameObject.GetComponent<AudioSource>().Play();
				_hasThrowBall = true;
			}
		}	
	}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetLayerWeight(layerIndex, 0.0f);
        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}
    }
}