﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Refactor{
    public class PassBallSMBehaviour : StateMachineBehaviour
    {
        private AudioSource _playerAudioSource;
        private bool _isPassBall;
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetLayerWeight(layerIndex, 1.0f);
            _isPassBall = false;
            _playerAudioSource = animator.gameObject.GetComponent<AudioSource>();
            _playerAudioSource.clip = Resources.Load("Audios/BallPassorShoot") as AudioClip;
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if(!_isPassBall){
			if(TacticExecuterLite.Instance.CheckReadyForPassBall()){
				if(stateInfo.normalizedTime > 0.01f){
					if(GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>() != null){
						GameObject.FindGameObjectWithTag("Basketball").GetComponent<BasketballLite>().InvokePassBall();
					}
					
					animator.gameObject.GetComponent<AudioSource>().Play();
					_isPassBall = true;
				}
			}
		}
	}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetLayerWeight(layerIndex, 0.0f);
        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}
    }
}