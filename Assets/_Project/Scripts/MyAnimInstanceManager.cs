﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAnimInstanceManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject defenders_parent;
    public AnimationInstancing.AnimationInstance instancing;
    void Start()
    {
        LoadAB();
        AnimationInstancing.AnimationInstancingMgr.Instance.UseInstancing = true;
        defenders_parent.SetActive(true);
        TacticExecuterLite.Instance.SetDefenderRef();
        
    }

    void LoadAB()
    {
        StartCoroutine(AnimationInstancing.AnimationManager.Instance.LoadAnimationAssetBundle(Application.streamingAssetsPath + "/AssetBundle/animationtexture"));
    }

    // Test
    void Update(){
        if(Input.GetKeyDown(KeyCode.V)){
            //instancing.PlayAnimation("20181017-2_Char00");
            instancing.CrossFade("20181017-2_Char00", 0.2f);
        }
    }
}
