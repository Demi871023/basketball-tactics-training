﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RingSoundTrigger : MonoBehaviour {
	private AudioSource _ringAudioSource;

	// Use this for initialization
	void Start () {
		_ringAudioSource = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider collider)	{
		if(collider.CompareTag("Basketball")){
			_ringAudioSource.Play();
		}
	}
}
