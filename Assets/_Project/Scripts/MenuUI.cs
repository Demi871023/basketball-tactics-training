using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuUI : MonoBehaviour
{
    [SerializeField] private Text _textTactic;
	[SerializeField] private TextMeshProUGUI _textTacticName;
	[SerializeField] private GameObject _canvasSetting;
	[SerializeField] private Image _imgPerspective;
	[SerializeField] private Image _imgMode;
	[SerializeField] private Image _imgDefender;

	[SerializeField] private Sprite[] _spritePerspective;
	[SerializeField] private Sprite[] _spriteMode;
	[SerializeField] private Sprite[] _spriteDefender;
    private Canvas _canvasSettingCanvas;

    private void Awake(){
		_canvasSettingCanvas = _canvasSetting.GetComponent<Canvas>();
	}

    public void HoldBallVibration(){
		if(TacticExecuterLite.Instance._currentBallHolder == (int)ModeManagerLite.Instance._perspective){
			if(ModeManagerLite.Instance._tacticIsExecuting){
				//if(!TacticExecuterLite.Instance._invokedAgent[TacticExecuterLite.Instance._currentBallHolder-1]._isDribbling)	
				//	WaveVR_Controller.Input(device).TriggerHapticPulse(50, WVR_InputId.WVR_InputId_Alias1_Touchpad);
			}
		}
	}

	public void OnSelectNextTactic(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			TacticManager.Instance._currentSelectIndex++;
			if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterLite.Instance.OnSelectedTactic();
			_textTactic.text = TacticManager.Instance._currentSelectIndex.ToString();
			_textTacticName.text = TacticManager.Instance._currentChosenTactic.tacticName;

			ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			_imgPerspective.sprite = _spritePerspective[0];
			ModeManagerLite.Instance._canvasEyeblink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();

		}
	}

	public void OnTriggerPerspective(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_perspecitve = (int)ModeManagerLite.Instance._perspective + 1;
			if(new_perspecitve > (int)Constants.Perspective.FIRST_PP_5) new_perspecitve = 0;
			ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)new_perspecitve);
			_imgPerspective.sprite = _spritePerspective[new_perspecitve];
		}
	}

	public void OnTriggerMode(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_mode = (int)ModeManagerLite.Instance._mode + 1;
			if(new_mode > (int)Constants.Mode.LEARNING) new_mode = 0;
			ModeManagerLite.Instance._mode = (Constants.Mode)new_mode;
			_imgMode.sprite = _spriteMode[new_mode];

			//Tactic3DExecuter.Instance.learning_pass_hint.SetActive(!Tactic3DExecuter.Instance.learning_pass_hint.gameObject.activeInHierarchy);
		}
	}

	public void OnTriggerDefender(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_defender = (int)ModeManagerLite.Instance._defender + 1;
			if(new_defender > (int)Constants.Defender.MODE1_DEFENDER) new_defender = 0;
			ModeManagerLite.Instance.ChangeDefenderMode((Constants.Defender)new_defender);
			_imgDefender.sprite = _spriteDefender[new_defender];
		}
	}

	public void OnChangeStair(bool is_up){
		GameObject inputManager = GameObject.FindGameObjectWithTag("InputManager");
		if(is_up){
			if(inputManager.GetComponent<CameraController>().root_height < 6.0f){
				inputManager.GetComponent<CameraController>().root_height = 7.0f;
				ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			}
			_imgPerspective.sprite = _spritePerspective[0];
		}
		else{
			if(inputManager.GetComponent<CameraController>().root_height > 1.0f){
				inputManager.GetComponent<CameraController>().root_height = Constants.INIITIAL_HEIGHT;
				ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			}
			_imgPerspective.sprite = _spritePerspective[0];
		}
	}

    
}
