﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using System.IO;

public class TacticReceiver : Singleton<TacticReceiver>
{
    public bool has_new_tactic = false;
    public Tactic new_tactic;
    public StringBuilder sb = new StringBuilder();

    public string current_receive_json;

    public void Start() {

        this.StartServer();
        System.Threading.Thread.Sleep(10);

    }

	private string localIP;
    /*
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    */

    public void Update()
    {
        if (has_new_tactic == true)
        {
            TacticManager.Instance._currentChosenTactic = new_tactic;
			TacticExecuterLite.Instance.OnSelectedTactic();
			
            has_new_tactic = false;
        }
    }

    public enum TestMessageOrder
    {
        NotConnected,
        Connected,
        SendFirstMessage,
        ReceiveFirstMessageReply,
        SendSecondMessage,
        ReceiveSecondMessageReply,
        SendThirdMessage,
        ReceiveThirdMessageReply,
        Error,
        Done
    }
    protected TcpListener m_tcpListener;
    protected Socket m_testClientSocket;
    protected byte[] m_readBuffer;

    [SerializeField]
    protected TestMessageOrder m_testClientState;
    public void StartServer()
    {
        IPHostEntry host;
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }

        m_tcpListener = new TcpListener(IPAddress.Parse(localIP), 2222);
        m_tcpListener.Start();
        StartListeningForConnections();
    }
    void StartListeningForConnections()
    {
        m_tcpListener.BeginAcceptSocket(AcceptNewSocket, m_tcpListener);
        Debug.Log("SERVER ACCEPTING NEW CLIENTS");
    }

    void AcceptNewSocket(System.IAsyncResult iar)
    {
        m_testClientSocket = null;
        m_testClientState = TestMessageOrder.NotConnected;
        m_readBuffer = new byte[32768];
        if(m_tcpListener != null) { 
            try
            {
                m_testClientSocket = m_tcpListener.EndAcceptSocket(iar);
            }
            catch (System.Exception ex)
            {
                Debug.Log(string.Format("Exception on new socket: {0}", ex.Message));
            }
            m_testClientSocket.NoDelay = true;
            m_testClientState = TestMessageOrder.Connected;
            BeginReceiveData();
            //SendTestData();
            StartListeningForConnections();
        }
           
    }

    void BeginReceiveData()
    {
        m_testClientSocket.BeginReceive(m_readBuffer, 0, m_readBuffer.Length, SocketFlags.None, EndReceiveData, null);
  
    }
    void EndReceiveData(System.IAsyncResult iar)
    {
        int numBytesReceived = m_testClientSocket.EndReceive(iar);
        Debug.Log(numBytesReceived);
        if (numBytesReceived > 0)
        {
            sb.Append(Encoding.ASCII.GetString(m_readBuffer, 0, numBytesReceived));
            Debug.Log(numBytesReceived);
        }
        else {
            if (sb.Length > 1) {
                string strContent = sb.ToString();
                ProcessData(strContent);
                sb.Length = 0;//Clean String Builder
                m_testClientSocket.Close();
            }
        }
       
        BeginReceiveData();
    }
    void ProcessData(string jsonString)
    {
        
        List<RunPacket> tmpRunBag = new List<RunPacket>();
        List<Vector2>[] tmpOffenderPath = new List<Vector2> [5];
        List<Vector2>[] tmpDefenderPath = new List<Vector2> [5];
        List<Vector2> tmpBallPath = new List<Vector2>();
        List<Vector2> tmpInitialPosition = new List<Vector2>();

        JSONNode jsonData = JSON.Parse(jsonString);
        current_receive_json = jsonString;

        // Read the RunBag
        for (int i = 0; i < jsonData["RunLine"].Count; i++) {
            RunPacket tmpPacket = new RunPacket();
            tmpPacket.startTime = jsonData["RunLine"][i]["start_time"];
            tmpPacket.duration = jsonData["RunLine"][i]["duration"];
            tmpPacket.handler = handleNameTransfer(jsonData["RunLine"][i]["handler"]);
            tmpPacket.roadStart = (jsonData["RunLine"][i]["road_start"] - 1)/2;
            tmpPacket.roadEnd = jsonData["RunLine"][i]["road_end"]/2;
            tmpPacket.rate = jsonData["RunLine"][i]["rate"];
            tmpPacket.ballHolder = jsonData["RunLine"][i]["ball_num"];
            
            tmpPacket.pathType = jsonData["RunLine"][i]["path_type"];
            tmpPacket.screenAngle = jsonData["RunLine"][i]["screen_angle"];
            tmpPacket.dribbleAngle = jsonData["RunLine"][i]["dribble_angle"];
            tmpPacket.dribbleLength = jsonData["RunLine"][i]["dribble_length"];

            tmpRunBag.Add(tmpPacket);
        }

        // 2018.09.10 Read the offensivePlayer path
        List<Vector2> t_tmpPlayer1Path = new List<Vector2>();
        List<Vector2> t_tmpPlayer2Path = new List<Vector2>();
        List<Vector2> t_tmpPlayer3Path = new List<Vector2>();
        List<Vector2> t_tmpPlayer4Path = new List<Vector2>();
        List<Vector2> t_tmpPlayer5Path = new List<Vector2>();


        // TODO:有可能沒有按照timeline順序塞進path中
        for (int i = 0; i < jsonData["RunLine"].Count; i++) {
            string handler = jsonData["RunLine"][i]["handler"];
            int t_roadStart = jsonData["RunLine"][i]["road_start"];
            int t_roadEnd = jsonData["RunLine"][i]["road_end"];

            if(handler == "P1_Handle"){
                if(t_roadStart == 0){
                    for (int j = 1; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer1Path.Add(new Vector2()
                        {
                            x = (jsonData["P1"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P1"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                }   
                else{
                    for (int j = t_roadStart; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer1Path.Add(new Vector2()
                        {
                            x = (jsonData["P1"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P1"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                } 
            }
            else if(handler == "P2_Handle"){
                if(t_roadStart == 0){
                    for (int j = 1; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer2Path.Add(new Vector2()
                        {
                            x = (jsonData["P2"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P2"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                }   
                else{
                    for (int j = t_roadStart; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer2Path.Add(new Vector2()
                        {
                            x = (jsonData["P2"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P2"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                } 
                
            }
            else if(handler == "P3_Handle"){
                if(t_roadStart == 0){
                    for (int j = 1; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer3Path.Add(new Vector2()
                        {
                            x = (jsonData["P3"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P3"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                }   
                else{
                    for (int j = t_roadStart; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer3Path.Add(new Vector2()
                        {
                            x = (jsonData["P3"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P3"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                } 
            }
            else if(handler == "P4_Handle"){
                if(t_roadStart == 0){
                    for (int j = 1; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer4Path.Add(new Vector2()
                        {
                            x = (jsonData["P4"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P4"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                }   
                else{
                    for (int j = t_roadStart; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer4Path.Add(new Vector2()
                        {
                            x = (jsonData["P4"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P4"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                } 
            }
            else if(handler == "P5_Handle"){
                if(t_roadStart == 0){
                    for (int j = 1; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer5Path.Add(new Vector2()
                        {
                            x = (jsonData["P5"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P5"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                }   
                else{
                    for (int j = t_roadStart; j < t_roadEnd; j=j+2) {
                        t_tmpPlayer5Path.Add(new Vector2()
                        {
                            x = (jsonData["P5"][j] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                            y = (jsonData["P5"][j + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                        });
                    }
                } 
            }
        }
        tmpOffenderPath[0] = t_tmpPlayer1Path;
        tmpOffenderPath[1] = t_tmpPlayer2Path;
        tmpOffenderPath[2] = t_tmpPlayer3Path;
        tmpOffenderPath[3] = t_tmpPlayer4Path;
        tmpOffenderPath[4] = t_tmpPlayer5Path;

        /*
        // Old version parse offensive way
        
        for(int player =0;player < 5; player++) { 
            List<Vector2> tmpPlayerPath = new List<Vector2>();
            string playerString = "P" + (player + 1);
            for (int i = 1; i < jsonData[playerString].Count; i=i+2) {

                tmpPlayerPath.Add(new Vector2()
                {
                    // 180331
                    // x = (jsonData[playerString][i] - 540) * 460 / 400,
                    // y = (jsonData[playerString][i + 1] - 760) * (-695) / 760
                    
                    x = (jsonData[playerString][i] - 540.0f) * ((7.04f+9.54f)/2.0f) / 400.0f - 1.25f,
                    y = (jsonData[playerString][i + 1] - 760.0f) * (0.0f-15.6f) / 760.0f + 0.0f
                });
            }
            tmpOffenderPath[player] = tmpPlayerPath;
        }
        */

        //Read the defensivePlayer path
        for (int player = 0; player < 5; player++)
        {
            List<Vector2> tmpPlayerPath = new List<Vector2>();
            string playerString = "D" + (player + 1);
            for (int i = 0; i < jsonData[playerString].Count; i = i + 2)
            {
                tmpPlayerPath.Add(new Vector2()
                {
					x = (jsonData[playerString][i] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                    y = (jsonData[playerString][i + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
                });
            }
            tmpDefenderPath[player] = tmpPlayerPath;
        }

        //Read the ball path
        for (int i = 0; i < jsonData["B"].Count; i++) {
            tmpBallPath.Add(new Vector2()
            {
                x = (jsonData["B"][i] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                y = (jsonData["B"][i + 1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
            });
        }

        //Read the initial position
        for (int i = 0; i < jsonData["Initial_Position"].Count; i=i+2)
        {
            tmpInitialPosition.Add(new Vector2()
            {
                x = (jsonData["Initial_Position"][i] - 540.0f) *(-1)* ((Constants.THREE_RIGHT_TOP_X - Constants.THREE_LEFT_TOP_X)/2.0f) / 400.0f + Constants.THREE_CENTER_X,
                y = (jsonData["Initial_Position"][i+1] - 760.0f) * (Constants.THREE_CENTER_Y - Constants.THREE_LEFT_TOP_Y) / 760.0f + Constants.THREE_CENTER_Y
            });            
        }

        BallPassPair[] tmpBallpasspairs = new BallPassPair[20];
        for (int i = 0; i < 20; i++)
        {
            tmpBallpasspairs[i] = new BallPassPair()
            {
                isValid = false,
                passerId = -1,
                catcherId = -1
            };
        }

        int prevHolderId = jsonData["Initial_ball_holder"];
        for (int i = 0; i < tmpRunBag.Count; i++)
        {
            // If current handler is ball
            if (tmpRunBag[i].handler == "10")
            {
                int timing = tmpRunBag[i].startTime;
                tmpBallpasspairs[timing] = new BallPassPair()
                {
                    isValid = true,
                    passerId = prevHolderId,
                    catcherId = tmpRunBag[i].ballHolder
                };
                prevHolderId = tmpRunBag[i].ballHolder;

            }
        }

        new_tactic = new Tactic()
        {
            runBag = tmpRunBag,
            offenderPath = tmpOffenderPath,
            defenderPath = tmpDefenderPath,
            ballPath = tmpBallPath,
            initialPosition = tmpInitialPosition,
            initialBallHolder = jsonData["Initial_ball_holder"],
            ballpasspairs = tmpBallpasspairs,
            tacticName = jsonData["Tactic_name"],
            categoryId = (Constants.TacticCategory)jsonData["Category_id"].AsInt,

        };

        Debug.Log("New Tactic : "+new_tactic.categoryId+", "+new_tactic.tacticName);

        has_new_tactic = true;
    }
    void OnDestroy()
    {
        if (m_testClientSocket != null)
        {
            m_testClientSocket.Close();
            m_testClientSocket = null;
        }
        if (m_tcpListener != null)
        {
            m_tcpListener.Stop();
            m_tcpListener = null;
        }
    }

    // Unused send methods
    void SendTestData()
    {
        Debug.Log(string.Format("Server: Client state: {0}", m_testClientState));
        switch (m_testClientState)
        {
            case TestMessageOrder.Connected:
                SendMessageOne();
                break;
            //case TestMessageOrder.SendFirstMessage:
            //break;
            case TestMessageOrder.ReceiveFirstMessageReply:
                SendMessageTwo();
                break;
            //case TestMessageOrder.SendSecondMessage:
            //break;
            case TestMessageOrder.ReceiveSecondMessageReply:
                SendMessageTwo();
                break;
            case TestMessageOrder.SendThirdMessage:
                break;
            case TestMessageOrder.ReceiveThirdMessageReply:
                m_testClientState = TestMessageOrder.Done;
                Debug.Log("ALL DONE");
                break;
            case TestMessageOrder.Done:
                break;
            default:
                Debug.LogError("Server shouldn't be here");
                break;
        }
    }
    void SendMessageOne()
    {
        m_testClientState = TestMessageOrder.SendFirstMessage;
        byte[] newMsg = new byte[] { 1, 100, 101, 102, 103, 104 };
        SendMessage(newMsg);
    }
    void SendMessageTwo()
    {
        m_testClientState = TestMessageOrder.SendSecondMessage;
        byte[] newMsg = new byte[] { 3, 100, 101, 102, 103, 104, 105, 106 };
        SendMessage(newMsg);
    }
    void SendMessageThree()
    {
        m_testClientState = TestMessageOrder.SendThirdMessage;
        byte[] newMsg = new byte[] { 5, 100, 101, 102, 103, 104, 105, 106, 107, 108 };
        SendMessage(newMsg);
    }
    void SendMessage(byte[] msg)
    {
        m_testClientSocket.BeginSend(msg, 0, msg.Length, SocketFlags.None, EndSend, msg);
    }
    void EndSend(System.IAsyncResult iar)
    {
        m_testClientSocket.EndSend(iar);
        byte[] msgSent = (iar.AsyncState as byte[]);
        string temp = CompileBytesIntoString(msgSent);
        Debug.Log(string.Format("Server sent: '{0}'", temp));
    }
    public static string CompileBytesIntoString(byte[] msg, int len = -1)
    {
        string temp = "";
        int count = len;
        if (count < 1)
        {
            count = msg.Length;
        }
        for (int i = 0; i < count; i++)
        {
            temp += string.Format("{0} ", msg[i]);
        }
        return temp;
    }

    // Garabage function
    private string handleNameTransfer(string oriName)
    {
        string inGameName = "";
        if (oriName.Equals("P1_Handle"))
        {
            inGameName = "0";
        }
        else if (oriName.Equals("P2_Handle"))
        {
            inGameName = "1";
        }
        else if (oriName.Equals("P3_Handle"))
        {
            inGameName = "2";
        }
        else if (oriName.Equals("P4_Handle"))
        {
            inGameName = "3";
        }
        else if (oriName.Equals("P5_Handle"))
        {
            inGameName = "4";
        }
        else if (oriName.Equals("B_Handle"))
        {
            inGameName = "10";
        }
        else if (oriName.Equals("D1_Handle"))
        {
            inGameName = "5";
        }
        else if (oriName.Equals("D2_Handle"))
        {
            inGameName = "6";
        }
        else if (oriName.Equals("D3_Handle"))
        {
            inGameName = "7";
        }
        else if (oriName.Equals("D4_Handle"))
        {
            inGameName = "8";
        }
        else if (oriName.Equals("D5_Handle"))
        {
            inGameName = "9";
        }

        return inGameName;
    }

}



