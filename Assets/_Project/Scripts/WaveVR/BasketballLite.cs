using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

//using wvr;

public class BasketballLite : MonoBehaviour {
	
	public int TimeCounter;
    public GameObject LeftHand;
    public XRBaseController LeftController, RightController;
	//public WVR_DeviceType device = WVR_DeviceType.WVR_DeviceType_Controller_Right;
	private int _ballCounter;
	private AudioSource _basketballAudioSource;
	private Transform _transform;
	private Rigidbody _rigidbody;

	#region Field : throw ball parameters
	
	private float V_THROW_STRENGTH = 10.0f;
	[Tooltip("視覺化投球軌跡")]
	public LineRenderer _sightLine;
	private const int _throwTrajectoryCount = 20;
	private int _modifiedThrowCount;
	private const float THROW_BALL_UPDATE_RATE = 0.075f;
	private Vector3[] _throwBallTrajectory;
	private Vector3 _lastVelocity;
	private Collider _hitObject;
	public Collider HitObject { get { return _hitObject; } }
	#endregion

	#region Field : pass ball parameters 
	private const int pass_trajectory_count = 69;
	private const float pass_ball_update_rate = 0.007f;
	private Vector3[] pass_ball_trajectory;
	private List<LineRenderer> _ballPathRenderers;
	private List<GameObject> ball_path_ghost;
	private GameObject baskeball_renderer;
	// 2020.05.26 The sequence order of the pass ball
	private int pass_ball_count = 1;

	#endregion
	
	private int _holdBallHash;
	private void Awake(){
		_basketballAudioSource = this.GetComponent<AudioSource>();
		_transform = this.GetComponent<Transform>();
		_rigidbody = this.GetComponent<Rigidbody>();
		Transform[] children = this.GetComponentsInChildren<Transform>(true);
        foreach(Transform child in children) {
			if(child.name == "Basketball_with_skel 1"){
				baskeball_renderer = child.gameObject;
			}
		}
	}

	private void Start () {
		_ballPathRenderers = new List<LineRenderer>();
		_holdBallHash = Animator.StringToHash("holdBall");
	}

	public void InvokePassBall(){
		_ballCounter = 0;
		#region Disable the dribbling
		Tactic current_tactic = TacticManager.Instance._currentChosenTactic;
		int passerId = current_tactic.ballpasspairs[TimeCounter].passerId;
		Animator playerAnim = TacticExecuterLite.Instance.Players[passerId-1]._animator;
		bool isDribblingLeft = TacticExecuterLite.Instance.Players[passerId-1]._player.transform.InverseTransformPoint(TacticExecuterLite.Instance.Defenders[passerId-1]._player.transform.position).z < 0;
		playerAnim.SetBool("leftDribble", false);
		playerAnim.SetBool("rightDribble", false);
		bool hasInvokeDribbling = playerAnim.GetBool(isDribblingLeft?"leftDribble":"rightDribble");
		this.GetComponent<Animator>().SetBool("leftDribble", false);
		this.GetComponent<Animator>().SetBool("rightDribble", false);
		this.GetComponent<Animator>().enabled = false;
		#endregion 
		InvokeRepeating("UpdatePassBallPosition", 0.0f, pass_ball_update_rate);
	}

	private void UpdatePassBallPosition(){
		if(TacticExecuterLite.Instance._invokedAgent[10]._isInvoked && _ballCounter < pass_trajectory_count){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;

			int catcher_id = current_tactic.ballpasspairs[TimeCounter].catcherId;
			GameObject catcher = GameObject.FindGameObjectWithTag("Player" + current_tactic.ballpasspairs[TimeCounter].catcherId);
			GameObject passer = GameObject.FindGameObjectWithTag("Player" + current_tactic.ballpasspairs[TimeCounter].passerId);

			// If current catcher is not the user's current perspective
			if (ModeManagerLite.Instance._perspective != (Constants.Perspective)catcher_id){
				//catcher.GetComponent<PlayerMovement>().SmoothLookAt(passer);
			}
			/*
			if (ballCounter % 10 == 0)
			{
				GameObject ballPassHint = (GameObject)Instantiate(Resources.Load("Prefabs/BallPassHint"));
				ModeManagerLite.Instance.ballpasshints.Add(ballPassHint);
				ballPassHint.transform.position = new Vector3(ballTrajectory[ballCounter].x, 0.1f, ballTrajectory[ballCounter].z);
				ballPassHint.transform.localRotation = Quaternion.LookRotation((catcher.transform.position - passer.transform.position) * (-1));
			}
			 */
			_transform.parent = null;
			_transform.position = pass_ball_trajectory[_ballCounter];
			_transform.Rotate(new Vector3(0.0f, 0.0f, -20.0f), Space.Self);

			_ballCounter++;
		}

		if(TacticExecuterLite.Instance._invokedAgent[10]._isInvoked && _ballCounter == pass_trajectory_count){
			Tactic current_tactic = TacticManager.Instance._currentChosenTactic;
			
			GameObject catcher = TacticExecuterLite.Instance.Players[current_tactic.ballpasspairs[TimeCounter].catcherId-1]._player;
			TacticExecuterLite.Instance.Players[current_tactic.ballpasspairs[TimeCounter].catcherId-1]._animator.SetBool(_holdBallHash, true);
			TacticExecuterLite.Instance.Players[current_tactic.ballpasspairs[TimeCounter].passerId-1]._animator.SetBool(_holdBallHash, false);
			GameObject defender = TacticExecuterLite.Instance.Defenders[current_tactic.ballpasspairs[TimeCounter].catcherId-1]._player;
			defender.GetComponent<DefenderMovementAI>().SmoothLookAt(catcher);

            if(current_tactic.ballpasspairs[TimeCounter].catcherId == (int)ModeManagerLite.Instance._perspective){
				if(LeftHand)
                    ChangeBallAttachment(LeftHand, 
                        new Vector3(-0.1163f, -0.1134f, 0.0348f));
                else{
                    Transform target_transform = TacticExecuterLite.Instance.Players[current_tactic.ballpasspairs[TimeCounter].catcherId-1]._animator.GetBoneTransform(HumanBodyBones.RightHand);
                    _transform.SetParent(target_transform);
                    _transform.localPosition = new Vector3(0.0093f, -0.1189f, -0.0701f);
                    _transform.localRotation = Quaternion.identity;
                    _transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                }
			}
			else{
				Transform target_transform = TacticExecuterLite.Instance.Players[current_tactic.ballpasspairs[TimeCounter].catcherId-1]._animator.GetBoneTransform(HumanBodyBones.RightHand);
                _transform.SetParent(target_transform);
                _transform.localPosition = new Vector3(0.0093f, -0.1189f, -0.0701f);
                _transform.localRotation = Quaternion.identity;
                _transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
			}

			TacticExecuterLite.Instance._invokedAgent[10]._isInvoked = false;
			CancelInvoke("UpdatePassBallPosition");
		}
	}

	public void GeneratePassBallTrajectory(Vector3 start_position, Vector3 target_position) {
        pass_ball_trajectory = new Vector3[pass_trajectory_count];
        pass_ball_trajectory[0] = start_position;
        Vector3 velocity = (target_position - start_position)/pass_trajectory_count;

        for (int i = 1; i < pass_trajectory_count; ++i)
        {
            pass_ball_trajectory[i] = pass_ball_trajectory[i - 1] + velocity;
        }

		#region Ball Pass Path Render
		// 2020.05.26 Change ball pass line to dotted line (related to real world basketball play diagram)
		LineRenderer line_renderer =  ((GameObject)Instantiate(Resources.Load("Prefabs/BallPassPathRenderer"))).GetComponent<LineRenderer>();

		//lineRenderer.transform.parent = runPathRendererParent.transform;
		Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(new Color32(255,67,0,255), 0.0f), new GradientColorKey(new Color32(255,67,0,255), 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0.2f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
		);
		// 2020.05.26 Downsample the points to make the dotted material works proper
		int ds_p = 16;

		line_renderer.colorGradient = gradient;
		line_renderer.positionCount = Mathf.RoundToInt(pass_trajectory_count/ds_p)+1;

		for(int i=0 ; i<pass_trajectory_count ; i=i+ds_p){
			if(i/ds_p < line_renderer.positionCount)
				line_renderer.SetPosition(i/ds_p, new Vector3(pass_ball_trajectory[i].x, 0.1f, pass_ball_trajectory[i].z));

			if(i%10 == 0){
				//GameObject ghost_ball = (GameObject)Instantiate(Resources.Load("Prefabs/GhostBasketball"));
				//ghost_ball.transform.position = pass_ball_trajectory[i];
				//ball_path_ghost.Add(ghost_ball);
			}
		}
		line_renderer.SetPosition(line_renderer.positionCount-1, new Vector3(pass_ball_trajectory[pass_trajectory_count-1].x, 0.1f, pass_ball_trajectory[pass_trajectory_count-1].z));
		
		// 2020.05.26 Get the pass ball bar Canvas and set the position, add corresponding pass ball bar in the canvas
		
		Vector3 middle_pos = new Vector3(pass_ball_trajectory[Mathf.RoundToInt(pass_trajectory_count/2)].x, 0.1f, pass_ball_trajectory[Mathf.RoundToInt(pass_trajectory_count/2)].z);
		GameObject pass_ball_bar_canvas = null;
		Transform[] children = line_renderer.GetComponentsInChildren<Transform>(true);
        foreach(Transform child in children) {
			if(child.name == "SequenceBar"){
				pass_ball_bar_canvas = child.gameObject;
			}
		}
		
		for(int i=0; i<pass_ball_count; i++){
			GameObject pass_ball_bar = ((GameObject)Instantiate(Resources.Load("Prefabs/PassBallBar")));
			pass_ball_bar_canvas.transform.localPosition = middle_pos;
			pass_ball_bar_canvas.transform.localRotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
			float pass_ball_angle = 0.0f;// rotate from (1, 0, 0) to the pass ball direction
			Vector3 pass_ball_dir = pass_ball_trajectory[pass_trajectory_count-1] - pass_ball_trajectory[0];
			pass_ball_dir.y = 0.0f;
			if(pass_ball_dir.z > 0) pass_ball_angle = Vector3.Angle(new Vector3(1.0f, 0.0f, 0.0f), pass_ball_dir);
			else pass_ball_angle = -Vector3.Angle(new Vector3(1.0f, 0.0f, 0.0f), pass_ball_dir);
			
			pass_ball_bar_canvas.transform.rotation = Quaternion.Euler(90.0f, 0.0f, pass_ball_angle);
			pass_ball_bar.transform.SetParent(pass_ball_bar_canvas.transform);
			pass_ball_bar.transform.localScale = Vector3.one;
			pass_ball_bar.transform.localRotation = Quaternion.Euler(Vector3.zero);
			pass_ball_bar.transform.localPosition = new Vector3(pass_ball_bar.transform.localPosition.x, pass_ball_bar.transform.localPosition.y, 0.01f);
		}
		pass_ball_count++;

		_ballPathRenderers.Add(line_renderer); 
		#endregion
    }

	public void InvokeThrowBall(){
		_ballCounter = 0;
		_transform.SetParent(null);
		InvokeRepeating("UpdateThrowBallPosition", 0.0f, THROW_BALL_UPDATE_RATE);
	}

	private void UpdateThrowBallPosition(){
		if(_ballCounter < _modifiedThrowCount){
			
			_transform.position = _throwBallTrajectory[_ballCounter];
			_transform.Rotate(new Vector3(0.0f, 0.0f, -20.0f), Space.Self);
			_ballCounter++;
		}
		//Debug.Log(modified_throw_count);
		if(_ballCounter == _modifiedThrowCount){
			this.GetComponent<Rigidbody>().velocity = _lastVelocity;
			this.GetComponent<Rigidbody>().useGravity = true;
			this.GetComponent<Rigidbody>().isKinematic = false;
			this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			CancelInvoke("UpdateThrowBallPosition");
		}
		
	}


	public void GenerateThrowBallTrajectory(Vector3 start_position, Vector3 target_position){
		_throwBallTrajectory = new Vector3[_throwTrajectoryCount];
		
		Vector3 distance = target_position - start_position;
		distance.y = 0.0f;

		_throwBallTrajectory[0] = start_position + distance.normalized * 0.4f;

		Vector3 v_segment_velocity = (TacticExecuterLite.Instance.Players[TacticExecuterLite.Instance._currentBallHolder-1]._player.transform.up * V_THROW_STRENGTH);
		float segment_time = 0.1f;//(segment_velocity.sqrMagnitude != 0) ? segment_scale / segment_velocity.magnitude : 0;
		
		Vector3 h_segment_velocity = (distance/segment_time).normalized;
		bool has_found_trajectory = false;
		int temp = 1;

		for(float s = 10.0f; s>=1.0f ; s=s-0.05f){
			Vector3  segment_velocity = v_segment_velocity + h_segment_velocity * s;
			
			for (int i = 1; i < _throwTrajectoryCount; i++){
				segment_velocity = segment_velocity + Physics.gravity * segment_time;

				float distance_to_basket = Vector3.Distance(_throwBallTrajectory[i - 1] + segment_velocity * segment_time, ModeManagerLite.Instance._hoopPosition);
				if(distance_to_basket < 0.2f){
					has_found_trajectory = true;
					break;
				}
				else{
					_throwBallTrajectory[i] = _throwBallTrajectory[i-1] + segment_velocity * segment_time;
					_lastVelocity = segment_velocity.normalized*2.0f;
				}
				temp = i;
			}

			Debug.Log(temp+"  "+s + "," + has_found_trajectory);
			if(has_found_trajectory)
				break;
		}
		
		/*
		Vector3 distance = target_position - start_position; distance.y = 0.0f;
		_throwBallTrajectory[0] = start_position + distance.normalized * 0.3f;
        Vector3 vSegmentVelocity = Vector3.up * V_THROW_STRENGTH * ( (0.1f * distance.magnitude + 4.5f)/5.7f);
        float segmentTime = 0.1f;

        Vector3 hSegmentVelocity = (distance/segmentTime).normalized;
        bool hasFoundTrajectory = false;
        int finalSize = _throwTrajectoryCount;

        float finalS = -1.0f;
        for(float s = 15.0f ; s >= 0.2f ; s = s-0.05f){
            Vector3 segmentVelocity = vSegmentVelocity + hSegmentVelocity * s;
            for(int i=1 ; i<_throwTrajectoryCount ; i++){
                segmentVelocity = segmentVelocity + Physics.gravity * segmentTime;
                Debug.Log("s: "+s+", sv: "+segmentVelocity.y);

                float distanceToBasket = Vector3.Distance(_throwBallTrajectory[i-1] + segmentVelocity * segmentTime, target_position);
                
                if( (distanceToBasket < 0.2f) && (segmentVelocity.y < 0.0f) ){
                        hasFoundTrajectory = true;
                        finalS = s;
                        break;
                }
                else{
                    _throwBallTrajectory[i] = _throwBallTrajectory[i-1] + segmentVelocity * segmentTime;
                    _lastVelocity = segmentVelocity.normalized * 2.0f;
                }
                finalSize = i;
            }

            if(hasFoundTrajectory){
                break;
            }
        }
		*/
		/*
		Vector3  segment_velocity = v_segment_velocity + h_segment_velocity * h_throw_strength;
			
		for (int i = 1; i < throw_trajectory_count; i++){
			segment_velocity = segment_velocity + Physics.gravity * segment_time;
			float segment_scale = segment_velocity.magnitude;
			//Debug.Log("Scale:"+segment_scale+",velocity:"+segment_velocity.normalized);
			RaycastHit hit;
			hit_object = null;

			//(segment_velocity * segment_time).magnitude)
			float distance_to_basket = Vector3.Distance(throw_ball_trajectory[i - 1] + segment_velocity * segment_time, ModeManagerLite.Instance.hoop_position);
			if(distance_to_basket < 0.2f){
				has_found_trajectory = true;
				break;
			}
			else{
				throw_ball_trajectory[i] = throw_ball_trajectory[i-1] + segment_velocity * segment_time;
				last_velocity = segment_velocity.normalized*2.0f;
			}
			
			temp = i;
		}
		 */

		_modifiedThrowCount = temp;

		
		#region Visualization of throw ball trajectory
		/*
		Color start_color = Color.red;
		Color end_color = start_color;
		start_color.a = 0;
		end_color.a = 1;
		_sightLine.startColor = start_color;
		_sightLine.endColor = end_color;

        int modified_count = temp;//throw_trajectory_count;
        for(int i=0 ; i < _throwTrajectoryCount ; i++){
            if(_throwBallTrajectory[i].magnitude < 0.1f){
                modified_count = i;
                break;
            }
        }
		_modifiedThrowCount = temp;
		_sightLine.positionCount = temp;
		for (int i = 0; i < temp; i++){
			_sightLine.SetPosition(i, _throwBallTrajectory[i]);
        }
	
		*/
		#endregion
		
	}

	public void ClearBallPath(){
		for(int i=0;i<_ballPathRenderers.Count;i++){
			Destroy(_ballPathRenderers[i].gameObject);
		}
		_ballPathRenderers.Clear();
		pass_ball_count = 1;
	}

	/** 
	// 2018.01.04
	// InputManager偵測到使用者做出傳球動作  --> 傳球動作無效(3PP/1PP但現在不用傳球)
	//                                    |  
	//                                     --> 傳球動作有效 ---> 使用者看對方向 ---> 讓戰術可以繼續執行
	//                                                     |
	//													   ---> 使用者看錯方向
	*/

	public void ChangeBallAttachment(GameObject attach_place, Vector3 offset){
		this.transform.parent = null;
		this.transform.parent = attach_place.transform;
		this.transform.localPosition = offset;

        // Haptic feedback on both hand
        LeftController.SendHapticImpulse(0.3f, 0.5f);
        RightController.SendHapticImpulse(0.3f, 0.5f);
	}

	public void AttachBallToHolder(){
		if(TacticExecuterLite.Instance._currentBallHolder != 0){
			Animator ball_holder_animator = GameObject.FindGameObjectWithTag("Player" + (TacticExecuterLite.Instance._currentBallHolder)).GetComponentInChildren<Animator>();
			Transform ball_transform = this.transform;
			ball_holder_animator.SetTrigger(_holdBallHash);

			this.transform.SetParent(ball_holder_animator.GetBoneTransform(HumanBodyBones.RightHand));
			// The paremeter is properly setting in the scene.
			this.transform.localPosition = new Vector3(0.0093f, -0.1189f, -0.0701f);
			this.transform.localRotation = Quaternion.identity;
			this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
		}
	}

	public void OnCollisionEnter(Collision collision){
		if(collision.collider.name == "Floor"){
			_basketballAudioSource.Play();
		}
	}

	public void OnBounceFloor(){
		_basketballAudioSource.Play();
		//WaveVR_Controller.Input(device).TriggerHapticPulse(1000, WVR_InputId.WVR_InputId_Alias1_Touchpad);
	}
}
