﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Profiling;
using SimpleJSON;
using UnityEngine.UI;
//using wvr;

public class TacticExecuterLite : Singleton<TacticExecuterLite> {

	/********************************/
    //      3D tactic execution     //
    /********************************/

	public GameObject _canvasControlState;


	public int _currentBallHolder; 
	public GameObject _canvasSetting;
	public GameObject _runpathRendererParent;
	public GameObject _learningPassHint;
	public GameObject _learningTurnLeft, _learningTurnRight;
	public InvokedAgent[] _invokedAgent;
    public class InvokedAgent
    {
        public bool _isInvoked;
        public bool _isDribbling;
        public bool _isScreen;
        public float _screenAngle;
        public int _startIndex;
        public int _endIndex;
		public int _pathType;
    };
	[Serializable]
	public class Player
	{
		public GameObject _player;
		public Animator _animator;
		public NavMeshAgent _navmeshAgent;
		public NavMeshWayPoint _navmeshWaypoint;
		public PlayerMovement _playerMovement;
	};

	public class CacheBasketball{
		public GameObject _gameObject;
		public Rigidbody _rigidbody;
		public Animator _animator;
		public Transform _transform;
	};
	public CacheBasketball _ball;

	public Player[] Players = new Player[5];
	public Player[] Defenders = new Player[5];

	private IEnumerator _coroutine;
	private Constants.ExecutorState _currentState;
	private int _maxTime;
	private int _timeCounter;
	private bool _isPassBall = false;
	private const float UPDATE_RATE = 1F;
	private bool _playerDirectionIsRight = false;
	private bool _userWantToContinue = false;
	private bool _activatePassBall = false;
	private bool _needToPassBall = false;
	
	private List<LineRenderer> _runpathRenderers;
	private List<GameObject> _screenBarPrefabs;
	private Color[] _runpathColorStart;
	private Color[] _runpathColorEnd;

	private int _isDribblingHash;
	private int _resetDribblingHash;
	private int _tacticStartHash;
	private int _isScreenHash;

	private void Awake(){
		_ball = new CacheBasketball();
		_ball._gameObject = GameObject.FindGameObjectWithTag("Basketball");
		_ball._rigidbody = _ball._gameObject.GetComponent<Rigidbody>();
		_ball._animator = _ball._gameObject.GetComponent<Animator>();
		_ball._transform = _ball._gameObject.transform;
	}
	// Use this for initialization
  	private void Start () {
		_coroutine = ExecuteTactic();
		_currentBallHolder = -1;
		_runpathRenderers = new List<LineRenderer>();
		_screenBarPrefabs = new List<GameObject>();

		_isDribblingHash = Animator.StringToHash("isDribbling");
		_resetDribblingHash = Animator.StringToHash("ResetDribbling");
		_tacticStartHash = Animator.StringToHash("tacticStart");
		_isScreenHash = Animator.StringToHash("isScreen");

		#region Initialize run path color
		_runpathColorEnd = new Color[5];
		
		_runpathColorEnd[0] = new Color32(17, 75, 95, 255);
		_runpathColorEnd[1] = new Color32(21, 127, 31, 255);
		_runpathColorEnd[2] = new Color32(228, 253, 225, 255);
		_runpathColorEnd[3] = new Color32(69, 105, 144, 255);
		_runpathColorEnd[4] = new Color32(244, 91, 105, 255);

		_runpathColorStart = new Color[5];
		_runpathColorStart[0] = new Color32(255, 255, 255, 255);
		_runpathColorStart[1] = new Color32(255, 255, 255, 255);
		_runpathColorStart[2] = new Color32(255, 255, 255, 255);
		_runpathColorStart[3] = new Color32(255, 255, 255, 255);
		_runpathColorStart[4] = new Color32(255, 255, 255, 255);
		#endregion
		#region Initialize players
		for(int i=0 ; i<5 ; i++){
			Players[i] = new Player();
			Players[i]._player = GameObject.FindGameObjectWithTag("Player" + (i+1));
			Players[i]._animator = Players[i]._player.GetComponent<Animator>();
			Players[i]._navmeshAgent = Players[i]._player.GetComponent<NavMeshAgent>();
			Players[i]._navmeshWaypoint = Players[i]._player.GetComponent<NavMeshWayPoint>();
			Players[i]._playerMovement = Players[i]._player.GetComponent<PlayerMovement>();
		}
		SetDefenderRef();
		#endregion
	}

	public void SetDefenderRef(){
		for(int i=0;i<5;i++){
			Defenders[i] = new Player();
			Defenders[i]._player = GameObject.FindGameObjectWithTag("Defender" + (i+1));
			Defenders[i]._animator = Defenders[i]._player.GetComponent<Animator>();
			Defenders[i]._navmeshAgent = Defenders[i]._player.GetComponent<NavMeshAgent>();
			Defenders[i]._navmeshWaypoint = Defenders[i]._player.GetComponent<NavMeshWayPoint>();
		}
	}
	
	public void OnSelectedTactic(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			Tactic currentTactic = TacticManager.Instance._currentChosenTactic;

			//Set the ball holder to hold ball humaniod rig position
			_ball._rigidbody.useGravity = false;
			_ball._rigidbody.isKinematic = true;
			_ball._rigidbody.constraints = RigidbodyConstraints.FreezePositionY;

			//Reset the animation state of all the players and defenders
			for(int i=0;i<5;i++)
				Players[i]._animator.SetTrigger(_tacticStartHash);
			
			_currentBallHolder = currentTactic.initialBallHolder;
			int ballHolderId = currentTactic.initialBallHolder;
			GameObject ballHolder;
			_ball._gameObject.GetComponent<BasketballLite>().AttachBallToHolder();

			//Make the ballHolder look at the hoop direction
			ballHolder = Players[ballHolderId-1]._player;

			Vector3 hoopPoint = ModeManagerLite.Instance._hoopPosition;
			hoopPoint.y = ballHolder.transform.position.y;
			ballHolder.transform.LookAt(hoopPoint);
			
			//Set all the player in target postion
			for (int i = 0; i < 5; i++)
			{
				GameObject player = Players[i]._player;
				Players[i]._navmeshAgent.enabled = false;
				Players[i]._player.transform.position = new Vector3(currentTactic.initialPosition[i].x, 0, currentTactic.initialPosition[i].y);
				Players[i]._navmeshAgent.enabled = true;
				
				
				float _distanceToHoop = (player.transform.position - ModeManagerLite.Instance._hoopPosition).magnitude;
				
				if (_distanceToHoop > 7.0f){
					player.transform.LookAt(hoopPoint);
				}
				else
					player.transform.LookAt(ballHolder.transform.position);
				
				// Pre relocate the Camera's position
				if((i+1) == (int)ModeManagerLite.Instance._perspective){
					GameObject _inputManager = GameObject.FindGameObjectWithTag("InputManager");
					_inputManager.GetComponent<CameraController>().target = Players[i]._player;
					_inputManager.transform.position = Players[i]._player.transform.position;
					_inputManager.transform.rotation = Players[i]._player.transform.rotation;
				}

			}
			ModeManagerLite.Instance.ChangeDefenderMode(ModeManagerLite.Instance._defender);
			ClearRunPathList();
			_ball._gameObject.GetComponent<BasketballLite>().ClearBallPath();
		}
	}

	public void InvokeExecution(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			Tactic currentTactic = TacticManager.Instance._currentChosenTactic;
			
			//Initialize the maxTime of current tactic
			_maxTime = 0;
			_timeCounter = 0;
			for (int i = 0; i < currentTactic.runBag.Count; i++) {
				if (currentTactic.runBag[i].startTime > _maxTime) {
					_maxTime = currentTactic.runBag[i].startTime;
				}
			}
			_currentState = Constants.ExecutorState.START;
			_currentBallHolder = currentTactic.initialBallHolder;
			_canvasSetting.GetComponent<Canvas>().enabled = false;
			_canvasControlState.SetActive(true);
			//_canvasSetting.GetComponent<CanvasDelayMove>().distance = 1000.0f;
			_canvasSetting.transform.localScale = new Vector3(0.001f, 0.001f, 1f);

			OnSelectedTactic();
			Debug.Log(TacticManager.Instance._currentSelectIndex+":"+TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex].tacticName, this);
			_invokedAgent = new InvokedAgent[11];
			_userWantToContinue = false;
			
			_coroutine = ExecuteTactic();
			this.StartCoroutine(_coroutine);
		}
	}

	private IEnumerator ExecuteTactic(){
		while(true){
			Tactic currentTactic = TacticManager.Instance._currentChosenTactic;
			List<RunPacket> currentRunbag = currentTactic.runBag;
			bool currentModeIsLearning = (ModeManagerLite.Instance._mode == Constants.Mode.LEARNING) ? true : false;
			bool currentPerspectiveIs3PP = (ModeManagerLite.Instance._perspective == Constants.Perspective.THIRD_PP) ? true : false;
			
			Debug.Log(_currentState);
			switch(_currentState){
				case Constants.ExecutorState.START:
					Debug.LogWarning("Tactic Start to play.");
					ModeManagerLite.Instance._tacticIsExecuting = true;
					for (int i = 0; i < 11; i++){
						_invokedAgent[i] = new InvokedAgent();
						_invokedAgent[i]._isInvoked = false;
					}

					if(ModeManagerLite.Instance._mode == Constants.Mode.LEARNING){
						_currentState = Constants.ExecutorState.LEARNING_START;
						ModeManagerLite.Instance._canvasContinue.SetActive(true);
					}
					else{
						_currentState = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
					}
					// 2019.02.23 Record the first person camera rotation
					CameraManager.Instance.IvokeCameraRotationRecorder();

					//TODO:Remember to re-rotate the VR camera
					break;
				
				case Constants.ExecutorState.LEARNING_START:
					
					if(_userWantToContinue){
						ModeManagerLite.Instance._canvasContinue.SetActive(false);
						_currentState = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
					}
					break;
				case Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG:
					//Debug.Log("State ==> CheckCurrTimeRunBag");
					_activatePassBall = false;
					_playerDirectionIsRight = false;
					_userWantToContinue = false;
					_needToPassBall = false;

					for(int i=0 ; i<currentRunbag.Count ; i++){
						if(currentRunbag[i].startTime == _timeCounter){

							#region Parse the handler to handleName
							string handle = currentRunbag[i].handler;
							string handleName;
							if(handle == "10"){
								handleName = "Basketball";
							}
							else if(handle == "0" || handle == "1" || handle == "2" || handle == "3" || handle == "4"){
								handleName = "Player" + (System.Convert.ToInt32(handle) + 1);
							}
							else{
								handleName = "Defender" + (System.Convert.ToInt32(handle) - 4);
							}
							#endregion

							for(int j=0;j<5;j++){
								Players[j]._animator.SetBool(_isScreenHash, false);
							}

							#region According to the handle, Setting the invoke agent and target animations
							if(handle == "10"){
								// Ball passing is being invoked.
								// Setting the animation parameter of "passBall" 

                                int passerId = currentTactic.ballpasspairs[_timeCounter].passerId;
								int catcherId = currentTactic.ballpasspairs[_timeCounter].catcherId;
								GameObject passer = Players[passerId-1]._player;
								//Check whether the dribbling layer is invoked.
								/*
								if(players[passer_id-1].animator.GetLayerWeight(1) > 0.9f){
									players[passer_id-1].animator.SetTrigger("holdBall");
									players[passer_id-1].animator.SetLayerWeight(1, 0.0f);
									ball.GetComponent<BasketballLite>().is_dribbling_end = true;
								}

								//players[passer_id-1].animator.ResetTrigger("holdBall");
								players[passer_id-1].animator.SetTrigger("passBall");
								*/

								// Setting the attachment of the ball and the trajectory 
								GameObject catcher = GameObject.FindGameObjectWithTag("Player" + currentTactic.ballpasspairs[_timeCounter].catcherId);
								Vector3 passerHandPosition = passer.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform.position;
								Vector3 catcherHandPosition = catcher.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).transform.position;
								_ball._gameObject.GetComponent<BasketballLite>().GeneratePassBallTrajectory(passerHandPosition, catcherHandPosition);
								
								//if( ModeManagerLite.Instance.perspective == (Constants.Perspective)passer_id)
								//	need_to_pass_ball = true;

								if (ModeManagerLite.Instance._perspective != (Constants.Perspective)passerId)
                                {
                                    // If current passer is not the user's current perspective
                                    //passer.transform.LookAt(catcher.transform.position);		
									
									// 2018.12.20 Smooth look at the catcher						
									//passer.transform.rotation = Quaternion.Slerp(passer.transform.rotation, Quaternion.LookRotation(catcher.transform.position - passer.transform.position, passer.transform.up), Time.deltaTime * rotateRate);

									passer.GetComponent<PlayerMovement>().SmoothLookAt(catcher);
								}

								if(ModeManagerLite.Instance._perspective != (Constants.Perspective)catcherId)
									catcher.GetComponent<PlayerMovement>().SmoothLookAt(passer);

								_invokedAgent[10]._isInvoked = true;
								_currentBallHolder = currentTactic.ballpasspairs[_timeCounter].catcherId;
								_ball._gameObject.GetComponent<BasketballLite>().TimeCounter = _timeCounter;
							}
							else{
								GameObject player = GameObject.FindGameObjectWithTag(handleName);
								NavMeshAgent playerAgent = player.GetComponentInChildren<NavMeshAgent>();
								if (player != null)
                                {
									int startIndex = currentRunbag[i].roadStart;
                                    int endIndex = currentRunbag[i].roadEnd - 1;
									int invokeId = System.Convert.ToInt32(handle);

									_invokedAgent[invokeId]._isInvoked = true;
                                    _invokedAgent[invokeId]._startIndex = currentRunbag[i].roadStart;
                                    _invokedAgent[invokeId]._endIndex = currentRunbag[i].roadEnd - 1;
                                    _invokedAgent[invokeId]._isDribbling = (currentRunbag[i].pathType == 2) ? true:false;
									_invokedAgent[invokeId]._isScreen = (currentRunbag[i].pathType == 1)? true:false;
									_invokedAgent[invokeId]._pathType = currentRunbag[i].pathType;
									
									if(invokeId < 5){
										List<Vector2> path = new List<Vector2>();
										for(int t = startIndex ; t<endIndex ; t++){
                                            bool null_checker = currentTactic.offenderPath[invokeId][t] == null ? true:false;
                                            if(!null_checker){
                                                path.Add(new Vector2(currentTactic.offenderPath[invokeId][t].x, currentTactic.offenderPath[invokeId][t].y));
                                            }
                                        }
										playerAgent.GetComponent<NavMeshWayPoint>().SetWayPoints(path);
                                        
										// 2018.12.30 Render run paths
										GenerateRunPathGuidance(path, _invokedAgent[invokeId]._isDribbling, invokeId);
										
										if(_invokedAgent[invokeId]._isScreen){
											GameObject screenBar =  ((GameObject)Instantiate(Resources.Load("Prefabs/ScreenBar")));
											screenBar.GetComponentInChildren<Image>().color = _runpathColorEnd[invokeId];
											Vector3 screenBarPosition = new Vector3(path[path.Count-1].x, 0.1f, path[path.Count-1].y);
											screenBar.transform.position = screenBarPosition;
											Vector3 targetDirection = new Vector3(path[path.Count-1].x, 0.1f, path[path.Count-1].y) - new Vector3(path[path.Count-2].x, 0.1f, path[path.Count-2].y);
											//screen_bar.transform.rotation = Quaternion.Euler(Vector3.RotateTowards(Vector3.zero, target_direction, 360.0f, 5.0f));
											screenBar.transform.rotation = Quaternion.LookRotation(targetDirection, new Vector3(0.0f, 1.0f, 0.0f));
											_screenBarPrefabs.Add(screenBar);
										}
									}else{

									}
								}
							}
							#endregion
						}
					}

					_currentState = Constants.ExecutorState.INVOKE_CURRENT_TIME_RUN_BAG;
					
					if(currentModeIsLearning)	
						ModeManagerLite.Instance._canvasContinue.SetActive(true);
					
					break;
				
				case Constants.ExecutorState.INVOKE_CURRENT_TIME_RUN_BAG:
					if(_invokedAgent[10]._isInvoked){
						int passerId = currentTactic.ballpasspairs[_timeCounter].passerId;
						int catcherId = currentTactic.ballpasspairs[_timeCounter].catcherId;
						GameObject passer = Players[passerId-1]._player;
						//Check whether the dribbling layer is invoked.
						/*
						if(Players[passerId-1]._animator.GetLayerWeight(1) > 0.9f){
							Players[passerId-1]._animator.SetBool(_isDribblingHash, false);
							Players[passerId-1]._animator.SetLayerWeight(1, 0.0f);
							Players[passerId-1]._animator.SetTrigger(_resetDribblingHash);
							_ball._animator.SetTrigger(_resetDribblingHash);
							_ball._animator.SetBool(_isDribblingHash, false);
							_ball._animator.enabled = false;
						}
						*/
						//players[passer_id-1].animator.ResetTrigger("holdBall");
						_ball._transform.localPosition = new Vector3(0.014f, 1.099f, 0.271f);
						Players[passerId-1]._animator.SetTrigger("passBall");
					}

					for(int i=0;i<5;i++){
						if(_invokedAgent[i]._isInvoked){
							Players[i]._navmeshWaypoint.StartToMove();
						
							if(_invokedAgent[i]._pathType == 2){
								/*
								Animator player_anim = players[i].animator;
								player_anim.SetLayerWeight(1, 1.0f);
								player_anim.ResetTrigger("holdBall");
								player_anim.SetTrigger("tacticStart");
								ball.GetComponent<BasketballLite>().InvokeDribbling();
								ball.GetComponent<BasketballLite>().is_dribbling_end = false;
								*/
								// 2020.05.29 Dribbling	
								/*
								Animator player_anim = _players[i]._animator;
								player_anim.SetLayerWeight(1, 1.0f);
								player_anim.SetBool(_isDribblingHash, true);

								_ball._transform.SetParent(_players[i]._player.transform);
								_ball._transform.localPosition = new Vector3(0.256f, 1.253f, 0.417f);
								_ball._transform.localRotation = Quaternion.Euler(Vector3.zero);
								_ball._transform.localScale = Vector3.one;
								_ball._animator.enabled = true; 
								_ball._animator.SetBool(_isDribblingHash, true);
								
								player_anim.Play("Dribble", 0, 0.0f);
								_ball._animator.Play("ballDribblingStandby", 0, 0.0f);
								*/
								Animator playerAnim2 = Players[i]._animator;
								bool isDribblingLeft2 = Players[i]._player.transform.InverseTransformPoint(Defenders[i]._player.transform.position).z < 0;
								playerAnim2.SetBool(isDribblingLeft2?"leftDribble":"rightDribble", true);
								bool hasInvokeDribbling2 = playerAnim2.GetBool(isDribblingLeft2?"leftDribble":"rightDribble");
								_ball._animator.enabled = true;
								// 2020.08.31 Rebind the animator to synchronize animators between the player and the basketball
								_ball._animator.Rebind();
								_ball._animator.SetBool(isDribblingLeft2?"leftDribble":"rightDribble", hasInvokeDribbling2);
								_ball._transform.SetParent(Players[i]._animator.GetBoneTransform(isDribblingLeft2?HumanBodyBones.LeftHand:HumanBodyBones.RightHand).transform);
							}
						}	
					}
					
					if(ModeManagerLite.Instance._defender == Constants.Defender.NO_DEFENDER)
						_currentState = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;
					else
						_currentState = Constants.ExecutorState.HMM_DEFENDER_UPDATE;				
					
					break;
				case Constants.ExecutorState.HMM_DEFENDER_UPDATE:
					yield return new WaitForSeconds(0.03f);
					#region If the offender is moving, trigger the correspond defender to follow
					for(int i=0 ; i<5 ; i++){
						float offenderTargetX, offenderTargetY;
						_invokedAgent[i+5]._isInvoked = true;
						
						GameObject offender = Players[i]._player;
						GameObject defender = Defenders[i]._player;
						
						if (_invokedAgent[i]._isInvoked)
						{
							int offEndIndex = _invokedAgent[i]._endIndex-1;
							offenderTargetX = currentTactic.offenderPath[i][offEndIndex].x;
							offenderTargetY = currentTactic.offenderPath[i][offEndIndex].y;
							Vector3 transformOffender = new Vector3(offenderTargetX, 0.0f, offenderTargetY) - ModeManagerLite.Instance._hoopPosition;
							Vector3 transformBall = _ball._transform.position - ModeManagerLite.Instance._hoopPosition;
							float targetX = (0.62f * transformOffender.x + 0.11f * transformBall.x + 0.0f * ModeManagerLite.Instance._hoopPosition.x + ModeManagerLite.Instance._hoopPosition.x);
							float targetY = (0.62f * transformOffender.z + 0.11f * transformBall.z + 0.0f * ModeManagerLite.Instance._hoopPosition.z + ModeManagerLite.Instance._hoopPosition.z);

							List<Vector2> offenderPath = offender.GetComponent<NavMeshWayPoint>().GetWayPoints();
							offenderPath.RemoveAt(offenderPath.Count-1);
							offenderPath.RemoveAt(offenderPath.Count-1);
							List<Vector2> defenderPath = new List<Vector2>();
							for(int j=0;j<offenderPath.Count;j++){
								Vector2 tempPos = offenderPath[j] - new Vector2(ModeManagerLite.Instance._hoopPosition.x, ModeManagerLite.Instance._hoopPosition.z);
								float newX = (0.62f * tempPos.x + 0.11f * transformBall.x + 0.0f * ModeManagerLite.Instance._hoopPosition.x + ModeManagerLite.Instance._hoopPosition.x);
								float newY = (0.62f * tempPos.y + 0.11f * transformBall.z + 0.0f * ModeManagerLite.Instance._hoopPosition.z + ModeManagerLite.Instance._hoopPosition.z);
								defenderPath.Add(new Vector2(newX, newY));
							}

							defenderPath.Add(new Vector2(targetX, targetY));

							defender.GetComponent<NavMeshWayPoint>().SetWayPoints(defenderPath);
							defender.GetComponent<NavMeshWayPoint>().StartToMove();
							//defender.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("Basic_Run_03", 0.2f);
						}
						else{
							
							offenderTargetX = (int)offender.transform.position.x;
							offenderTargetY = (int)offender.transform.position.z;

							Vector3 transformOffender = new Vector3(offenderTargetX, 0.0f, offenderTargetY) - ModeManagerLite.Instance._hoopPosition;
							Vector3 transformBall = _ball._transform.position - ModeManagerLite.Instance._hoopPosition;
							float target_x = (0.62f * transformOffender.x + 0.11f * transformBall.x + 0.0f * ModeManagerLite.Instance._hoopPosition.x + ModeManagerLite.Instance._hoopPosition.x);
							float target_y = (0.62f * transformOffender.z + 0.11f * transformBall.z + 0.0f * ModeManagerLite.Instance._hoopPosition.z + ModeManagerLite.Instance._hoopPosition.z);
							
							//defender.GetComponentInChildren<NavMeshAgent>().SetDestination(new Vector3(target_x, 0, target_y));
							//defender.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("Basic_Walk_01", 0.2f);
							
						}
					}
					#endregion
					_currentState = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;


					break;
				case Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG:
					// Rotate the defender to target rotation
					#region Check whether each agent reach their destination
					for(int i=0 ; i<5 ; i++){
						if(_invokedAgent[i]._isInvoked){
							if(!Players[i]._navmeshAgent.pathPending){
								if (Players[i]._navmeshAgent.GetComponent<NavMeshWayPoint>()._reachEndPoint){
									_invokedAgent[i]._isInvoked = false;
									if(_invokedAgent[i]._isScreen){
										Players[i]._animator.SetBool(_isScreenHash, true);
									}
								}
							}
						}
					}
					for(int i=0 ; i<5 ; i++){
						if(_invokedAgent[i+5]._isInvoked){
							
							if(!Defenders[i]._navmeshAgent.pathPending){
								
								if ((Defenders[i]._navmeshAgent.remainingDistance <= Defenders[i]._navmeshAgent.stoppingDistance) || Defenders[i]._navmeshAgent.GetComponent<NavMeshWayPoint>()._reachEndPoint){
									_invokedAgent[i+5]._isInvoked = false;
									//Defenders[i]._player.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("HumanoidIdle", 0.5f);
								}
							}
						}
					}
					#endregion

					bool isAgentNotFinish = false;
					// Check whether every invoked agent finished their task in this time step
                    if(currentModeIsLearning){
						for (int i = 0; i < 10; i++)
							isAgentNotFinish = isAgentNotFinish || _invokedAgent[i]._isInvoked;
					}
					else{
						for (int i = 0; i < 10; i++)
							isAgentNotFinish = isAgentNotFinish || _invokedAgent[i]._isInvoked;
					}
						
					if(isAgentNotFinish)
						_currentState = Constants.ExecutorState.EXECUTE_CURRENT_TIME_RUN_BAG;
					else{
						Defenders[_currentBallHolder-1]._player.GetComponent<DefenderMovementAI>().SmoothLookAt(Players[_currentBallHolder-1]._player);
						//Defenders[_currentBallHolder-1]._player.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("20181017-2_Char00", 0.5f);
						_currentState = Constants.ExecutorState.CHECK_FINISH;
					}
					break;
				case Constants.ExecutorState.CHECK_FINISH:

					//                           |---> Third Person Perspective 
					//       |--> Normal mode ---|                                    
					//       |                   |                                 |---> Donot need to pass ball
                    // When -|                   |---> First Person Perspective ---|                          
                    //       |                                                     |---> 1.Need to pass ball  2.Pass ball action active   
					//       |                                                       
					//       |                     |---> Third Person Perspective                                     
					//       |--> Learning mode ---|                                                          
					//                             |                                 |---> 1. Donot need to pass ball 2.Player face to correct direction 3.User wants to continue
                    //       					   |---> First Person Perspective ---|              						  
					//                                                               |---> 1. Need to pass ball 2.Player face to correct direction 3. Pass ball action active 4. User wants to continue
					//
                    // If all the conditions commit, the next step can execute.
					

					#region Check the head pose of user

					//Debug.Log(current_mode_is_learning + ", " + current_perspective_is_3PP + ", " + need_to_pass_ball + ", " + activate_pass_ball + ", " + player_direction_is_right);
					if(!currentPerspectiveIs3PP && currentModeIsLearning){
						// Check for whether the user look at the right direction
                        // IF current perspective is the ball holder: 
                        //    |---> If next step the player is going to pass ball, look at the catcher
                        //   -|
                        //    |---> If not, look at the hoop position.
                        //
                        // ELSE : 
                        //    |---> Look at the ball holder.

						Vector3 cameraForward = CameraManager.Instance.wave_camera.transform.forward; //InputManager.Instance.inputdevice[(int)InputManager.Instance.controller].camera.transform.forward;
						Vector3 correctDirection = Vector3.zero;
						Vector3 targetPosition = Vector3.zero;
						Vector3 currentPosition = Players[(int)ModeManagerLite.Instance._perspective-1]._player.transform.position;
						currentPosition.y = 4.5f;

						if(ModeManagerLite.Instance._perspective == (Constants.Perspective)_currentBallHolder){
							if(ModeManagerLite.Instance._perspective == (Constants.Perspective)currentTactic.ballpasspairs[_timeCounter + 1].passerId){
								int catcherId = currentTactic.ballpasspairs[_timeCounter + 1].catcherId;
								
								targetPosition = Players[catcherId-1]._player.transform.position;
								targetPosition.y = 4.5f;
								correctDirection = targetPosition - currentPosition;
							}
							else{
								targetPosition = ModeManagerLite.Instance._hoopPosition;
								targetPosition.z -= 1.0f;
								targetPosition.y = 5.0f;
								correctDirection = targetPosition - currentPosition;
							}
						}
						else{
							targetPosition = Players[_currentBallHolder-1]._player.transform.position;
							targetPosition.y = 4.5f;
							correctDirection = targetPosition - currentPosition;
						}

						//Debug.Log("====\nCorrect: " + correct_direction);
						//Debug.Log("====\nCamera forward: " + camera_forward);
						//Debug.Log("Angle: " + Vector3.Angle(correct_direction, camera_forward));
						//Debug.Log("Angle: " + Vector3.SignedAngle(correct_direction, camera_forward, Vector3.up));

						Constants.Direction targetDirection = (Vector3.SignedAngle(correctDirection, cameraForward, Vector3.up)>0)?Constants.Direction.LEFT:Constants.Direction.RIGHT;
						_playerDirectionIsRight = (Vector3.Angle(correctDirection, cameraForward) < 20.0f) ? true:false;
						
						if(_playerDirectionIsRight){
							ModeManagerLite.Instance._canvasContinue.SetActive(true);
							_learningTurnLeft.SetActive(false);
							_learningTurnRight.SetActive(false);
						}
						else{
							ModeManagerLite.Instance._canvasContinue.SetActive(false);
							if(!_userWantToContinue){
								if(targetDirection == Constants.Direction.LEFT){
									_learningTurnLeft.SetActive(true);
									_learningTurnRight.SetActive(false);
								}
								else{
									_learningTurnLeft.SetActive(false);
									_learningTurnRight.SetActive(true);
								}
							}
						}

						//2019.01.26 Generate Pass Target Arrow Hint
						_learningPassHint.transform.localPosition = targetPosition;
					}
					else if(currentPerspectiveIs3PP && currentModeIsLearning){
						ModeManagerLite.Instance._canvasContinue.SetActive(true);
					}

					#endregion
					
					#region Invoke pass ball event
					//2019.01.26 Check when to invoke pass ball event
					//
					//  Normal   3PP X ---> PassBallSMBehaviour
					//  Normal   1PP X ---> PassBallSMBehaviour
					//  Normal   1PP O ---> 
					//  Learning 3PP X ---> PassBallSMBehaviour
					//  Learning 1PP X ---> PassBallSMBehaviour
					//  Learning 1PP O --->

					if(!_isPassBall){
						if(!currentModeIsLearning && !currentPerspectiveIs3PP && _needToPassBall && _activatePassBall){
							_ball._gameObject.GetComponent<BasketballLite>().InvokePassBall();
							_isPassBall = true;
						}
						else if(currentModeIsLearning && !currentPerspectiveIs3PP && _needToPassBall && _activatePassBall){
							_ball._gameObject.GetComponent<BasketballLite>().InvokePassBall();
							_isPassBall = true;
						}
					}
					#endregion

					// currentModeIsLearning
					// currentPerspectiveIs3PP
					// _needToPassBall
					// _activatePassBall
					// _userWantToContinue
					// _playerDirectionIsRight

					//2019.01.25 New conditions: need_to_pass_ball, activate_pass_ball
					if( ( !currentModeIsLearning && ( currentPerspectiveIs3PP || 
					                                   ( !currentPerspectiveIs3PP && ( !_needToPassBall || 
													                                    ( _needToPassBall && _activatePassBall ) )) ) ) || 
					    (  currentModeIsLearning && ( (currentPerspectiveIs3PP && _userWantToContinue )|| 
						                               ( !currentPerspectiveIs3PP && ( (!_needToPassBall && _playerDirectionIsRight && _userWantToContinue) || 
													                                      (_needToPassBall && _playerDirectionIsRight && _activatePassBall && _userWantToContinue))))))
					{
						_currentState = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
						_timeCounter++;
						_isPassBall = false;
						ModeManagerLite.Instance._canvasContinue.SetActive(false);
					}

					/*
					if( (!current_mode_is_learning) || (current_mode_is_learning && user_want_to_continue &&
							  (current_perspective_is_3PP || (!current_perspective_is_3PP && player_direction_is_right)))){
						current_state = Constants.ExecutorState.CHECK_CURRENT_TIME_RUN_BAG;
						time_counter++;
					}
					*/

					if(_timeCounter > _maxTime){
						_currentState = Constants.ExecutorState.LAST_THROW;
						_isPassBall = false;
					}
					break;
				case Constants.ExecutorState.LAST_THROW:
					
					Players[_currentBallHolder-1]._playerMovement.SmoothLookAt(GameObject.FindGameObjectWithTag("hoop"));
					Animator playerAnim = Players[_currentBallHolder-1]._animator;
					bool isDribblingLeft = Players[_currentBallHolder-1]._player.transform.InverseTransformPoint(Defenders[_currentBallHolder-1]._player.transform.position).z < 0;
					playerAnim.SetBool("leftDribble", false);
					playerAnim.SetBool("rightDribble", false);
					bool hasInvokeDribbling = playerAnim.GetBool(isDribblingLeft?"leftDribble":"rightDribble");
					_ball._animator.SetBool("leftDribble", false);
					_ball._animator.SetBool("rightDribble", false);
					_ball._animator.enabled = false;
					/*
					if(Players[_currentBallHolder-1]._animator.GetLayerWeight(1) > 0.9f){
						Players[_currentBallHolder-1]._animator.SetBool(_isDribblingHash, false);
						Players[_currentBallHolder-1]._animator.SetLayerWeight(1, 0.0f);
						Players[_currentBallHolder-1]._animator.SetTrigger(_resetDribblingHash);
						_ball._animator.SetTrigger(_resetDribblingHash);
						_ball._animator.SetBool(_isScreenHash, false);
						_ball._animator.enabled = false;
					}
					*/
					Vector3 passer_position = Players[_currentBallHolder-1]._player.transform.localPosition;
					passer_position.y = 2.0f;
					// 2019.02.01 Slightly forward to generate more accurate ball trajectory begin position (closer to hand position)
					//passer_position += players[_currentBallHolder-1].player.transform.forward * 0.5f;
					#region Offender Throw Ball
					_ball._gameObject.GetComponent<BasketballLite>().GenerateThrowBallTrajectory(passer_position, ModeManagerLite.Instance._hoopPosition);
					Players[_currentBallHolder-1]._animator.SetBool("holdBall", false);
					Players[_currentBallHolder-1]._animator.SetTrigger("throwBall");
					#endregion

					#region Defender Block
					Vector3 targetPosition2 = Players[_currentBallHolder-1]._player.transform.position;
					if(Vector3.Distance(Defenders[_currentBallHolder-1]._player.transform.position, targetPosition2) > 1.5f){
						targetPosition2 += (Defenders[_currentBallHolder-1]._player.transform.position - targetPosition2).normalized * 1.2f;
						targetPosition2.y = 0.0f;
						Defenders[_currentBallHolder-1]._animator.SetFloat("speed", 2.0f);
						Defenders[_currentBallHolder-1]._animator.SetFloat("direction", 1.0f);
						Defenders[_currentBallHolder-1]._navmeshAgent.SetDestination(targetPosition2);
					}
					Defenders[_currentBallHolder-1]._animator.SetTrigger("block");
					#endregion


					_currentState = Constants.ExecutorState.END;
					break;
				case Constants.ExecutorState.END:
					
					//TODO:Re-enable state gracefully
					ModeManagerLite.Instance._canvasContinue.SetActive(false);
					Defenders[_currentBallHolder-1]._animator.SetFloat("speed", 0.0f);
					ModeManagerLite.Instance._tacticIsExecuting = false;
					_learningPassHint.transform.localPosition = new Vector3(100.0f, 100.0f, 100.0f);
					_canvasSetting.GetComponent<Canvas>().enabled = true;
					_canvasControlState.SetActive(false);
					//_canvasSetting.GetComponent<CanvasDelayMove>().distance = 2.0f;
					_canvasSetting.transform.localScale = new Vector3(0.01f, 0.01f, 1f);
					
					CameraManager.Instance.StopRecordRotation();
					StopCoroutine(_coroutine);
					_coroutine = null;
					break;	
			}

			yield return new WaitForSeconds(UPDATE_RATE);
		}
	}

	public void ClearRunPathList(){
		for(int i=0;i<_runpathRenderers.Count;i++){
			Destroy(_runpathRenderers[i].gameObject);
		}
		_runpathRenderers.Clear();
		for(int i=0;i<_screenBarPrefabs.Count;i++){
			Destroy(_screenBarPrefabs[i].gameObject);
		}
		_screenBarPrefabs.Clear();
	}

	public bool CheckReadyForPassBall(){
		bool isReady = false;
		bool currentModeIsLearning = (ModeManagerLite.Instance._mode == Constants.Mode.LEARNING) ? true : false;
		bool currentPerspectiveIs3PP = (ModeManagerLite.Instance._perspective == Constants.Perspective.THIRD_PP) ? true : false;
		
		if(!currentModeIsLearning && currentPerspectiveIs3PP){
			isReady = true;
		}
		else if(!currentModeIsLearning &&  !currentPerspectiveIs3PP && !_needToPassBall){
			isReady = true;
		}
		else if(currentModeIsLearning && currentPerspectiveIs3PP){
			isReady = true;
		}
		else if(currentModeIsLearning &&  !currentPerspectiveIs3PP && !_needToPassBall ){
			isReady = true;
		}
		return isReady;
	}

	public void OnTriggerContinue(bool isTrigger){
		if(ModeManagerLite.Instance._canvasContinue.activeInHierarchy){
			_userWantToContinue = true;
		}
	}

	public void ActivatePassBall(bool isActivate){
		if(_currentState == Constants.ExecutorState.CHECK_FINISH){	
			if(!_activatePassBall){
				_activatePassBall = isActivate;
			}
		}
	}

	private void GenerateRunPathGuidance(List<Vector2> path, bool isDribbling, int invokeId){
		// 2018.12.30 Render run paths
		LineRenderer lineRenderer =  ((GameObject)Instantiate(Resources.Load("Prefabs/PlayerRunPathRenderer"))).GetComponent<LineRenderer>();
		lineRenderer.transform.parent = _runpathRendererParent.transform;
		
		Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(_runpathColorEnd[invokeId], 0.0f), new GradientColorKey(_runpathColorEnd[invokeId], 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0.2f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
		);
		lineRenderer.colorGradient = gradient;
		if(isDribbling){
			int coils = (int)Mathf.Round((path[path.Count-1] - path[0]).magnitude*1.5f);
			lineRenderer.positionCount = coils*3;
			lineRenderer.widthMultiplier = 2.0f;

			MyTools tools = new MyTools();
			Vector2 s = path[0];
			Vector2 e = path[path.Count-1];
			List<Vector2> spring = tools.GenerateSpringPoints(s, e, 0.2f, lineRenderer.positionCount/3);
			for(int i=0 ; i<lineRenderer.positionCount ; i++){
				lineRenderer.SetPosition(i, new Vector3(spring[i].x, 0.1f, spring[i].y));
			}
		}
		else{
			lineRenderer.positionCount = path.Count;
			for(int i=0 ; i<path.Count ; i++){
				lineRenderer.SetPosition(i, new Vector3(path[i].x, 0.1f, path[i].y));
			}
		}
		_runpathRenderers.Add(lineRenderer);
	}
}
