﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallProjectorFollower : MonoBehaviour
{
    GameObject _ball;
    private void Awake()
    {
        _ball = GameObject.FindGameObjectWithTag("Basketball");
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 newPosition = _ball.transform.position;
        newPosition.y += 3.0f;
        this.transform.position = newPosition;
    }
}
