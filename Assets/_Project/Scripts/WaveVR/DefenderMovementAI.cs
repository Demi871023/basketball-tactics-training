﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DefenderMovementAI : MonoBehaviour
{
	private NavMeshAgent _agent;
	private Animator _animator;
	private Transform _transform;
    private const float ROTATE_RATE = 15.0f;
	private IEnumerator _passCoroutine;
	private float _angle;
	private Quaternion _targetRotation;

	private void Awake(){
		_agent = GetComponent<NavMeshAgent>();
		_animator = GetComponent<Animator>();
		_transform = this.transform;
	}
	private void Update(){
		_animator.SetFloat("speed", _agent.velocity.magnitude);
		
		if(!(this.name == "Defender"+TacticExecuterLite.Instance._currentBallHolder)){
			//Check it is not the defender who defend the ball holder
			{
				_animator.SetBool("defend", false);
				_animator.SetFloat("direction", Quaternion.LookRotation(_agent.velocity).eulerAngles.y/180.0f);

                Vector3 targetDir = Vector3.zero -  ModeManagerLite.Instance._hoopPosition;//CourtManager.Instance.BasketPosition;
                targetDir.y = _transform.position.y;
                _transform.rotation = Quaternion.LookRotation(targetDir);
			}
		}
		else{
			if(_agent.velocity.magnitude > 0.5f)
				_animator.SetBool("defend", false);
			else
				_animator.SetBool("defend", true);
		}
		
	}

    public void SmoothLookAt(GameObject target){
		_passCoroutine = UpdateRotation(target);
		_angle = 360.0f;
		Transform transform = this.transform;
		Quaternion currentRotation = transform.rotation;
		if(target != null){
			Vector3 targetPosition = target.transform.position;
			targetPosition.y = transform.position.y;
			_targetRotation = Quaternion.LookRotation(targetPosition - transform.position, transform.up);
		}
		else{
			Vector3 player2hoopDir = ModeManagerLite.Instance._hoopPosition - transform.position;
			Vector3 player2ballDir = GameObject.FindGameObjectWithTag("Basketball").transform.position - transform.position;
			Vector3 targetDir = (player2ballDir + player2hoopDir)/2; targetDir.y = 0.0f;
			_targetRotation = Quaternion.LookRotation(targetDir, transform.up);
		}
		_angle = Quaternion.Angle(currentRotation, _targetRotation);
		_animator.SetFloat("angle", _angle);
		_animator.SetFloat("speed", 0.2f);

		StartCoroutine(_passCoroutine);
	}

	private IEnumerator UpdateRotation(GameObject target){
		//Quaternion.Slerp(passer.transform.rotation, Quaternion.LookRotation(catcher.transform.position - passer.transform.position, passer.transform.up), Time.deltaTime * rotateRate);
		while(true){
			Quaternion currentRotation = this.transform.rotation;
			this.transform.rotation =  Quaternion.Slerp(currentRotation, _targetRotation, Time.deltaTime * ROTATE_RATE);
			
			_angle = Quaternion.Angle(currentRotation, _targetRotation);
			if(_angle < 20.0f){
				StopCoroutine(_passCoroutine);
				_animator.SetFloat("speed", 0.0f);
			}
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	// Enable\Disable the rendering of defender when setting menu go through
	private void OnTriggerStay(Collider other) {
		if(other.name == "SettingCanvas"){
			Transform[] children = this.GetComponentsInChildren<Transform>(true);
			for(int i=0;i<children.Length;i++){
				if(children[i].name == "MyCombinedMesh"){
					children[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
				}
			}
			//this.GetComponent<AnimationInstancing.AnimationInstance>().enabled = false;
		}
	}

	private void OnTriggerExit(Collider other) {
		if(other.name == "SettingCanvas"){
			if(ModeManagerLite.Instance._defender != Constants.Defender.NO_DEFENDER){
				Transform[] children = this.GetComponentsInChildren<Transform>(true);
				for(int i=0;i<children.Length;i++){
					if(children[i].name == "MyCombinedMesh"){
						children[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
					}
				}
			}
				//this.GetComponent<AnimationInstancing.AnimationInstance>().enabled = true;
		}
	}
}
