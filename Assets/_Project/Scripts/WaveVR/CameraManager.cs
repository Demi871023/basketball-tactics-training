﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using System.Text;
using System.IO;

public class CameraManager : Singleton<CameraManager> {
	public Camera wave_camera;
	private DateTime start_time;
	private JSONObject camera_rotation_json;
	private int update_counter;

	public void IvokeCameraRotationRecorder(){
		camera_rotation_json = new JSONObject();
		update_counter = 0;
		start_time = DateTime.Now;
		InvokeRepeating("UpdateRecordRotation", 0.0f, 0.016f);
	}

	private void UpdateRecordRotation(){
		JSONArray current_rotation = new JSONArray();
		current_rotation.Add( Math.Round(wave_camera.transform.rotation.eulerAngles.x, 3));
		current_rotation.Add( Math.Round(wave_camera.transform.rotation.eulerAngles.y, 3));
		current_rotation.Add( Math.Round(wave_camera.transform.rotation.eulerAngles.z, 3));
		
		camera_rotation_json.Add(update_counter.ToString(), current_rotation);
		update_counter++;
	}

	public void StopRecordRotation(){
		string json_string = camera_rotation_json.ToString();
		byte[] texts = new byte[] { };
		try{
			byte [] curTexts = Encoding.UTF8.GetBytes(json_string);
			texts = curTexts;
		}catch(Exception e){
			Debug.LogError("Message:" + e.Message);
		}
		DateTime dt = DateTime.Now;
		TimeSpan ts = dt.Subtract(start_time);

		string file_path = PathForDocumentsFile("Rotation/"+dt.Year.ToString()+dt.Month.ToString().PadLeft(2, '0')+dt.Day.ToString().PadLeft(2, '0')+"_"+
												dt.Hour.ToString().PadLeft(2, '0')+dt.Minute.ToString().PadLeft(2, '0')+dt.Second.ToString().PadLeft(2, '0')+"_"+
												ts.TotalMilliseconds.ToString()+"_"+
												TacticManager.Instance._currentChosenTactic.tacticName+"_"+
												(int)ModeManagerLite.Instance._perspective+".json");
		if(File.Exists(file_path)){
			File.Delete(file_path);
		}
		//Debug.LogError(file_path);
		/*
		using(FileStream fs = new FileStream(file_path, FileMode.OpenOrCreate, FileAccess.Write)){
			if(fs != null){
				Debug.LogError("Saved");
				fs.Write(texts, 0, texts.Length);
				fs.Flush();
				fs.Dispose();
			}
		}
		*/
		CancelInvoke("UpdateRecordRotation");
	}

    public string PathForDocumentsFile( string filename ) 
	{ 
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			string path = Application.dataPath.Substring( 0, Application.dataPath.Length - 5 );
			path = path.Substring( 0, path.LastIndexOf( '/' ) );
			return Path.Combine( Path.Combine( path, "Documents" ), filename );
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			string path = Application.persistentDataPath;	
			path = path.Substring(0, path.LastIndexOf( '/' ) );	
			return Path.Combine (path, filename);
		}
		else 
		{
			string path = Application.dataPath;	
			path = path.Substring(0, path.LastIndexOf( '/' ) );
			return Path.Combine (path, filename);
		}
	}
}
