﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using wvr;

[RequireComponent(typeof(LineRenderer))]
public class Teleport : MonoBehaviour
{
    /*
    [Tooltip("Any layers the raycast should not affect")]
	public LayerMask _excludeLayers;
    public GameObject _testController;
    private GameObject _halfSphere;
    private GameObject _canvasEyeBlink;
    private GameObject _canvasSetting;
    private Canvas _canvasSettingCanvas;
    private GameObject _target;

    #region Controller Settings

    private WaveVR_Controller.Device _controller;
    private GameObject _dominantController = null;
    private WaveVR_Beam _beam;
    private WaveVR_ControllerPointer _pointer;

    private Vector3 ControllerForward{
        get {
			#if UNITY_EDITOR
                return _testController.transform.forward;
			#else
			    return _controller.transform.rot * Vector3.forward;
            #endif
		}
    }
    private Vector3 ControllerUp{
        get {
			#if UNITY_EDITOR
			    return _testController.transform.up;
            #else
                return _controller.transform.rot * Vector3.up;
			#endif
		}
    }
    private Vector3 ControllerRight{
        get {
			#if UNITY_EDITOR
			    return _testController.transform.right;
			#else
			    return _controller.transform.rot * Vector3.right;
            #endif
		}
    }
    private Vector3 ControllerPosition{
        get {
			#if UNITY_EDITOR
			    GameObject input_manager = GameObject.FindGameObjectWithTag("InputManager");
			    return _controller.transform.pos + input_manager.transform.position;
            #else
                GameObject input_manager = GameObject.FindGameObjectWithTag("InputManager");
			    return _controller.transform.pos + input_manager.transform.position;
            #endif
		}
    }
    
    #endregion

    #region Teleport Curve

    [Tooltip("Horizontal distance of end point from controller")]
	public float DISTANCE = 15.0f;
	[Tooltip("Vertical of end point from controller")]
	public float DROP_HEIGHT = 3.0f;
	[Tooltip("Height of bezier control (0 is at mid point)")]
	public float CONTROL_HEIGHT = 10.0f;
    
    private Vector3 PStart{
        get{
            return ControllerPosition;
        }
    }
    private Vector3 Control{
        get{
            return PStart + (_end - PStart) * 0.5f + ControllerUp * CONTROL_HEIGHT;
        }
    }
    private Vector3 _end;
    private Vector3 _hitPoint;
    private LayerMask _hitLayer;

    private bool _isContact;
    private bool _unreachable;

    [Tooltip("How manu angles from world up the surface can point and still be valid. Avoids casting onto walls.")]
	public float SURFACE_ANGLE = 5;

    [Tooltip("How many segments to use for curve, must be at least 3. More segments = better quality")]
    private int SEGMENTS = 20;
    private LineRenderer _arcRenderer;
    private Color COLOR_REACHABLE = new Color(76.0f/255.0f, 225.0f/225.0f, 225.0f/255.0f);
    private Color COLOR_UNREACHABLE = new Color(255.0f/255.0f, 23.0f/225.0f, 21.0f/255.0f);

    #endregion

    private void Awake(){
        _arcRenderer = GetComponent<LineRenderer>();
        _canvasEyeBlink = GameObject.FindGameObjectWithTag("Panel_EyeBlink");
        _canvasSetting = GameObject.FindGameObjectWithTag("Canvas_setting");
        _canvasSettingCanvas = _canvasSetting.GetComponent<Canvas>();
    }

    private void Start()
    {
        _controller = WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant);
        _halfSphere = (GameObject)Instantiate(Resources.Load("Prefabs/half_sphere"));
        _halfSphere.SetActive(false);
        _target = (GameObject)Instantiate(Resources.Load("Prefabs/target"));
        _arcRenderer.positionCount = SEGMENTS;
    }

    private void OnEnable() {
        WaveVR_Utils.Event.Listen (WaveVR_Utils.Event.CONTROLLER_MODEL_LOADED, OnControllerLoaded);
    }

    // Update is called once per frame
    private void Update()
    {
        if(!ModeManagerLite.Instance._tacticIsExecuting && ModeManagerLite.Instance._perspective == Constants.Perspective.THIRD_PP){
            if(!_canvasSettingCanvas.enabled){
                if(WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant).GetPress(WVR_InputId.WVR_InputId_Alias1_Touchpad)){
                    OnTeleport();
                }
                

                if(WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant).GetPressUp(WVR_InputId.WVR_InputId_Alias1_Touchpad)){
                    OnTeleportEnd();
                }
            }

            if(Input.GetKey(KeyCode.Q)){
                OnTeleport();
            }

            if(Input.GetKeyUp(KeyCode.Q)){
                OnTeleportEnd();
            }
        }
    }

    private void OnControllerLoaded(params object[] args)
    {
        WaveVR_Controller.EDeviceType _type = (WaveVR_Controller.EDeviceType)args [0];
        if (_type == WaveVR_Controller.EDeviceType.Dominant)
        {
            this._dominantController = (GameObject)args [1];
            listControllerObjects(this._dominantController);
        }
    }

    private void listControllerObjects(GameObject ctrlr)
    {
        if (ctrlr == null)
            return;

        _beam = null;
        _pointer = null;

        // Get all children.
        GameObject[] _objects = new GameObject[ctrlr.transform.childCount];
        for (int i = 0; i < ctrlr.transform.childCount; i++)
            _objects[i] = ctrlr.transform.GetChild (i).gameObject;

        // Find beam.
        for (int i = 0; i < _objects.Length; i++)
        {
            _beam = _objects [i].GetComponentInChildren<WaveVR_Beam> ();
            if (_beam != null)
                break;
        }
        if (_beam != null)
            Debug.Log ("Find beam: " + _beam.name);

        // Find pointer.
        for (int i = 0; i < _objects.Length; i++)
        {
            _pointer = _objects [i].GetComponentInChildren<WaveVR_ControllerPointer> ();
            if (_pointer != null)
                break;
        }
        if (_pointer != null)
            Debug.Log ("Find pointer: " + _pointer.name);
    }

    private Vector3 SampleCurve(Vector3 start, Vector3 end, Vector3 control, float t){
		// Interpolate along line S0: control - start;
		Vector3 Q0 = Vector3.Lerp(start, control, t);
		// Interpolate along line S1: S1 = end - control;
		Vector3 Q1 = Vector3.Lerp(control, end, t);
		// Interpolate along line S2: Q1 - Q0
		Vector3 Q2 = Vector3.Lerp(Q0, Q1, t);
		return Q2; // Q2 is a point on the curve at time t
	}

    private void SetCurveVisual(Color color){
        Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0.1f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
		);
		_arcRenderer.colorGradient = gradient;
    }

    private void OnTeleport(){
        if(_beam != null)   _beam.enabled = false;
        if(_pointer != null)    _pointer.enabled = false;

        _isContact = false;
        _end = _hitPoint = ControllerPosition + ControllerForward * DISTANCE + (ControllerUp * -1.0f) * DROP_HEIGHT;
        _arcRenderer.positionCount = SEGMENTS;

        RaycastHit hit;
        Vector3 last = PStart;
        float recip = 1.0f/ (float)(SEGMENTS - 1);
        for (int i = 0; i < SEGMENTS; ++i) {
            float t = (float)i * recip;
            Vector3 sample = SampleCurve(PStart, _end, Control, Mathf.Clamp01(t));
            if(!_isContact)
                _arcRenderer.SetPosition(i, sample);

            if (Physics.Linecast(last, sample, out hit)) {
                float angle = Vector3.Angle(Vector3.up, hit.normal);
                if (angle < SURFACE_ANGLE) {
                    _arcRenderer.positionCount = i;
                    _hitPoint = hit.point;
                    _hitLayer = hit.transform.gameObject.layer;
                    _isContact = true;
                    _halfSphere.SetActive(true);
                    _halfSphere.transform.position = _hitPoint;
                    SetCurveVisual(COLOR_REACHABLE);
                    break;
                }
            }
            last = sample;
        }
        
        _unreachable = (_excludeLayers.value == (_excludeLayers.value | (1 << _hitLayer.value)));
        if(!_isContact || _unreachable){
            SetCurveVisual(COLOR_UNREACHABLE);
            _halfSphere.SetActive(false);
        }
    }

    private void OnTeleportEnd(){
        _arcRenderer.positionCount = 0;
                    
        if(_beam != null)   _beam.enabled = true;
        if(_pointer != null)    _pointer.enabled = true;
        if(_isContact && !_unreachable){
            Vector3 new_pos = _hitPoint;
            _target.transform.position = new_pos;

            GameObject inputManager = GameObject.FindGameObjectWithTag("InputManager");
            inputManager.GetComponent<CameraController>().target = _target;
            if(_hitPoint.y > 5.0f){
                inputManager.GetComponent<CameraController>().root_height = 7.0f;
            }
            else{
                inputManager.GetComponent<CameraController>().root_height = Constants.INIITIAL_HEIGHT;
            }

            inputManager.transform.position = new_pos;
            _canvasEyeBlink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();
        }
        _halfSphere.SetActive(false);
    }
    */
}
