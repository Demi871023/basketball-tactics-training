﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using System.IO;
using UnityEngine.UI;

public class CommandReceiver : MonoBehaviour
{
    public Image _imgPerspective;
    public Sprite[] _spritePerspective;

    public bool _hasNewTactic = false;
    public StringBuilder _sb = new StringBuilder();
    public string _currentReceiveJson;
	
	private int _perspective;

    public string _localIP;

    public void Start() {
        this.StartServer();
        System.Threading.Thread.Sleep(10);
    }
    /*
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    */

    public void Update()
    {      
        if (_hasNewTactic == true)
        {
			ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)_perspective);
			_imgPerspective.sprite = _spritePerspective[_perspective];
			_hasNewTactic = false;
		}	
    }

    public enum TestMessageOrder
    {
        NotConnected,
        Connected,
        SendFirstMessage,
        ReceiveFirstMessageReply,
        SendSecondMessage,
        ReceiveSecondMessageReply,
        SendThirdMessage,
        ReceiveThirdMessageReply,
        Error,
        Done
    }
    protected TcpListener m_tcpListener;
    protected Socket m_testClientSocket;
    protected byte[] m_readBuffer;

    [SerializeField]
    protected TestMessageOrder m_testClientState;
    public void StartServer()
    {
        IPHostEntry host;
        _localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                _localIP = ip.ToString();
                break;
            }
        }

        Debug.Log(_localIP);

        m_tcpListener = new TcpListener(IPAddress.Parse(_localIP), 2044);
        m_tcpListener.Start();
        StartListeningForConnections();
    }
    void StartListeningForConnections()
    {
        m_tcpListener.BeginAcceptSocket(AcceptNewSocket, m_tcpListener);
        Debug.Log("SERVER ACCEPTING NEW CLIENTS");
    }

    void AcceptNewSocket(System.IAsyncResult iar)
    {
        m_testClientSocket = null;
        m_testClientState = TestMessageOrder.NotConnected;
        m_readBuffer = new byte[32768];
        if(m_tcpListener != null) { 
            try
            {
                m_testClientSocket = m_tcpListener.EndAcceptSocket(iar);
            }
            catch (System.Exception ex)
            {
                Debug.Log(string.Format("Exception on new socket: {0}", ex.Message));
            }
            m_testClientSocket.NoDelay = true;
            m_testClientState = TestMessageOrder.Connected;
            BeginReceiveData();
            //SendTestData();
            StartListeningForConnections();
        }
           
    }

    void BeginReceiveData()
    {
        m_testClientSocket.BeginReceive(m_readBuffer, 0, m_readBuffer.Length, SocketFlags.None, EndReceiveData, null);
  
    }
    void EndReceiveData(System.IAsyncResult iar)
    {
        int numBytesReceived = m_testClientSocket.EndReceive(iar);
        Debug.Log(numBytesReceived);
        if (numBytesReceived > 0)
        {
            _sb.Append(Encoding.ASCII.GetString(m_readBuffer, 0, numBytesReceived));
            //Debug.Log("sb.Length:"+sb.Length);
        }
        else {
			Debug.Log("(<0)sb.Length:"+_sb.Length);
            if (_sb.Length > 1) {
                string strContent = _sb.ToString();
                ProcessData(strContent);
                _sb.Length = 0;//Clean String Builder
				//Debug.Log("After:"+sb.ToString());
                m_testClientSocket.Close();
            }
        }
       
        BeginReceiveData();
    }
    void ProcessData(string jsonString)
    {
        JSONNode jsonData = JSON.Parse(jsonString);
        _currentReceiveJson = jsonString;
		Debug.Log(jsonString);

		_perspective = jsonData["Perspective"];
    
        _hasNewTactic = true;
    }
    void OnDestroy()
    {
        if (m_testClientSocket != null)
        {
            m_testClientSocket.Close();
            m_testClientSocket = null;
        }
        if (m_tcpListener != null)
        {
            m_tcpListener.Stop();
            m_tcpListener = null;
        }
    }

    // Unused send methods
    void SendTestData()
    {
        Debug.Log(string.Format("Server: Client state: {0}", m_testClientState));
        switch (m_testClientState)
        {
            case TestMessageOrder.Connected:
                SendMessageOne();
                break;
            //case TestMessageOrder.SendFirstMessage:
            //break;
            case TestMessageOrder.ReceiveFirstMessageReply:
                SendMessageTwo();
                break;
            //case TestMessageOrder.SendSecondMessage:
            //break;
            case TestMessageOrder.ReceiveSecondMessageReply:
                SendMessageTwo();
                break;
            case TestMessageOrder.SendThirdMessage:
                break;
            case TestMessageOrder.ReceiveThirdMessageReply:
                m_testClientState = TestMessageOrder.Done;
                Debug.Log("ALL DONE");
                break;
            case TestMessageOrder.Done:
                break;
            default:
                Debug.LogError("Server shouldn't be here");
                break;
        }
    }
    void SendMessageOne()
    {
        m_testClientState = TestMessageOrder.SendFirstMessage;
        byte[] newMsg = new byte[] { 1, 100, 101, 102, 103, 104 };
        SendMessage(newMsg);
    }
    void SendMessageTwo()
    {
        m_testClientState = TestMessageOrder.SendSecondMessage;
        byte[] newMsg = new byte[] { 3, 100, 101, 102, 103, 104, 105, 106 };
        SendMessage(newMsg);
    }
    void SendMessageThree()
    {
        m_testClientState = TestMessageOrder.SendThirdMessage;
        byte[] newMsg = new byte[] { 5, 100, 101, 102, 103, 104, 105, 106, 107, 108 };
        SendMessage(newMsg);
    }
    void SendMessage(byte[] msg)
    {
       	// string temp = ImageReceiver.CompileBytesIntoString(msg);
       	// Debug.Log(string.Format("Server sending: '{0}'", temp));
        m_testClientSocket.BeginSend(msg, 0, msg.Length, SocketFlags.None, EndSend, msg);
    }
    void EndSend(System.IAsyncResult iar)
    {
        m_testClientSocket.EndSend(iar);
        byte[] msgSent = (iar.AsyncState as byte[]);
        string temp = CompileBytesIntoString(msgSent);
        Debug.Log(string.Format("Server sent: '{0}'", temp));
    }
    public static string CompileBytesIntoString(byte[] msg, int len = -1)
    {
        string temp = "";
        int count = len;
        if (count < 1)
        {
            count = msg.Length;
        }
        for (int i = 0; i < count; i++)
        {
            temp += string.Format("{0} ", msg[i]);
        }
        return temp;
    }

}



