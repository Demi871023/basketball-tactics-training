﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ModeManagerLite : Singleton<ModeManagerLite> {
	
	public bool _tacticIsExecuting;
	
	[Header("Status")]
	public Constants.Perspective _perspective;
    public Constants.Mode _mode;
    public Constants.Defender _defender;
	
	public GameObject _hoop;
	public Vector3 _hoopPosition;

	public GameObject _threeppPosition;
	public GameObject _canvasEyeblink;
	public GameObject _canvasContinue;
    public GameObject _waveVR;

    private GameObject _inputManager;

	public void ChangeCameraPerspective(Constants.Perspective perspective){
		//   1. Change camera perspective bewteen Player 1~5 
        //   2. From Player 1~5 change camera perspective to Main Camera(Third person perspective)
        //   3. Change within Third person perspective
        //   4. From Main Camera(Third person perspective) change camera perspective to Player 1~5
        if(this._perspective != Constants.Perspective.THIRD_PP && perspective != Constants.Perspective.THIRD_PP){
            GameObject player = GameObject.FindGameObjectWithTag("Player" + (int)this._perspective);
            SetRenderability(player, true, false);

            GameObject nextPerspectivePlayer = GameObject.FindGameObjectWithTag("Player" + (int)perspective);
            SetRenderability(nextPerspectivePlayer, false, false);

            //2019.02.24 Slow down the current perspective player
            player.GetComponent<NavMeshAgent>().speed = 3.5f;
            nextPerspectivePlayer.GetComponent<NavMeshAgent>().speed = 1.6f;

            _inputManager.GetComponent<CameraController>().target = nextPerspectivePlayer;
            _inputManager.GetComponent<CameraController>().root_height = Constants.INIITIAL_HEIGHT;
            _inputManager.transform.position = nextPerspectivePlayer.transform.position;
            _inputManager.transform.rotation = nextPerspectivePlayer.transform.rotation;
        }
        else if(this._perspective != Constants.Perspective.THIRD_PP && perspective == Constants.Perspective.THIRD_PP){
            GameObject player = GameObject.FindGameObjectWithTag("Player" + (int)this._perspective);
            SetRenderability(player, true, false);

            //2019.02.24 Slow down the current perspective player
            player.GetComponent<NavMeshAgent>().speed = 3.5f;

            //ChangeVR3PPPerspective(Constants.GodViewPosition.CENTER);
            _inputManager.GetComponent<CameraController>().target = _threeppPosition;
            _inputManager.GetComponent<CameraController>().root_height = 7.0f;
			_inputManager.transform.position = _threeppPosition.transform.position;
			_inputManager.transform.rotation = _threeppPosition.transform.rotation;
        }
        else if(this._perspective == Constants.Perspective.THIRD_PP && perspective == Constants.Perspective.THIRD_PP){
            //ChangeVR3PPPerspective(Constants.GodViewPosition.CENTER);
            _inputManager.GetComponent<CameraController>().target = _threeppPosition;
			_inputManager.transform.position = _threeppPosition.transform.position;
			_inputManager.transform.rotation = _threeppPosition.transform.rotation;
        }
        else{
            if((int)perspective != 0) {
                GameObject nextPerspectivePlayer = GameObject.FindGameObjectWithTag("Player" + (int)perspective);
                SetRenderability(nextPerspectivePlayer, false, false);

                //2019.02.24 Slow down the current perspective player
                nextPerspectivePlayer.GetComponent<NavMeshAgent>().speed = 1.6f;
                
                _inputManager.GetComponent<CameraController>().target = nextPerspectivePlayer;
                _inputManager.GetComponent<CameraController>().root_height = Constants.INIITIAL_HEIGHT;
                _inputManager.transform.position = nextPerspectivePlayer.transform.position;
                _inputManager.transform.rotation = nextPerspectivePlayer.transform.rotation;
                _waveVR.transform.position =  new Vector3(_waveVR.transform.position.x, 0.0f, _waveVR.transform.position.z);
            }
        }
        this._perspective = perspective;
        
		//Invoke eye blink
        _canvasEyeblink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();
    }

	public void SetRenderability(GameObject obj, bool isRender, bool hasDefender) {
        Transform[] ts = obj.transform.GetComponentsInChildren<Transform>();
		// Get every child elements of the Player/Defender
		// Find the body and hair meshes who owns the SkinnedMeshRenderer
		// Set ability of renderer to control the visibility of Defenders 
		for (int j = 0; j < ts.Length; j++)
		{
			if ((ts[j].gameObject.name == "busto"))
			{
				SkinnedMeshRenderer render = ts[j].gameObject.GetComponent<SkinnedMeshRenderer>();
				render.enabled = isRender;
			}
            else if((ts[j].gameObject.name == "capelli")){
                SkinnedMeshRenderer render = ts[j].gameObject.GetComponent<SkinnedMeshRenderer>();
				render.enabled = isRender;
            }
            else if((ts[j].gameObject.name == "hair")){
                MeshRenderer render = ts[j].gameObject.GetComponent<MeshRenderer>();
                render.enabled = isRender;
            }
            else if((ts[j].gameObject.name == "basketBall_SingleMesh")){
                SkinnedMeshRenderer render = ts[j].gameObject.GetComponent<SkinnedMeshRenderer>();
                render.enabled = isRender;
            }
            else if((ts[j].gameObject.name == "MyCombinedMesh")){
                SkinnedMeshRenderer render = ts[j].gameObject.GetComponent<SkinnedMeshRenderer>();
                render.enabled = isRender;
            }
		}
    }

    public void ChangeDefenderMode(Constants.Defender defender) {
        Tactic currentTactic = TacticManager.Instance._currentChosenTactic;
        GameObject ball = GameObject.FindGameObjectWithTag("Basketball");
        int ballHolderId = currentTactic.initialBallHolder;
        GameObject ballHolder = GameObject.FindGameObjectWithTag("Player" + (ballHolderId));

        switch(defender){
            case Constants.Defender.NO_DEFENDER:
                SetEnableDefender(false);
                break;
            case Constants.Defender.MODE1_DEFENDER:
                SetEnableDefender(true);
                for(int i=0 ; i<5 ; i++){
                    GameObject offender = GameObject.FindGameObjectWithTag("Player" + (i + 1).ToString());
                    GameObject defenderPlayer = GameObject.FindGameObjectWithTag("Defender" + (i + 1).ToString());
                    
                    Vector3 transformOffender = offender.transform.position - _hoopPosition;
                    Vector3 transBall = new Vector3(ball.transform.position.x, 0.0f, ball.transform.position.z) - _hoopPosition;

                    defenderPlayer.GetComponent<NavMeshAgent>().enabled = false;
                    float defenderX = (0.62f * transformOffender.x + 0.11f * transBall.x + 0.0f * _hoopPosition.x + _hoopPosition.x);
                    float defenderY = (0.62f * transformOffender.z + 0.11f * transBall.z + 0.0f * _hoopPosition.z + _hoopPosition.z);
                    defenderPlayer.transform.localPosition = new Vector3(defenderX, 0.0f, defenderY);
                    defenderPlayer.GetComponent<NavMeshAgent>().enabled = true;

                    defenderPlayer.transform.LookAt(new Vector3(ball.transform.position.x, 0.0f, ball.transform.position.z));
                    /*
                    if( (i+1) == TacticExecuterLite.Instance._currentBallHolder ){
                        defenderPlayer.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("20181017-2_Char00", 0.2f);
                    }
                    else{
                        defenderPlayer.GetComponent<AnimationInstancing.AnimationInstance>().CrossFade("HumanoidIdle", 0.2f);
                    }
                    */
                }
                break;
        }
        this._defender = defender;
    }   

    private void Awake(){
        _inputManager = GameObject.FindGameObjectWithTag("InputManager");
    }    
    private void Start () {
		_tacticIsExecuting = false;
		_hoopPosition = _hoop.transform.position;
        _defender = Constants.Defender.NO_DEFENDER;
        
	}

    private void SetEnableDefender(bool is_on) {
        _defender = (Constants.Defender)Convert.ToInt32(is_on);
        for(int i=0 ; i<5 ; i++){
            GameObject defenderPlayer = GameObject.FindGameObjectWithTag("Defender" + (i + 1).ToString());
            //defenderPlayer.GetComponent<AnimationInstancing.AnimationInstance>().enabled = is_on;
            SetRenderability(defenderPlayer, Convert.ToBoolean(is_on), true);
        }
    }
}
