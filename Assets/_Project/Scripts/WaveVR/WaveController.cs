﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using wvr;
using UnityEngine.UI;
using System;

public class WaveController : MonoBehaviour {
	/*
	public WVR_DeviceType device = WVR_DeviceType.WVR_DeviceType_Controller_Right;
	
	public Text _textTactic;
	public Text _textTacticName;
	public GameObject _canvasSetting;
	private Canvas _canvasSettingCanvas;
	public Image _imgPerspective;
	public Image _imgMode;
	public Image _imgDefender;

	public Sprite[] _spritePerspective;
	public Sprite[] _spriteMode;
	public Sprite[] _spriteDefender;

	private void Awake(){
		_canvasSettingCanvas = _canvasSetting.GetComponent<Canvas>();
	}
	private void Start(){
		InvokeRepeating("HoldBallVibration", 0.0f, 0.2f);
	}

	private void Update () {
		// WaveVR controller
		if(WaveVR_Controller.Input(device).GetPressDown(WVR_InputId.WVR_InputId_Alias1_Menu)){
			_canvasSettingCanvas.enabled = !_canvasSettingCanvas.enabled;
		}

		if(WaveVR_Controller.Input(device).GetPressDown(WVR_InputId.WVR_InputId_Alias1_Grip)){
			_canvasSettingCanvas.enabled = !_canvasSettingCanvas.enabled;
		}

		if(WaveVR_Controller.Input(device).GetPressDown(WVR_InputId.WVR_InputId_Alias1_Trigger)){
			_canvasSettingCanvas.enabled = !_canvasSettingCanvas.enabled;
		}

		if(WaveVR_Controller.Input(device).GetPressDown(WVR_InputId.WVR_InputId_Alias1_Touchpad)){
			TacticExecuterLite.Instance.OnTriggerContinue(true);
		}

		// Development control
		if (Input.GetMouseButtonDown(0)){
			TacticExecuterLite.Instance.OnSelectedTactic();
		}

		if (Input.GetKeyDown(KeyCode.LeftArrow)){
			TacticManager.Instance._currentSelectIndex--;
			if(TacticManager.Instance._currentSelectIndex < 0) TacticManager.Instance._currentSelectIndex = TacticManager.Instance._tactics.Count-1;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterLite.Instance.OnSelectedTactic();
		}

		if (Input.GetKeyDown(KeyCode.RightArrow)){
			TacticManager.Instance._currentSelectIndex++;
			if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterLite.Instance.OnSelectedTactic();
		}

		if(Input.GetKeyDown(KeyCode.M)){
			OnTriggerMode();
		}
		if(Input.GetKeyDown(KeyCode.P)){
			OnTriggerPerspective();
		}
		if(Input.GetKeyDown(KeyCode.C)){
			TacticExecuterLite.Instance.OnTriggerContinue(true);
		}
		if(Input.GetKeyDown(KeyCode.S)){
			TacticExecuterLite.Instance.InvokeExecution();
		}
		if(Input.GetKeyDown(KeyCode.D)){
			OnTriggerDefender();
		}

		if(Input.GetKeyDown(KeyCode.UpArrow)){
			OnChangeStair(true);
		}

		if(Input.GetKeyDown(KeyCode.DownArrow)){
			OnChangeStair(false);
		}
	}

	public void HoldBallVibration(){
		if(TacticExecuterLite.Instance._currentBallHolder == (int)ModeManagerLite.Instance._perspective){
			if(ModeManagerLite.Instance._tacticIsExecuting){
				if(!TacticExecuterLite.Instance._invokedAgent[TacticExecuterLite.Instance._currentBallHolder-1]._isDribbling)	
					WaveVR_Controller.Input(device).TriggerHapticPulse(50, WVR_InputId.WVR_InputId_Alias1_Touchpad);
			}
		}
	}

	public void OnSelectNextTactic(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			TacticManager.Instance._currentSelectIndex++;
			if(TacticManager.Instance._currentSelectIndex >= TacticManager.Instance._tactics.Count) TacticManager.Instance._currentSelectIndex = 0;
			TacticManager.Instance._currentChosenTactic = TacticManager.Instance._tactics[TacticManager.Instance._currentSelectIndex];
			TacticExecuterLite.Instance.OnSelectedTactic();
			_textTactic.text = TacticManager.Instance._currentSelectIndex.ToString();
			_textTacticName.text = TacticManager.Instance._currentChosenTactic.tacticName;

			ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			_imgPerspective.sprite = _spritePerspective[0];
			ModeManagerLite.Instance._canvasEyeblink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();

		}
	}

	public void OnTriggerPerspective(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_perspecitve = (int)ModeManagerLite.Instance._perspective + 1;
			if(new_perspecitve > (int)Constants.Perspective.FIRST_PP_5) new_perspecitve = 0;
			ModeManagerLite.Instance.ChangeCameraPerspective((Constants.Perspective)new_perspecitve);
			_imgPerspective.sprite = _spritePerspective[new_perspecitve];
		}
	}

	public void OnTriggerMode(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_mode = (int)ModeManagerLite.Instance._mode + 1;
			if(new_mode > (int)Constants.Mode.LEARNING) new_mode = 0;
			ModeManagerLite.Instance._mode = (Constants.Mode)new_mode;
			_imgMode.sprite = _spriteMode[new_mode];

			//Tactic3DExecuter.Instance.learning_pass_hint.SetActive(!Tactic3DExecuter.Instance.learning_pass_hint.gameObject.activeInHierarchy);
		}
	}

	public void OnTriggerDefender(){
		if(!ModeManagerLite.Instance._tacticIsExecuting){
			int new_defender = (int)ModeManagerLite.Instance._defender + 1;
			if(new_defender > (int)Constants.Defender.MODE1_DEFENDER) new_defender = 0;
			ModeManagerLite.Instance.ChangeDefenderMode((Constants.Defender)new_defender);
			_imgDefender.sprite = _spriteDefender[new_defender];
		}
	}

	public void OnChangeStair(bool is_up){
		GameObject input_manager = GameObject.FindGameObjectWithTag("InputManager");
		if(is_up){
			if(input_manager.GetComponent<CameraController>().root_height < 6.0f){
				input_manager.GetComponent<CameraController>().root_height = 7.0f;
				ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			}
			_imgPerspective.sprite = _spritePerspective[0];
		}
		else{
			if(input_manager.GetComponent<CameraController>().root_height > 1.0f){
				input_manager.GetComponent<CameraController>().root_height = Constants.INIITIAL_HEIGHT;
				ModeManagerLite.Instance.ChangeCameraPerspective(Constants.Perspective.THIRD_PP);
			}
			_imgPerspective.sprite = _spritePerspective[0];
		}
	}
	*/
}
