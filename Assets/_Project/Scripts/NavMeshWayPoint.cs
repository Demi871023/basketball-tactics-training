﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshWayPoint : MonoBehaviour {
	public bool _reachEndPoint;
	[SerializeField]
	private List<Vector2> _wayPoints;
	[SerializeField]
	private int _wayPointsCount;
	
	private NavMeshAgent _agent;
	private IEnumerator _coroutine;
	private const float UPDATE_RATE = 0.001F;

	private void Awake(){
		_agent = GetComponent<NavMeshAgent>();
	}
	// Use this for initialization
	private void Start () {
		_wayPointsCount = 0;
		_coroutine = Move();
	}

	public void SetWayPoints(List<Vector2> wayPoints){
		if(wayPoints != null){
			this._wayPoints = new List<Vector2>();
			for(int i=0;i<wayPoints.Count;i++){
				this._wayPoints.Add(wayPoints[i]);
			}
		}
	}

	public List<Vector2> GetWayPoints(){
		List<Vector2> return_list = new List<Vector2>();
		for(int i=0 ; i<this._wayPoints.Count ; i++){
			return_list.Add(this._wayPoints[i]);
		}
		return return_list;
	}

	public void StartToMove(){
		_reachEndPoint = false;
		_wayPointsCount = 0;
		StartCoroutine(_coroutine);
	}
	
	private IEnumerator Move(){
		while(true){
			if(_wayPointsCount < _wayPoints.Count){
				//TRICKY
				_agent.SetDestination(new Vector3(_wayPoints[_wayPointsCount].x, 0.0f, _wayPoints[_wayPointsCount].y));
				if(_agent.remainingDistance <= _agent.stoppingDistance){
					//Debug.LogWarning(agent.remainingDistance + "," + agent.stoppingDistance);
					if(!_reachEndPoint){
						_wayPointsCount++;
					}	
				}

				if(_wayPointsCount >= _wayPoints.Count){
					_reachEndPoint = true;
					StopCoroutine(_coroutine);
				}
			}
			yield return new WaitForSeconds(UPDATE_RATE);
		}
	}

	
}
