﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class PlayerMovement : MonoBehaviour {
	private Animator _anim;
	private Animator _ballAnim;
    private NavMeshAgent _agent;
	private NavMeshWayPoint _wayPoint;
	private const float ROTATE_RATE = 15.0f;
	private IEnumerator _passCoroutine;
	private GameObject _ball;
	private int _speedHash;

	private float _angle;
	private Quaternion _targetRotation;
	
	private void Awake(){
		_anim = this.GetComponent<Animator>();
        _agent = this.GetComponent<NavMeshAgent>();
		_wayPoint = this.GetComponent<NavMeshWayPoint>();
	}

	private void Start () {
		_ball = GameObject.FindGameObjectWithTag("Basketball");
		_speedHash = Animator.StringToHash("speed");
	}
	
	private void Update () {
		_anim.SetFloat(_speedHash, _agent.velocity.magnitude);
	}

	public void SmoothLookAt(GameObject target){
		_passCoroutine = UpdateRotation(target);
		_angle = 360.0f;
		Transform transform = this.transform;
		Quaternion currentRotation = transform.rotation;
		if(target != null){
			Vector3 targetPosition = target.transform.position;
			targetPosition.y = transform.position.y;
			_targetRotation = Quaternion.LookRotation(targetPosition - transform.position, transform.up);
		}
		else{
			Vector3 player2hoopDir = ModeManagerLite.Instance._hoopPosition - transform.position;
			Vector3 player2ballDir = GameObject.FindGameObjectWithTag("Basketball").transform.position - transform.position;
			Vector3 targetDir = (player2ballDir + player2hoopDir)/2; targetDir.y = 0.0f;
			_targetRotation = Quaternion.LookRotation(targetDir, transform.up);
		}
		_angle = Quaternion.Angle(currentRotation, _targetRotation);
		_anim.SetFloat("angle", _angle);
		_anim.SetFloat("speed", 0.2f);

		StartCoroutine(_passCoroutine);
	}

	private IEnumerator UpdateRotation(GameObject target){
		//Quaternion.Slerp(passer.transform.rotation, Quaternion.LookRotation(catcher.transform.position - passer.transform.position, passer.transform.up), Time.deltaTime * rotateRate);
		while(true){
			Quaternion currentRotation = this.transform.rotation;
			this.transform.rotation =  Quaternion.Slerp(currentRotation, _targetRotation, Time.deltaTime * ROTATE_RATE);
			
			_angle = Quaternion.Angle(currentRotation, _targetRotation);
			if(_angle < 20.0f){
				StopCoroutine(_passCoroutine);
				_anim.SetFloat("speed", 0.0f);
			}
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}
}
