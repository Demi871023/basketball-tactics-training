﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using wvr;

public class TestHMDHeight : MonoBehaviour
{   
    Text txt;
    public GameObject test;
    // Start is called before the first frame update
    void Start()
    {
        txt = this.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //txt.text = WaveVR_Controller.Input (WaveVR_Controller.EDeviceType.Dominant).transform.pos.y.ToString();
        txt.text = test.transform.position.y.ToString();
    }
}
