﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        LoadAB();
        AnimationInstancing.AnimationInstancingMgr.Instance.UseInstancing = true;   
    }

    void LoadAB()
    {
		  StartCoroutine(AnimationInstancing.AnimationManager.Instance.LoadAnimationAssetBundle(Application.streamingAssetsPath + "/AssetBundle/animationtexture"));
    }
}
