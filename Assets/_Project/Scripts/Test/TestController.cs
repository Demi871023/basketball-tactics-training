﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using wvr;

public class TestController : MonoBehaviour
{
    /*
    public GameObject main_p;
    public GameObject half_sphere;

    [Tooltip("Any layers the raycast should not affect")]
	public LayerMask exclude_layers;

    private bool is_contact;
    private bool unreachable;

    public GameObject canvas_eyeblink;
    
    private Vector3 end;
    private Vector3 hit_point;
    private LayerMask hit_layer;
    private WaveVR_Controller.Device controller;
    private WaveVR_Beam _beam;
    private WaveVR_ControllerPointer _pointer;

    private GameObject dominant_controller = null;

    [Tooltip("How manu angles from world up the surface can point and still be valid. Avoids casting onto walls.")]
	public float surface_angle = 5;

    [Tooltip("How many segments to use for curve, must be at least 3. More segments = better quality")]
    private int segments = 30;
    private LineRenderer arc_renderer;
    private Color color_reachable = new Color(76.0f/255.0f, 225.0f/225.0f, 225.0f/255.0f);
    private Color color_unreachable = new Color(255.0f/255.0f, 23.0f/225.0f, 21.0f/255.0f);

    [Tooltip("Horizontal distance of end point from controller")]
	public float distance = 15.0f;
	[Tooltip("Vertical of end point from controller")]
	public float drop_height = 5.0f;
	[Tooltip("Height of bezier control (0 is at mid point)")]
	public float control_height = 10.0f;

    private Vector3 controller_forward{
        get {
			#if UNITY_EDITOR
                return this.transform.forward;
			#else
			    return controller.transform.rot * Vector3.forward;
            #endif
		}
    }
    private Vector3 controller_up{
        get {
			#if UNITY_EDITOR
			    return this.transform.up;
            #else
                return controller.transform.rot * Vector3.up;
			#endif
		}
    }
    private Vector3 controller_right{
        get {
			#if UNITY_EDITOR
			    return this.transform.right;
			#else
			    return controller.transform.rot * Vector3.right;
            #endif
		}
    }
    private Vector3 controller_position{
        get {
			#if UNITY_EDITOR
			    return this.transform.position;
            #else
			    return controller.transform.pos + main_p.transform.position;
            #endif
		}
    }
    private Vector3 start{
        get{
            return controller_position;
        }
    }
    private Vector3 control{
        get{
            return start + (end - start) * 0.5f + controller_up * control_height;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant);
        arc_renderer = GetComponent<LineRenderer>();
        arc_renderer.positionCount = segments;
        
    }

    private void OnEnable() {
        WaveVR_Utils.Event.Listen (WaveVR_Utils.Event.CONTROLLER_MODEL_LOADED, OnControllerLoaded);
    }

    private void OnControllerLoaded(params object[] args)
    {
        WaveVR_Controller.EDeviceType _type = (WaveVR_Controller.EDeviceType)args [0];
        if (_type == WaveVR_Controller.EDeviceType.Dominant)
        {
            this.dominant_controller = (GameObject)args [1];
            listControllerObjects(this.dominant_controller);
        }
    }

    private void listControllerObjects(GameObject ctrlr)
    {
        if (ctrlr == null)
            return;

        _beam = null;
        _pointer = null;

        // Get all children.
        GameObject[] _objects = new GameObject[ctrlr.transform.childCount];
        for (int i = 0; i < ctrlr.transform.childCount; i++)
            _objects[i] = ctrlr.transform.GetChild (i).gameObject;

        // Find beam.
        for (int i = 0; i < _objects.Length; i++)
        {
            _beam = _objects [i].GetComponentInChildren<WaveVR_Beam> ();
            if (_beam != null)
                break;
        }
        if (_beam != null)
            Debug.Log ("Find beam: " + _beam.name);

        // Find pointer.
        for (int i = 0; i < _objects.Length; i++)
        {
            _pointer = _objects [i].GetComponentInChildren<WaveVR_ControllerPointer> ();
            if (_pointer != null)
                break;
        }
        if (_pointer != null)
            Debug.Log ("Find pointer: " + _pointer.name);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKey(KeyCode.T)){
        //if(WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant).GetPress(WVR_InputId.WVR_InputId_Alias1_Touchpad)){
            //_beam.enabled = false;
            //_pointer.enabled = false;
            is_contact = false;
            
            end = hit_point = controller_position + controller_forward * distance + (controller_up * -1.0f) * drop_height;
            //Debug.Debug.Log(end);
            arc_renderer.positionCount = segments;
            RaycastHit hit;
            Vector3 last = start;
            float recip = 1.0f/ (float)(segments - 1);
            for (int i = 0; i < segments; ++i) {
                float t = (float)i * recip;
                Vector3 sample = SampleCurve(start, end, control, Mathf.Clamp01(t));
                if(!is_contact)
                    arc_renderer.SetPosition(i, sample);

                if (Physics.Linecast(last, sample, out hit)) {
                    float angle = Vector3.Angle(Vector3.up, hit.normal);
                    //Debug.Log("hit: " + hit.transform.gameObject.layer + ", " + hit.transform.gameObject.name);
                    if (angle < surface_angle) {
                        arc_renderer.positionCount = i;
                        hit_point = hit.point;
                        hit_layer = hit.transform.gameObject.layer;
                        is_contact = true;
                        half_sphere.SetActive(true);
                        half_sphere.transform.position = hit_point;
                        SetCurveVisual(color_reachable);
                        break;
                    }
                }
                last = sample;
            }
            

            unreachable = (exclude_layers.value == (exclude_layers.value | (1 << hit_layer.value)));
            if(!is_contact || unreachable){
                SetCurveVisual(color_unreachable);
                half_sphere.SetActive(false);
            }
        }

        if(Input.GetKeyUp(KeyCode.T)){
        //if(WaveVR_Controller.Input(WaveVR_Controller.EDeviceType.Dominant).GetPressUp(WVR_InputId.WVR_InputId_Alias1_Touchpad)){
            arc_renderer.positionCount = 0;
            half_sphere.SetActive(false);
            //_beam.enabled = true;
            //_pointer.enabled = true;
            if(is_contact && !unreachable){
                Vector3 new_pos = hit_point;
                new_pos.y = 1.5f;
                main_p.transform.position = new_pos;
                canvas_eyeblink.GetComponent<AnimatedBlinkEyeEffect>().InvokeEyeBlink();
            }
        }
    
    }

    private Vector3 SampleCurve(Vector3 start, Vector3 end, Vector3 control, float t){
		// Interpolate along line S0: control - start;
		Vector3 Q0 = Vector3.Lerp(start, control, t);
		// Interpolate along line S1: S1 = end - control;
		Vector3 Q1 = Vector3.Lerp(control, end, t);
		// Interpolate along line S2: Q1 - Q0
		Vector3 Q2 = Vector3.Lerp(Q0, Q1, t);
		return Q2; // Q2 is a point on the curve at time t
	}

    private void SetCurveVisual(Color color){
        Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(0.1f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
		);
		arc_renderer.colorGradient = gradient;
    }
    */
}
