using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(Animator))]
public class Hand : MonoBehaviour
{   
    
    public ActionBasedController Controller;
    public float Speed;
    private Animator _animator;
    private float _gripTarget;
    private float _triggerTarget;
    private float _gripCurrent;
    private float _triggerCurrent;
    private string _animatorGripParam = "Grip";
    private string _animatorTriggerParam = "Trigger";

    private void Awake(){
        _animator = this.GetComponent<Animator>();
    }

    private void Update(){
        SetGrip(Controller.selectAction.action.ReadValue<float>());
        SetTrigger(Controller.activateAction.action.ReadValue<float>());
        AnimateHand();
    }

    internal void SetGrip(float v){
        _gripTarget = v;
    }

    internal void SetTrigger(float v){
        _triggerTarget = v;
    }

    private void AnimateHand(){
        if(_gripCurrent != _gripTarget){
            _gripCurrent = Mathf.MoveTowards(_gripCurrent, _gripTarget, Time.deltaTime * Speed);
            _animator.SetFloat(_animatorGripParam, _gripCurrent);
        }

        if(_triggerCurrent != _triggerTarget){
            _triggerCurrent = Mathf.MoveTowards(_triggerCurrent, _triggerTarget, Time.deltaTime * Speed);
            _animator.SetFloat(_animatorTriggerParam, _triggerCurrent);
        }
    }
    
}
