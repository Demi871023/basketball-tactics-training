/*
Skinned Mesh Combiner_2019.3.2
Lylek Games

Thank you for your purchase!

Included are three .unitypackage files: One for the Standard (Built-in) Pipline, one for the High-Definition Pipline,
and one for the Universal Pipeline.

Please import the appropriate file for your project by selecting: Assets > Import Package > Custom Package > and browsing
to the select .unitypackage.

For information on how to use the asset in your project, please adhere to the Readme file(s) located in the root of the
package content, upon import.

For assistance please contact:
support@lylekgames.com
or visit https://www.lylekgames.com/ssl/contacts

*/
